
$(document).ready(function(){

    //selectpicker search placeholder
    $('.bs-searchbox input').attr('placeholder', 'Search record..');

    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    //Auto Close Timer
    $('.sync_contacts').on('change', function () {

        $('#loading').show();

        if($('.sync_contacts').is(':checked')){
            var sync = 1;
            var message = 'Your contacts synchronize enabled successfully!';
        }else{
            var sync = 0;
            var message = 'Your contacts synchronize disabled successfully!';
        }
        var link = base_url + '/sync_stats';

        $.post(link, {sync:sync}, function(result){
            $('#loading').show();
                if(result == 1){
                    swal('Success!', message, 'success' );
                }
            $('#loading').hide();
        });
    });
});


/* notifications*/

function notify(from, align, type, message) {

    $.notify({
        icon: "notifications",
        message: message

    }, {
        type: type[type],
        timer: 3000,
        placement: {
            from: from,
            align: align
        }
    });
}

function refreshTable(){
    $('#load_datatable').DataTable().ajax.reload();
}

function deleteRow(x){

    var id = $(x).data('id');
    var obj = $(x).data('obj');
    var link = $("#delete_link").val();

    if(id!='' && obj!=''){
        swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary Record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-warning',
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
        }).then(function() {
                
                    $("#loading").show();

                    $.post(link, {id: id, obj: obj}, function(result){
                            //console.log(result);
                            if(result!='0'){
                                var data = JSON.parse(result);

                                if(data.type == 'success'){

                                    //hide gallery image
                                    if(obj == 'gallery') { $('#item_'+id).hide(); }

                                    swal("Success!", data.msg, "success");

                                    if( obj == 'mailjet_settings' || obj == 'mailgun_settings' || obj == 'smtp_settings' || obj == 'sparkpost_settings' || obj == 'sendgrid_settings' || obj == 'activecampaign_settings' || obj == 'aweber_settings' || obj == 'getresponse_settings' || obj == 'mailchimp_settings'){
                                        location.reload();
                                    }
                                }

                                if(data.type == 'error'){
                                    swal("Error!", data.msg, "error");
                                }

                            }else{
                                swal("Error!", "Something went wrong.", "error");
                            }
                            if(obj != 'gallery') {
                                refreshTable();
                            }
                            if(obj == 'delete_list') {
                                $('#load_datatable_list').DataTable().ajax.reload();
                            }
                            $('#loading').hide();
                        }

                    );

                
            }
        )

    }else{
        swal("Error!", "Information Missing. Please reload the page and try again.", "error");
    }
}


function changeStatus(x){

    $("[name='status']").on('switchChange.bootstrapSwitch', function(event, state) {

        $('#loading').show();

        var id = $(this).data('id');
        var obj = $(this).data('obj');
        var link = $("#change_tatus_link").val();

        if(id!='' && obj!=''){
            $.post(link, {id: id, obj: obj, status: state}, function(result){
                    $('#loading').hide();

                    refreshTable();

                    if(result!='0'){
                        notify('top','center', 'success', 'Status Updated successfully!');

                    }else{
                        notify('top', 'center',"error", "danger", "something went wrong. Please reload page and try again!");
                    }
                }

            );
        }


    });
}