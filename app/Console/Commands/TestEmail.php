<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use GB;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Testing Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = '';
        $users = User::findOrFail(1);
        if($users){
            $email = $users->email;
        }

        if($email!==''){
            mail($email,'testing email','here is message of this email');
        }
    }
}
