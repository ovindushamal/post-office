<?php

namespace App\Http\Controllers\Admin;

use App\EmailTemplate;
use App\SubjectTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use DB;
use Auth;
use Carbon\Carbon;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SubjectTemplate::all();
        $data = array();
        return view("admin.email_template.list", ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Categories::all();
        return view("admin.email_template.add", ['cats' => $cats]);
    }


    public function importForm()
    {
        return view("admin.email_template.Import");
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = $this->getToken();
        //    insert subject to table `email_subjects`
        $check_subject = DB::table('email_subjects')->select('id')->where('id', '=', $request->subject_id)->first();
        $getSubject = SubjectTemplate::where('id', $request->subject_id)->first();
        $SubjectTemplate = SubjectTemplate::where('token', $request->input('token'))->first();
        $tokenCheck = $request->has('token');
        if ($tokenCheck) {
            if ($getSubject['token'] === $request->input('token')) {
                $token = $getSubject['token'];
            } else if ($request->input('token') === $SubjectTemplate['token']) {
                $token = $this->getToken();
            }
            $token = $request->input('token');
        }

        if ($check_subject) {
            DB::table('email_subjects')
                ->where('id', $request->subject_id)
                ->update(['subject' => $request->subject, 'token' => $token]);

            $subject_id = $request->subject_id;
        } else {
            $subject_id = DB::table('email_subjects')->insertGetId(
                ['user_id' => Auth::User()->id, 'details' => '',  'subject' => $request->subject, 'token' => $token, 'status' => 1]
            );
        }


        $obj = new EmailTemplate();
        if ($request->id != '') {
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());

        $obj->category_id = $request->category_id;
        $obj->subject_id = $subject_id;
        $obj->domain = $request->domain;

        $obj->user_id = Auth::User()->id;
        if ($obj->save()) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function storeImport(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        $data = EmailTemplate::findOrFail($id);
        //        $cats = Categories::all();
        $subject = SubjectTemplate::find($id);
        $content = DB::table('email_contents')->where('subject_id', $subject->id)->first();

        //echo $subject->subject;

        $categories = Categories::select(['id', 'category', 'status', 'created_at'])->where('user_id', Auth::user()->id)->get();

        return view("admin.email_template.add", ['content' => $content, 'subject' => $subject, 'categories' => $categories]);
    }

    public function getToken()
    {
        return str_random(8);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
