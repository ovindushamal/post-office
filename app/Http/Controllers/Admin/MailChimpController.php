<?php

namespace App\Http\Controllers\Admin;

use App\EmailBroadcast;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mailchimp\Mailchimp;
use Illuminate\Support\Facades\Config;
use App\Settings;
use Auth;
use DB;

class MailChimpController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::select('mailchimp_key')->get();
        foreach($settings as $value)
        {
            $api_key = $value->mailchimp_key;
            if ($api_key!='')
            {
                $mc = new Mailchimp($api_key);
                $mailChimps = EmailBroadcast::where('type','=','mailchimp')->get();

                if ($mailChimps)
                {
                    foreach($mailChimps as $k=>$v)
                    {
                        $report = $mc->get("reports/$v->boradcast_id", []);
                        $totalSent = (isset($report['emails_sent']))?$report['emails_sent']:0;
                        $unsubscribed = (isset($report['unsubscribed']))?$report['unsubscribed']:0;
                        $opened = 0;
                        if (isset($report['opens']))
                        {
                            $opens = $report['opens'];
                            $opened = $opens->unique_opens;
                            $last_open = $opens->last_open;
                        }

                        $clicked = 0;
                        if (isset($report['clicks']))
                        {
                            $clicks = $report['clicks'];
                            $clicked = $clicks->unique_clicks;
                            $last_click = $clicks->last_click ;
                        }

                        $check_open = DB::table('autoresponders_stats')
                            ->select('opened_time', 'broadcast_id')
                            ->where(['broadcast_id' => $v->boradcast_id, 'type' => 'mailchimp', 'opened_time' => trim($last_open) ]  )
                            ->first();

                        if (count($check_open) == 0 )
                        {
                            if ($last_open != "")
                            {
                                DB::table('autoresponders_stats')->insert(
                                    [
                                        'opened_time' => gmdate('Y-m-d H:i:s', strtotime($opens->last_open )),
                                        'broadcast_id' => $v->boradcast_id,
                                        'type' => 'mailchimp',
                                        'user_id' => $v->user_id
                                    ]
                                );
                            }
                        }
                        // clicked
                        $check_click = DB::table('autoresponders_stats')
                            ->select('clicked_time')
                            ->where(['broadcast_id' => $v->boradcast_id, 'type' => 'mailchimp', 'clicked_time' => $last_click ])
                            ->first();

                        if (count($check_click)== 0 )
                        {
                            if ($last_click != "")
                            {
                                DB::table('autoresponders_stats')->insert(
                                    [
                                        'clicked_time' => gmdate('Y-m-d H:i:s', strtotime($opens->last_click )),
                                        'broadcast_id' => $v->boradcast_id,
                                        'type' => 'mailchimp',
                                        'user_id' => $v->user_id
                                    ]
                                );
                            }
                        }

                        EmailBroadcast::where(['type' => 'mailchimp', 'boradcast_id' => $v->boradcast_id ])
                            ->update([
                                'sent' => $totalSent,
                                'opened' => $opened,
                                'clicked' => $clicked,
                                'unsubscribed' => $unsubscribed,
                            ]);
                    }
                }
            }
        }

        exit;
        return 'updated';

        //https://github.com/pacely/mailchimp-api-v3#create-lists

        /*Config::set('mailchimp.apikey', Settings::where('user_id', Auth::User()->id)->value('mailchimp_key'));
        $api_key = Config::get('mailchimp.apikey');*/
        $api_key = Settings::where('user_id', Auth::User()->id)->value('mailchimp_key');

        $mc = new Mailchimp($api_key);


        /*$result = $mc->post('lists', [
            'name' => 'New list',
            'permission_reminder' => 'You signed up for updates on Greeks economy.',
            'email_type_option' => false,
            'contact' => [
                'company' => 'Doe Ltd.',
                'address1' => 'DoeStreet 1',
                'address2' => '',
                'city' => 'Doesy',
                'state' => 'Doedoe',
                'zip' => '1672-12',
                'country' => 'US',
                'phone' => '55533344412'
            ],
            'campaign_defaults' => [
                'from_name' => 'John Doe',
                'from_email' => 'john@doe.com',
                'subject' => 'My new campaign!',
                'language' => 'US'
            ]
        ]);*/

        /*$result = $mc->request('lists',[]);

        if (isset($result['lists']))
        {
            if (count($result['lists'])>0)
        {

                $lists = $result['lists'];
                foreach($lists as $k=>$v)
        {
                    echo $v->id.' - '.$v->web_id.' - '.$v->name."<br>";
                }

            }
        }

        return '';*/


        /* start reporting */
        $report = $mc->get('reports/0eee6e79dc', []);

        echo "sent ".$totalSent = (isset($report['emails_sent']))?$report['emails_sent']:0;
        echo "<br> unsubscribed ".$unsubscribed = (isset($report['unsubscribed']))?$report['unsubscribed']:0;

        if (isset($report['opens']))
        {
            $opens = $report['opens'];
            echo "<br> opened ".$opens->unique_opens;
        }

        if (isset($report['clicks']))
        {
            $clicks = $report['clicks'];
            echo "<br> clicked ".$clicks->unique_clicks;
        }

        return '';
        /* End reporting */

        /* start creating campaign, setting content in campaign and sending it to selected list */
        $result_cr = $mc->post('campaigns', [
            'type' => 'regular',
            'recipients'=>[
                'list_id'=>'37e2f6e01d'
            ],
            'settings'=>[
                'title' => 'Testing Campaign',
                'subject_line'=>'This is testing sajawal 22',
                'from_name'=>'NWS',
                'reply_to'=>'nisar@nimblewebsolutions.com'
            ]
        ]);


        echo $campaign_id = isset($result_cr['id'])?$result_cr['id']:'';
        if ($campaign_id!='')
        {
            $post_cn = $mc->put("campaigns/$campaign_id/content",[
                'html'=>'<h>This is test content of email</h>'
            ]);

            echo "<br>========================================================================<br>";

            $post_sn = $mc->post("campaigns/$campaign_id/actions/send", []);


            echo "<br>========================================================================<br>";

        }
        /* Ends creating campaign, setting content in campaign and sending it to selected list */

        return '';

        $post_cm = $mc->post('campaigns/f0029e4119/actions/send ');

        return '';

        // Get 10 lists starting from offset 10 and include only a specific set of fields
        $result = $mc->request('lists',[]);



        if (isset($result['lists']))
        {
            if (count($result['lists'])>0)
            {

                $lists = $result['lists'];
                foreach($lists as $k=>$v)
                {
                    echo $v->id.' - '.$v->web_id.' - '.$v->name."<br>";
                }

            }
        }

        $result = $mc->get('lists/ca27e52059', ['fields' => 'id,name,stats.member_count']);


    }

    public function updateStats()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    public function importForm()
    {

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function storeImport(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
