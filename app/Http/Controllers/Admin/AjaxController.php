<?php

namespace App\Http\Controllers\Admin;

use App\Activecampaign_Setting;
use App\Aweber_Setting;
use App\GetResponse_Setting;
use App\MailChimp_Setting;
use App\SendLane_Setting;
use App\Categories;
use App\EmailTemplate;
use App\SubjectTemplate;
use App\Gallery;
use App\EmailBroadcast;
use App\User;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use DB;


class AjaxController extends Controller
{
    private  $notification_subject, $notification_to_email = "";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function syncStats(Request $request)
    {
        $user = User::where(['id' => Auth::user()->id])->update(['sync' => $request->sync]);
        if ($user)
        {
            echo 1;
        }else{
            echo 2;
        }
    }

    function showBoradcastError(Request $request)
    {
        $error = DB::table('autoresponder_error')->where('email_campaign_title_id', $request->id)->value('error');
        echo $error;
    }

    function loadContacts(Request $request)
    {
        $count = 0;
        $contacts = DB::table('autoresponder_contact')
           ->where( 'user_id', Auth::user()->id );
        return Datatables::of($contacts)
        ->editColumn('id', function($contacts)
        {
            global $count;
            $count++;
            return $count;
        })
        ->addColumn('action', function ($contacts){
            $b= '<button onclick="deleteRow(this)" data-id="'.$contacts->id.'" data-obj="delete_contact" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>';
            return $b;
        })
        ->make(true);
    }

    function loadLists(Request $request)
    {
        $count = 0;
        $list = DB::table('autoresponder_lists')
           ->where( 'user_id', Auth::user()->id );
        return Datatables::of($list)
        ->editColumn('id', function($list)
        {
            global $count;
            $count++;
            return $count;
        })
        ->addColumn('action', function ($list){
            $b= '<button onclick="deleteRow(this)" data-id="'.$list->list_id.'" data-obj="delete_list" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>';
            $b.='<button onclick="editList(this)" data-id="'.$list->id.'"  class="btn btn-xs btn-info"><i class="fas fa-pencil-alt"></i></button>';

            return $b;
        })
        ->make(true);
    }

    function loadOpenByEmail(Request $request)
    {

        $group_id = DB::table('email_broadcasts')->where('email_campaign_title_id', $request->camp_title_id)->value('group_id');
        $title_ids = DB::table('email_broadcasts')->select('email_campaign_title_id')->where('group_id', $group_id)->get()->toArray();

        $id = array();
        foreach($title_ids as $ids)
        {
            array_push($id , $ids->email_campaign_title_id);
        }

        // emails
        $count= 0;

        $emails = DB::table('autoresponder_new_stats')
            ->join('email_broadcasts', 'autoresponder_new_stats.email_campaign_title_id', '=', 'email_broadcasts.email_campaign_title_id')
            ->select('autoresponder_new_stats.email', 'autoresponder_new_stats.type', 'autoresponder_new_stats.id')
            ->where('autoresponder_new_stats.stat_type', '=', 'open')
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            ->where('autoresponder_new_stats.user_id', '=', Auth::user()->id)
            ->where('autoresponder_new_stats.email', '!=', '')
            ->where('autoresponder_new_stats.subject_id', '!=', "")
            ->whereColumn('email_broadcasts.boradcast_id', 'autoresponder_new_stats.email_brodcast_id' );
            //->groupBy('autoresponder_new_stats.email')


        return Datatables::of($emails)
            ->editColumn('id', function( $emails )
            {
                global $count;
                $count++;
                return $count;
            })
            ->make(true);
    }


    function loadUnopenByEmail(Request $request)
    {

        $group_id = DB::table('email_broadcasts')->where('email_campaign_title_id', $request->camp_title_id)->value('group_id');
        $title_ids = DB::table('email_broadcasts')->select('email_campaign_title_id')->where('group_id', $group_id)->get()->toArray();

        $id = array();
        foreach($title_ids as $ids)
        {
            array_push($id , $ids->email_campaign_title_id);
        }

        $un_open_emails = DB::table('campaign_send_list')
            ->join('autoresponder_new_stats', 'campaign_send_list.campaign_title_id', 'autoresponder_new_stats.email_campaign_title_id')
            ->select(
                'autoresponder_new_stats.email',
                'autoresponder_new_stats.email_category_id',
                'autoresponder_new_stats.email_campaign_title_id',
                'campaign_send_list.type',
                'campaign_send_list.list_id',
                'campaign_send_list.type as autores_type',
                'campaign_send_list.account_id'
            )
            ->where(['autoresponder_new_stats.user_id' => Auth::user()->id])
            ->where('autoresponder_new_stats.email', '!=', '')
            ->where('autoresponder_new_stats.subject_id', '!=', "")
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            //->groupBy('autoresponder_new_stats.email')
            //->groupBy('campaign_send_list.type')
            ->where('autoresponder_new_stats.stat_type', 'open')
            ->get();


        $unOpenEmails = $list_id = $email = $type = $account_id = array();

        foreach($un_open_emails as $value)
        {

            array_push($list_id, $value->list_id);
            array_push($email,$value->email);
            array_push($type, $value->autores_type);
            array_push($account_id, $value->account_id);
        }

        $list_id = array_unique( $list_id );
        $email = array_unique( $email );
        $type = array_unique( $type );
        $account_id = array_unique( $account_id );

        $data = DB::table('autoresponder_contact')
            ->whereIn('list_id', $list_id )
            ->whereIn('account_id',$account_id )
            ->whereIn('type', $type )
            ->whereNotIn('email', $email )
            ->where(['user_id' => Auth::user()->id] )
            ->groupBy('email')
            ->get();


        foreach($data as $item)
        {
            array_push($unOpenEmails, ['email'=> $item->email, 'type' => $item->type ] );
        }
        $id= 0;

        return Datatables::of($unOpenEmails)
            ->editColumn('id', function( $emails )
            {
                global $id;
                $id++;
                return $id;
            })
            ->make(true);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('datatables.index');
    }

    function privateEmail(Request $request)
    {

        if ($request->action == 'imported')
        {
            $req = 'id';
        }else{
            $req = 'email_campaign_title_id';
        }

        $stat = DB::table('email_contents')->where(['user_id' => Auth::user()->id, $req => $request->id])->value('is_private');

        if ($stat == 1)
        {  $status = 0; $return = 0; }
        if ($stat == 0)
        {  $status = 1; $return = 1; }

        $update = DB::table('email_contents')->where(['user_id' => Auth::user()->id, $req => $request->id])->update( ['is_private' => $status] );

        echo   json_encode(array('res' => $status));

    }


    public function loadSubCategoriesEmails(Request $request)
    {
        $count = 0;
        $subjects =  $imported_emails = DB::table('email_subjects')
            ->join('email_contents', 'email_contents.subject_id', '=', 'email_subjects.id')
            ->select('email_subjects.id','email_subjects.subject', 'email_subjects.created_at')
            ->where( ['email_contents.user_id' => Auth::user()->id , 'email_subjects.sub_category_id' => $request->id ]);

        return Datatables::of($subjects)
            ->addColumn('action', function ($subjects)
            {
                $b = '<button class="btn btn-xs btn-purple" data-id="'.$subjects->id.'" data-toggle="tooltip" title="Use Me" onclick="submitUseMe(this)"><i class="material-icons">mouse</i></button> ';
                return $b;
            })
            ->editColumn('id', function()
            {
                global $count;
                $count++;
                return $count;
            })

            ->make(true);
    }


    public function loadSubCategories(Request $request)
    {
        $category = Categories::select(['id','account_id', 'category', 'type', 'category_id', 'status', 'created_at'])->where('user_id', Auth::user()->id)->get();

        $cats = array();
        $count =0;
        foreach($category as $val)
        { $count++;
            $total =  $imported_emails = DB::table('email_subjects')
                ->join('email_contents', 'email_contents.subject_id', '=', 'email_subjects.id')
                ->where( ['email_contents.user_id' => Auth::user()->id , 'email_subjects.sub_category_id' => $val->id ])->count();

            array_push($cats, [ 'account_id' => $val->account_id, 'type' => $val->type, 'index' => $count, 'id' => $val->id, 'total' => $total, 'category' => $val->category,'category_id' => $val->category_id, 'status' => $val->status, 'created_at' => $val->created_at->format('Y-m-d h:i:s') ]);
        }

        return Datatables::of($cats)
            ->addColumn('action', function ($cats)
            {
                 if ($cats['account_id'] == "")
                 {
                     $account_id = 'manual-'.$cats['id'];
                 }else{
                     $account_id = $cats['account_id'];
                 }

                $b = '<a onclick="editRow(' . $cats['id'] . ')" href="javascript:;" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fas fa-pencil-alt"></i></a> ';
                $b .= '<a onclick="deleteIt(this)" data-id="'.$cats['id'].'" data-obj="email_sub_category" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a> ';
                //$b = '<a onclick="share_emails_code(this)" href="javascript:;" data-class="'.$cats['type'].'_'.$account_id.'" datadata-toggle="tooltip"-title="'. ucfirst($cats['type']).'" data-id="'.$account_id.'" title="Share emails from aweber account" class="btn btn-xs btn-pink"> <i class="fa fa-share-alt"></i> </a>';
                $b .= '&nbsp;<a  data-id="'.$cats['id'].'"  href="'.route('email-sub-category.show', $cats['id']).'" data-toggle="tooltip" title="View" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a> ';

                return $b;
            })
            ->editColumn('type', function( $cats )
            {
                if ($cats['category_id'] == 6) $category = 'imported Emails';
                if ($cats['category_id'] == 3) $category = 'User Created Emails';

                return $category;
            })
            ->make(true);
    }

    public function loadCategories(Request $request)
    {
        $cats = Categories::select(['id', 'category', 'status', 'created_at'])->where('user_id', Auth::user()->id);

        return Datatables::of($cats)
            ->addColumn('action', function ($cats)
            {
                $b = '<input type="checkbox" name="status" data-id="'.$cats->id.'" data-obj="categories" " '. (($cats->status=='1')?'checked':'') .' " data-on-text="On" data-off-text="Off" data-size="mini"  >&nbsp; <span id="action_separator">|</span> &nbsp;';
                $b .= '<a onclick="editRow(' . $cats->id . ')" href="javascript:;" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fas fa-pencil-alt"></i></a> ';
                $b .= '<a onclick="deleteRow(this)" data-id="'.$cats->id.'" data-obj="categories" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a> ';
                return $b;
            })
            ->make(false);
    }



    public function loadUsers(Request $request)
    {
        $data = User::select(['id', 'name', 'type', 'email', 'status', 'created_at']);
        $count =0;
        return Datatables::of($data)
            ->addColumn('action', function ($data)
            {
                $b='';
                if($data->type != 'a') {
                    $b = '<input type="checkbox" name="status" data-id="' . $data->id . '" data-obj="users"  ' . (($data->status == '1') ? 'checked' : '') . '  data-on-text="On" data-off-text="Off" data-size="mini"  >&nbsp; <span id="action_separator">|</span> &nbsp;';
                }
                $b .= '<a onclick="editRow(' . $data->id . ')" href="javascript:;" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fas fa-pencil-alt"></i></a> ';
                $b .= '<a onclick="deleteRow(this)" data-id="'.$data->id.'" data-obj="users" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a> ';
                $b .= '<a onclick="showStats(this)" data-id="'.$data->id.'" href="javascript:;" data-toggle="tooltip" title="Stats" class="btn btn-xs btn-info"><i class="fa fa-line-chart"></i></a> ';
                return $b;
            })
            ->editColumn('id', function ()
            {
                global $count;
                $count++;
                return $count;
            })
            ->make(true);

    }

    public function loadTrackingStats(Request $request)
    {

        $stats = DB::table('autoresponder_new_stats')
            ->select(
                'id',
                'subject',
                'type',
                'stat_type',
                'country',
                'ip',
                'city',
                'region',
                'longi',
                'lati',
                'date_time'
                //'user_agent',
                //'referrer'
            );

        return Datatables::of($stats)
            ->make(true);
    }



    public function loadSubjects(Request $request)
    {
        //$subject = SubjectTemplate::select(['id', 'subject', 'status', 'created_at']);

        $subject = DB::table('email_subjects')
            ->leftjoin('email_category', 'email_subjects.email_category_id', '=', 'email_category.id')
            ->select('email_subjects.id',
                'email_subjects.subject',
                'email_subjects.status',
                'email_subjects.created_at',
                'email_category.category'
            );


        return Datatables::of($subject)
            ->addColumn('action', function ($subject)
            {
                $b = '<input type="checkbox" name="status" data-id="'.$subject->id.'" data-obj="email_subjects" " '. (($subject->status=='1')?'checked':'') .' " data-on-text="On" data-off-text="Off" data-size="mini"  >&nbsp; <span id="action_separator">|</span> &nbsp;';
                $b .= '<a href="' . url("/email-template/$subject->id/edit") . '" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fas fa-pencil-alt"></i></a> ';
                $b .= '<a onclick="deleteRow(this)" data-id="'.$subject->id.'" data-obj="email_subjects" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a> ';
                $b .= '<a onclick="viewContent(this)" data-id="'.$subject->id.'"  href="javascript:;" data-toggle="tooltip" title="View email content" class="btn btn-xs btn-info danger-alert"><i class="fa fa-eye"></i></a> ';

                return $b;
            })
            ->make(true);
    }

    public function loadEmailContent(Request $request)
    {
       $content = DB::table('email_contents')
       // ->join('categories', 'email_contents.category_id', '=', 'categories.id')
        ->join('email_subjects', 'email_subjects.id', '=', 'email_contents.subject_id')
        ->select('email_contents.id',
            'email_contents.status',
            'email_contents.created_at',
            'email_subjects.subject'
            //'categories.category'
        );

        return Datatables::of($content)
            ->addColumn('action', function ($content)
            {
                $b = '<input type="checkbox" name="status" data-id="'.$content->id.'" data-obj="email_contents" " '. (($content->status=='1')?'checked':'') .' " data-on-text="On" data-off-text="Off" data-size="mini"  >&nbsp; <span id="action_separator">|</span> &nbsp;';
                $b .= '<a href="' . url("/email-template/$content->id/edit") . '" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fas fa-pencil-alt"></i></a> ';
                $b .= '<a onclick="deleteRow(this)" data-id="'.$content->id.'" data-obj="email_contents" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a> ';
                return $b;
            })
            ->make(true);
    }

// load imported emails
    function loadMyEmails(Request $request)
    {
        $count=0;

        $imported_emails = DB::table('email_sub_category')
            ->join('email_subjects', 'email_subjects.sub_category_id', '=', 'email_sub_category.id')
            ->join('email_contents', 'email_contents.subject_id', '=', 'email_subjects.id')
            ->select('email_sub_category.category', 'email_sub_category.type', 'email_subjects.subject', 'email_subjects.id', 'email_contents.id as contents_id', 'email_contents.is_private')
            ->where( ['email_sub_category.user_id' => Auth::user()->id , 'email_subjects.email_category_id' => 6, 'email_contents.email_category_id' => 6] );

        return Datatables::of($imported_emails)
            ->editColumn('id', function()
            {
                global $count;
                $count++;
                return $count;
            })
            ->addColumn('action', function ($action)
            {

                if ($action->is_private == 0)
                {
                    $status = "fa-unlock";
                    $title = 'Make private';
                }
                if ($action->is_private == 1)
                {
                    $status = "fa-lock";
                    $title = 'Make public';
                }

                $b = '<button class="btn btn-xs btn-purple"  data-id="'.$action->id.'" data-toggle="tooltip" title="Use Me" onclick="submitUseMe(this)" ><i class="material-icons">mouse</i></button>';
                $b .= '&nbsp;<a onclick="makePrivate(this)" data-id="'.$action->contents_id.'" data-toggle="tooltip" title="'.$title.'" class="btn btn-xs btn-pink"><i class="fa '.$status.' "></i></a>';
            return $b;
            })
            ->make(true);
    }

    // load shared emails
    function loadSharedEmails(Request $request)
    {

        $sub_category = DB::table('email_sub_category')
            ->whereIn('id', explode(',', Auth::user()->email_shared_id) )
            ->get();

        $shared_emails = array();
        $count = 0;
        foreach($sub_category as $value)
        {
            $count++;
            $user_name = User::where('id', $value->user_id)->value('name');

            $total = DB::table('email_subjects')
                ->join('email_contents', 'email_subjects.id', '=', 'email_contents.subject_id')
                ->where('email_contents.email_category_id', 6)
                ->where('email_contents.is_private', 0)
                ->where(['email_subjects.sub_category_id' => $value->id])->count();
            array_push($shared_emails, ['user_name' => $user_name, 'index' => $count, 'id' => $value->id, 'title' => $value->category, 'total' => $total ]);

        }
             return Datatables::of($shared_emails)

        ->addColumn('action', function ($action)
        {
            $b = '<a href="'.route('ar-campaigns.show', $action['id']).'" class="btn btn-xs btn-info" data-toggle="tooltip" title="view emails"><i class="fa fa-eye"></i></a>';

            return $b;
        })
            ->make(true);
    }

    function sharedEmailsDetail(Request $request)
    {

        $count = 0;

        $shared_emails = DB::table('email_sub_category');
        $shared_emails = $shared_emails->join('email_subjects', 'email_subjects.sub_category_id', '=', 'email_sub_category.id');
        $shared_emails = $shared_emails->join('email_contents', 'email_contents.subject_id', '=', 'email_subjects.id');
        $shared_emails = $shared_emails->select('email_sub_category.category', 'email_sub_category.type', 'email_subjects.subject', 'email_subjects.id');
        $shared_emails = $shared_emails->where( [
            //'email_sub_category.user_id' => Auth::user()->id ,
            'email_subjects.email_category_id' => 6,
            'email_contents.is_private' => 0
        ] );
        // shared emails
        if (Auth::User()->email_shared_id!="")
        {
            $shared_emails = $shared_emails->where('email_subjects.sub_category_id', $request->sub_id );
        }

        return Datatables::of($shared_emails)

            ->editColumn('id', function($shared_emails)
            {
                global $count;
                 $count++;
                return $count;
            })

            ->addColumn('action', function ($action)
            {
                $b = '<button class="btn btn-xs btn-purple"  data-id="'.$action->id.'" data-toggle="tooltip" title="Use Me" onclick="submitUseMe(this)" ><i class="material-icons">mouse</i></button>';

                return $b;
            })
            ->make(true);
    }

// your email contents
    public function loadYourEmailContent(Request $request)
    {
        $campaign_title =  DB::table('email_campaign_title')
            ->join('email_contents', 'email_campaign_title.id', '=', 'email_contents.email_campaign_title_id' )
            ->join('email_broadcasts', 'email_campaign_title.id', '=', 'email_broadcasts.email_campaign_title_id' )
            ->select(
                'email_campaign_title.user_id',
                'email_campaign_title.parent_id',
                'email_campaign_title.id',
                'email_campaign_title.email_category_id',
                'email_campaign_title.title',
                'email_campaign_title.created_at',
                'email_contents.subject_id',
                'email_broadcasts.boradcast_id as broadcast_id',
                'email_broadcasts.type as broad_resend',
                'email_broadcasts.status',
                'email_campaign_title.id as title_id',
                'email_contents.is_private',
                'email_broadcasts.group_id'
            )
            ->where('email_contents.user_id', Auth::User()->id)
            ->groupBy('email_broadcasts.group_id')
            ->get();

        $result = array(); //m final result
        $count = 0;
        foreach( $campaign_title as $value)
        {
            $count++;
            $title_ids = DB::table('email_broadcasts')->select('email_campaign_title_id')->where('group_id', $value->group_id)->get()->toArray(); // subject title

            $ids = array();

            foreach($title_ids as $id)
            {
                array_push($ids , $id->email_campaign_title_id);
            }

            $subject_title = DB::table('email_subjects')->where('id', $value->subject_id)->value('subject'); // subject title
            $category_title = DB::table('email_sub_category')->where('id', $value->email_category_id)->value('category'); // category title
            $total_opened = DB::table('autoresponder_new_stats')
                ->where('email', '!=', '')
                ->whereIn('email_campaign_title_id', $ids )
                ->where([ 'stat_type'=> 'open', 'subject_id' => $value->subject_id,  'email_category_id' => $value->email_category_id])
                ->count();
            $total_clicked = DB::table('autoresponder_new_stats')
                ->where(['stat_type'=> 'click', 'subject_id' => $value->subject_id, 'email_category_id' => $value->email_category_id])
                ->whereIn('email_campaign_title_id', $ids )
                ->count();
            $total_send_lists = DB::table('email_broadcasts')->where(['group_id' => $value->group_id])->count();

            $arr = array(
                'id' => $count,
                'title_id' => $value->title_id,
                'title' => $value->title,
                'subject_id' => $value->subject_id,
                'opened' => $total_opened,
                'clicked' => $total_clicked,
                'subject' => $subject_title,
                'category' => $category_title,
                'created_at' => $value->created_at,
                'is_private' => $value->is_private,
                'type' => $value->parent_id,
                'broad_resend' => $value->broad_resend,
                'status' => $value->status,
                'send_list_total' => $total_send_lists,
                'group_id' => $value->group_id
            );
            array_push($result, $arr);
        }

        return Datatables::of($result)
            ->addColumn('action', function ($result)
            {
                if ($result['is_private'] == 0)
                {
                    $status = "fa-unlock";
                    $title = 'Make private';
                }
                if ($result['is_private'] == 1)
                {
                    $status = "fa-lock";
                    $title = 'Make public';
                }

                $b='';
                if ($result['status'] != 1)
                {
                    $b .= '&nbsp;<a href="'. route('autoresponder-report','id='. $result['title_id']).'" class="btn btn-xs btn-primary"  data-id="'.$result["title_id"].'" data-toggle="tooltip" title="View Stats"><i class="material-icons">trending_up</i></a>&nbsp;';
                }
                $b .= '<button class="btn btn-xs btn-purple"  data-id="'.$result["subject_id"].'" data-toggle="tooltip" title="Use Me" onclick="submitUseMe(this)" ><i class="material-icons">mouse</i></button>';
                $b .= '&nbsp;<a onclick="makePrivate(this)" data-id="'.$result['title_id'].'" data-toggle="tooltip" data-toggle="tooltip" title="'.$title.'" class="btn btn-xs btn-pink"><i class="fa '.$status.' "></i></a>';
                $b .= '&nbsp;<a onclick="deleteThis(this)" data-id="'.$result['title_id'].'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';

                return $b;
            })

            ->editColumn('status', function($result)
            {
                 if ($result['status'] == 2)
                 {     $status = 'sent';  }
                 if ( $result['status'] == 0 )
                 {  $status = 'queued';  }
                 if ($result['status'] == 1)
                 {  $status = 'error'; }
                 return $status;
            })

            /*
            ->editColumn('send_list_total', function($result)
            {

                $html = '
                    <a href="javascript:void(0)" data-id="'.$result['group_id'].'">'.$result['send_list_total'].'</a>
                ';
                 return $html;
            })
            ->rawColumns(['send_list_total', 'action'])
            */

            ->make(true);
    }
    /*
     * can delete any record from any table
     * required id and obj(table name)
     * */
    public function delete(Request $request)
    {
        if ($request->id!='' && $request->obj!='')
        {

            $delete = 1;
            $msg = 'Deleted Successfully.';

            // delete_content
            if ($request->obj=='delete_list'){
                DB::table('autoresponder_lists')->where('list_id', $request->id)->delete();
                DB::table('autoresponder_contact')->where('list_id', $request->id)->delete();
            }


            // delete_content
            if ($request->obj=='delete_contact'){
                DB::table('autoresponder_contact')->where('id', $request->id)->delete();
            }
            if ($request->obj=='categories')
            {
                $check_usage = EmailTemplate::where('category_id', '=', $request->id)->first();

                if ($check_usage)
                {
                    $delete = 0;
                    $msg = 'Category is in use under Email Template. You cannot Delete it.';
                }else{
                    Categories::findOrFail($request->id)->delete();
                }
            }

            // subject
            if ($request->obj=='email_subjects')
            {
                $check_usage = EmailBroadcast::where('subject_id', '=', $request->id)->first();

                if ($check_usage)
                {
                    $delete = 0;
                    $msg = 'Email subject is in use under Auto Responder. You cannot Delete it.';
                }else{
                    SubjectTemplate::findOrFail($request->id)->delete();
                }
            }

            // subject
            if ($request->obj=='users')
            {
                $check_usage1 = SubjectTemplate::where('user_id', '=', $request->id)->first();
                $check_usage2 = EmailTemplate::where('user_id', '=', $request->id)->first();
                $acheck_usage3 = Aweber_Setting::where('user_id', '=', $request->id)->first();
                $acheck_usage4 = GetResponse_Setting::where('user_id', '=', $request->id)->first();
                $acheck_usage5 = Activecampaign_Setting::where('user_id', '=', $request->id)->first();
                $acheck_usage6 = MailChimp_Setting::where('user_id', '=', $request->id)->first();

                if ($check_usage1 || $check_usage2 || $acheck_usage3 || $acheck_usage4 || $acheck_usage5 || $acheck_usage6)
                {
                    $delete = 0;
                    $msg = 'User have some Email Templates and Subject Lines. If you delete that User these Templates will also be deleted.';
                }else{
                    $user = User::findOrFail($request->id)->delete();
                    //Do not turn ON
                    //as user deletes delete all related infos from database
                    /*if($user)
                    {
                        SubjectTemplate::where('user_id', '=', $request->id)->delete();
                        EmailTemplate::where('user_id', '=', $request->id)->delete();
                        EmailTemplate::where('user_id', '=', $request->id)->delete();
                    }*/
                }
            }

            //  email content
            if ($request->obj=='email_contents')
            {
                $obj = EmailTemplate::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }
            //  delete image from gallery
            if ($request->obj=='gallery')
            {
                $obj = Gallery::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }


            //  delete image from aweber_settings
            if ($request->obj=='aweber_settings')
            {
                $obj = Aweber_Setting::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }

            //  delete image from getresponse_settings
            if ($request->obj=='getresponse_settings')
            {
                $obj = GetResponse_Setting::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }

            //  delete image from mailchimp_settings
            if ($request->obj=='mailchimp_settings')
            {
                $obj = MailChimp_Setting::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }

             //  delete sendlane_settings api settings
            if ($request->obj=='sendlane_settings')
            {
                $obj = SendLane_Setting::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }
            //  delete Activecampaign_settings api settings
            if ($request->obj=='activecampaign_settings')
            {
                $obj = Activecampaign_Setting::findOrFail($request->id)->delete();
                if (!$obj)
                { $delete = 0; }
            }

            if ($request->obj == 'sparkpost_settings')
            {
                $delete = DB::table('email_sender')->where([ 'id' => $request->id, 'type' => 'sparkpost' ])->delete();
                if (!$delete)
                {
                    $delete = 0;
                }
            }

            if ($request->obj == 'sendgrid_settings')
            {
                $delete = DB::table('email_sender')->where([ 'id' => $request->id, 'type' => 'sendgrid' ])->delete();
                if (!$delete)
                {
                    $delete = 0;
                }
            }
            if ($request->obj == 'mailgun_settings')
            {
                $delete = DB::table('email_sender')->where([ 'id' => $request->id, 'type' => 'mailgun' ])->delete();
                if (!$delete)
                {
                    $delete = 0;
                }
            }
            if ($request->obj == 'smtp_settings')
            {
                $delete = DB::table('email_sender')->where([ 'id' => $request->id, 'type' => 'smtp' ])->delete();
                if (!$delete)
                {
                    $delete = 0;
                }
            }
            if ($request->obj == 'mailjet_settings')
            {
                $delete = DB::table('email_sender')->where([ 'id' => $request->id, 'type' => 'mailjet' ])->delete();
                if (!$delete)
                {
                    $delete = 0;
                }
            }


            if ($request->obj=='email_sub_category')
            {

                $email_content = EmailTemplate::where('email_category_id', $request->id)->value('id');
                if ($email_content)
                {
                    $delete = 0;
                    $msg = 'Category is used under other emails. To delete assign it other category!';
                }else{
                    $obj = Categories::where(['id' => $request->id, 'user_id' => Auth::user()->id])->delete();
                }
            }

            if ($delete==1)
            {
                echo '{"type":"success","msg":"'.$msg.'"}';
            }else{
                echo '{"type":"error","msg":"'.$msg.'"}';
            }
        }else{
            echo '0';
        }
    }


    /*
     * can change status of any record from any table
     * required id and obj(table name) and state(status)
     * */
    public function changeStatus(Request $request)
    {

        if ($request->id!='' && $request->obj!='' && $request->status!='')
        {
            if ($request->status=='false')
            {
                $status = 0;
            }else{
                $status = 1;
            }
            $is_status = 0;
            if ($request->obj=='users')
            {
                $obj = User::findOrFail($request->id);
                if ($obj)
                {
                    $is_status = 1;
                }
            }

            if ($request->obj=='categories')
            {
                $obj = Categories::findOrFail($request->id);
            }

            if ($request->obj=='email_subjects')
            {
                $obj = SubjectTemplate::findOrFail($request->id);
            }

            if ($request->obj=='email_contents')
            {
                $obj = EmailTemplate::findOrFail($request->id);
            }

            if ($obj)
            {
                $obj->status = $status;
                $obj->save();
                echo 1;

                // START email send on user status chane

                if ($obj)
                {

                    if ($is_status == 1)
                    {

                        $email_temp = DB::table('email_settings')->select('status_subject','status_content')->where('user_id', Auth::user()->id)->first();

                        $subject = $email_temp->status_subject;
                        $content = $email_temp->status_content;

                        $content = str_replace('%email%', $obj->email, $content);
                        $content = str_replace('%name%', $obj->name, $content);

                        if ($obj->status == 0)
                        {
                            $status = "Inactive";
                        }else{
                            $status = "Active";
                        }
                        $content = str_replace('%status%', $status, $content);
                        $content = str_replace('%password%', $obj->plain_password, $content);

                        $this->notification_to_email = $obj->email;
                        $this->notification_subject = $subject;
                        $data = array('email' => $obj->email, 'content' => $content, 'subject' => $subject );

                        Mail::send('admin.user.email_notification', $data, function($msg)
                        {
                            $msg->subject($this->notification_subject);
                            $msg->to($this->notification_to_email);
                        });


                    }
                }
                // END email send on user status chane

            }else{
                echo 0;
            }

        }
    }


    function deleteEmail(Request $request)
    {

        $content = EmailTemplate::where(['email_campaign_title_id' => $request->id, 'user_id' => Auth::user()->id ])->value('id');
        if ($content)
        {

            $del_campaign = DB::table('email_campaign_title')->where(['user_id' => Auth::user()->id, 'id' => $request->id ])->delete();

            $del_content = EmailTemplate::where([ 'email_campaign_title_id' => $request->id, 'user_id' => Auth::user()->id ])->delete();
            $del_broadcast = EmailBroadcast::where([ 'email_campaign_title_id' => $request->id, 'user_id' => Auth::user()->id ])->delete();
            $del_stats = DB::table('autoresponder_new_stats')->where([ 'user_id' => Auth::user()->id, 'email_campaign_title_id' => $request->id ])->delete();

            echo 1;
        }
    }
}
