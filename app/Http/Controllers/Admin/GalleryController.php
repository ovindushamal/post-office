<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use DB;
use Auth;
use Validator;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $your_gallery = Gallery::all()->where('user_id', Auth::User()->id);
        $gallery = Gallery::all()->where('user_id', "!=" , Auth::User()->id);
        return view("admin.gallery.add")->with('your_gallery', $your_gallery)->with('gallery', $gallery);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $keyword =  $request->keyword;
        $page =  $request->page;
        $url = "https://pixabay.com/api/?key=5207526-c0856218d67f25f2252e5e1bd&q=".$keyword."=all&pretty=true&order=popular&per_page=18&page=".$page."";

        $result = $this->curl($url, '','post');
        $result = json_decode($result);

        if (isset($result->totalHits) && $result->totalHits > 0)
        {
            $html = "";
            $count = 0;
            $param = "pixabay_";
            foreach ($result->hits as $value)
            { $count++;
                $html .= "
                <div class='col-sm-2 text-center'>
                    <img id='pixabay_".$count."' src='" . $value->webformatURL . "' alt='image' class='img-responsive img-rounded' />
                    <div id='".$count."' data-param='pixabay_' class='caption' onclick='load_image(this)' title='Insert Image'>
                        <a href='#' class='btn btn-info   btn-sm' role='button'><i class='fa fa-upload'></i></a>
                    </div>
                </div>
            ";
            }
            return $html;
        }else{
            return '
                <div class="alert alert-danger">Result not found!</div>';
            }
    }

        //browse gallery
        function browseGallery(Request $request)
        {

            $validator = Validator::make($request->all(), [
                'image' => 'required|mimes:jpeg,bmp,png'
            ]);
            if ($validator->fails()) {
                return response()->json(['msg' => 2, 'error'=> 'The image must be a file of type: jpeg, bmp, png.']);
            }



            $input = $request->all();
            $name =   $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(base_path() . '/assets/gallery/', $input['image']);
            //save to gallery table
            $obj = new Gallery();
            $obj->fill($request->all());
            $obj->images = $name;

            $obj->user_id = Auth::User()->id;
            if ($obj->save())
            {
                return response()->json(['msg' => 1, 'file' => '<img src="'.asset("assets/gallery/".$name."").'" >' ]);
               
            }
        }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function curl($url,$body,$method)
    {

        if (is_array($body))
            $body=http_build_query($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        if ($method == "post")
            curl_setopt($ch, CURLOPT_POST, true );
        if ($method == "get")
            curl_setopt($ch, CURLOPT_HTTPGET, true );
        if ($method=="put")
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if ($method=="delete")
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }

        if ($body!="")
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $filecontents=curl_exec($ch);

        curl_close($ch);

        return $filecontents;
    }
}
