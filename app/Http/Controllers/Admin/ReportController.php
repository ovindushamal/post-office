<?php

namespace App\Http\Controllers\Admin;

use App\Aweber_Setting;
use App\EmailBroadcast;
use App\EmailTemplate;
use App\GetResponse_Setting;
use App\Activecampaign_Setting;
use App\SendLane_Setting;
use App\MailChimp_Setting;
use App\Settings;
use App\SubjectTemplate;
use App\UsedAutoResponder;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Categories;
use DB;
use Auth;
use Mailchimp\Mailchimp;
use Illuminate\Support\Facades\Config;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('autoresponder_new_stats')->select('*')->where('user_id', '=', Auth::user()->id)->get();

        // autoresponder stats
        $mailchimp_opened =  $mailchimp_clicked = $get_response_opened = $get_response_clicked = $activecampaign_clicked = $activecampaign_opened = 0;

        // fav open time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
            ->where('date_time', '!=','')
            ->where('user_id', '=', Auth::user()->id)
            ->where('stat_type', '=', 'open')
            ->get();
        $fav = array();
        foreach($fav_raw as $value)
        {
            $fav[] =  $value->open_time;
        }

        if (count($fav) > 0) {
            $c = array_count_values($fav);
            $fav_open_time = array_search(max($c), $c);
        }else{
            $fav_open_time = '0:0';
        }

        // fav click time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as click_time"))
            ->where('date_time', '!=','')
            ->where('user_id', '=', Auth::user()->id)
            ->where('stat_type', '=', 'click')
            ->get();
        $fav = array();
        foreach($fav_raw as $value)
        {
            $fav[] =  $value->click_time;
        }

        if (count($fav) > 0) {
            $c = array_count_values($fav);
            $fav_click_time = array_search(max($c), $c);
        }else{
            $fav_click_time = '0:0';
        }

        return view('report', compact(

                'fav_click_time',
                'fav_open_time',
                'data'
            )
        );
    }

    function show($id)
    {
        //
    }

    function userAgentStats($title_id, $stat_type, $like, $type=false)
    {

        $result = DB::table('autoresponder_new_stats');
        $result = $result->select('id');
        $result = $result->where(['user_id' => Auth::user()->id, 'stat_type' => $stat_type]);
        $result = $result->whereIn('email_campaign_title_id', $title_id);
        $result = $result->whereRaw('user_agent LIKE "%'.$like.'%" ');
        $result = $result->where('subject_id', '!=', '');
        //$result = $result->groupBy('email');

        if ($type) {
            $result = $result->where('autoresponder_new_stats.type', '=', $type);
        }

        $result = $result->count('id');
        return $result;
    }

    function typewiseReport( Request $request)
    {
        $title_id = $request->id;
        // fetching group
        $group_id = DB::table('email_broadcasts')->where('email_campaign_title_id', $title_id)->value('group_id');
        $title_ids = DB::table('email_broadcasts')->select('email_campaign_title_id')->where('group_id', $group_id)->get()->toArray();

        $id = array();
        foreach($title_ids as $ids)
        {
            array_push($id , $ids->email_campaign_title_id);
        }

        $autoresponder_type = $request->type;
        $type = $request->type;

        // total opens about title

        $open = DB::table('autoresponder_new_stats')
            ->join('email_broadcasts', 'autoresponder_new_stats.email_campaign_title_id', '=', 'email_broadcasts.email_campaign_title_id')
            ->where('autoresponder_new_stats.stat_type', '=', 'open')
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            ->where('autoresponder_new_stats.user_id', '=', Auth::user()->id)
            ->where('autoresponder_new_stats.type', '=', $autoresponder_type)
            ->where('autoresponder_new_stats.email', '!=', '')
            ->where('autoresponder_new_stats.subject_id', '!=', '')
            //->groupBy('autoresponder_new_stats.email')
            ->count();

        $click = DB::table('autoresponder_new_stats')
            ->join('email_broadcasts', 'autoresponder_new_stats.email_campaign_title_id', '=', 'email_broadcasts.email_campaign_title_id')
            ->where('autoresponder_new_stats.stat_type', '=', 'click')
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            ->where('autoresponder_new_stats.user_id', '=', Auth::user()->id)
            ->where('autoresponder_new_stats.type', '=', $autoresponder_type)
            ->count();
        // #############################    fav times  #######################//
        // fav open time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
            ->where('date_time', '!=','')
            ->where('email', '!=','')
            ->where('subject_id', '!=','')
            ->whereIn('email_campaign_title_id', $id)
            ->where(
                [
                    'user_id'=> Auth::user()->id,
                    'stat_type' => 'open',
                    'type' => $type
                ]
            )
            ->get();
        $fav = array();
        foreach($fav_raw as $value)
        {
            $fav[] =  $value->open_time;
        }

        if (count($fav) > 0) {
            $c = array_count_values($fav);
            $fav_open_time = array_search(max($c), $c);
        }else{
            $fav_open_time = '0:0';
        }

        // fav click time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as click_time"))
            ->where('date_time', '!=','')
            ->whereIn('email_campaign_title_id', $id)
            ->where(
                [
                    'user_id'=> Auth::user()->id,
                    'stat_type' => 'click',
                    'type' => $type
                ]
            )
            ->get();
        $fav = array();
        foreach($fav_raw as $value)
        {
            $fav[] =  $value->click_time;
        }

        if (count($fav) > 0) {
            $c = array_count_values($fav);
            $fav_click_time = array_search(max($c), $c);
        }else{
            $fav_click_time = '0:0';
        }


        if (isset($id) && isset($autoresponder_type) && $id != "" && $autoresponder_type != "") {

            $data = DB::table('email_campaign_title')
                ->select('title')
                ->whereIn('id', $id)
                ->where(['user_id' => Auth::user()->id])
                ->first();

            $user_agent_subject = $data->title;
            // pc open
            $pc_open = $this->userAgentStats( $id, 'open', 'Windows', $autoresponder_type);
            // pc click
            $pc_click = $this->userAgentStats( $id, 'click', 'Windows', $autoresponder_type);
            // android open
            $android_open = $this->userAgentStats( $id, 'open', 'Android', $autoresponder_type);
            // android click
            $android_click = $this->userAgentStats( $id, 'click', 'Android', $autoresponder_type);
            // iPhone open
            $iPhone_open = $this->userAgentStats( $id, 'open', 'iPhone', $autoresponder_type);
            // iPhone click
            $iPhone_click = $this->userAgentStats( $id, 'click', 'iPhone', $autoresponder_type);
            // iPad open
            $iPad_open = $this->userAgentStats( $id, 'open', 'iPad', $autoresponder_type);
            // iPad click
            $iPad_click = $this->userAgentStats( $id, 'click', 'iPad', $autoresponder_type);
            // Macintosh open
            $Macintosh_open = $this->userAgentStats( $id, 'open', 'Macintosh', $autoresponder_type);
            // Macintosh click
            $Macintosh_click = $this->userAgentStats( $id, 'click', 'Macintosh', $autoresponder_type);

            $totalOpenByUserAgent = $pc_open + $android_open + $iPhone_open + $iPad_open + $Macintosh_open;

            $totalClickByUserAgent = $pc_click + $android_click + $iPhone_click + $iPad_click + $Macintosh_click;
            $other_open = $open - $totalOpenByUserAgent;
            $other_click = $click - $totalClickByUserAgent;

            $user_agent = array(
                'subject' => $user_agent_subject,
                'pc_open' => $pc_open,
                'pc_click' => $pc_click,
                'android_open' => $android_open,
                'android_click' => $android_click,
                'iPhone_open' => $iPhone_open,
                'iPhone_click' => $iPhone_click,
                'iPad_open' => $iPad_open,
                'iPad_click' => $iPad_click,
                'Macintosh_open' => $Macintosh_open,
                'Macintosh_click' => $Macintosh_click,
                'other_open' => $other_open,
                'other_click' => $other_click
            );
            // user agent create chart json

            $user_agent_open = "[";
            $user_agent_click = "[";

            $agent_label = array();

            if ($user_agent['pc_open'] != 0 || $user_agent['pc_click'] != 0 ) {

                $user_agent_open .= '{label: "Windows", value: '.$user_agent['pc_open'].' },';
                $user_agent_click .= '{label: "Windows", value: '.$user_agent['pc_click'].' },';

                array_push($agent_label, ['color' => 'pink', 'agent' => 'Windows' ]);
            }

            if ($user_agent['android_open'] != 0 || $user_agent['android_click'] != 0 ) {
                $user_agent_open .= '{label: "Andriod", value: '.$user_agent['android_open'].' },';
                $user_agent_click .= '{label: "Andriod", value: '.$user_agent['android_click'].' },';

                array_push($agent_label, ['color' => 'green', 'agent' => 'Andriod' ]);
            }

            if ($user_agent['iPhone_open'] != 0 || $user_agent['iPhone_click'] != 0 ) {

                $user_agent_open .= '{label: "iPhone", value: '.$user_agent['iPhone_open'].' },';
                $user_agent_click .= '{label: "iPhone", value: '.$user_agent['iPhone_click'].' },';

                array_push($agent_label, ['color' => 'blue', 'agent' => 'iPhone' ]);
            }

            if ($user_agent['iPad_open'] != 0 || $user_agent['iPad_click'] != 0 ) {
                $user_agent_open .= '{label: "iPad", value: '.$user_agent['iPad_open'].' },';
                $user_agent_click .= '{label: "iPad", value: '.$user_agent['iPad_click'].' },';

                array_push($agent_label, ['color' => 'yellow', 'agent' => 'iPad' ]);
            }

            if ($user_agent['Macintosh_open'] != 0 || $user_agent['Macintosh_click'] != 0 ) {
                $user_agent_open .= '{label: "Macintosh", value: '.$user_agent['Macintosh_open'].' },';
                $user_agent_click .= '{label: "Macintosh", value: '.$user_agent['Macintosh_click'].' },';

                array_push($agent_label, ['color' => 'red', 'agent' => 'Macintosh' ]);
            }

            if ($user_agent['other_open'] != 0 || $user_agent['other_click'] != 0 ) {
                $user_agent_open .= '{label: "Other", value: '.$user_agent['other_open'].' },';
                $user_agent_click .= '{label: "Other", value: '.$user_agent['other_click'].' },';

                array_push($agent_label, ['color' => 'black', 'agent' => 'Other' ]);
            }

            $user_agent_open = rtrim($user_agent_open, ',');
            $user_agent_open = $user_agent_open.']';

            $user_agent_click = rtrim($user_agent_click, ',');
            $user_agent_click = $user_agent_click.']';

            if ( $user_agent_open == '[]') {
                $user_agent_open = '[{label: "No open found yet", value: 0}]';
            }

            if ( $user_agent_click == '[]') {
                $user_agent_click = '[{label: "No click found yet", value: 0}]';
            }

            // ###############  CHART DATA
            $current_date = date("Y-m-d H:i:s");
            $last24 = date("Y-m-d H:i:s", strtotime('-24 hours', time()));

            $open_raw = DB::table('autoresponder_new_stats')
                //->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
                ->select(DB::raw(" date_format(date_time, '%H') as open_time"))
                ->where('date_time', '!=', '')
                ->where('user_id', '=', Auth::user()->id)
                ->where('stat_type', '=', 'open')
                ->where('email', '!=', '')
                ->whereIn('email_campaign_title_id', $id)
                ->where('type', '=', $autoresponder_type)
                ->where('subject_id', '!=', '')
                ->whereBetween('date_time', [$last24, $current_date])
                //->groupBy('email')
                ->get();

            $click_raw = DB::table('autoresponder_new_stats')
                //->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
                ->select(DB::raw(" date_format(date_time, '%H') as click_time"))
                ->where('date_time', '!=', '')
                ->where('user_id', '=', Auth::user()->id)
                ->where('stat_type', '=', 'click')
                ->whereIn('email_campaign_title_id', $id)
                ->where('type', '=', $autoresponder_type)
                ->whereBetween('date_time', [$last24, $current_date])
                ->get();

            //print_r($open_raw);
            //print_r($click_raw);

            $hours = array(
                '00' => array("open" => 0, "click" => 0),
                '01' => array("open" => 0, "click" => 0),
                '02' => array("open" => 0, "click" => 0),
                '03' => array("open" => 0, "click" => 0),
                '04' => array("open" => 0, "click" => 0),
                '05' => array("open" => 0, "click" => 0),
                '06' => array("open" => 0, "click" => 0),
                '07' => array("open" => 0, "click" => 0),
                '08' => array("open" => 0, "click" => 0),
                '09' => array("open" => 0, "click" => 0),
                '10' => array("open" => 0, "click" => 0),
                '11' => array("open" => 0, "click" => 0),
                '12' => array("open" => 0, "click" => 0),
                '13' => array("open" => 0, "click" => 0),
                '14' => array("open" => 0, "click" => 0),
                '15' => array("open" => 0, "click" => 0),
                '16' => array("open" => 0, "click" => 0),
                '17' => array("open" => 0, "click" => 0),
                '18' => array("open" => 0, "click" => 0),
                '19' => array("open" => 0, "click" => 0),
                '20' => array("open" => 0, "click" => 0),
                '21' => array("open" => 0, "click" => 0),
                '22' => array("open" => 0, "click" => 0),
                '23' => array("open" => 0, "click" => 0),
            );

            $fav_open = array();
            foreach ($open_raw as $value)
            {
                $fav_open[] = $value->open_time;
            }
            //print_r($fav_open);
            if (count($fav_open) > 0) {
                $result_open = array_count_values($fav_open);
                foreach ($result_open as $k1 => $v1)
                {
                    foreach ($hours as $k2 => $v2)
                    {
                        if ($k1 == $k2) {
                            $hours[$k1]['open'] = $result_open[$k1];
                        }
                    }
                }
            }
            //print_r($hours);
            //######################    CLICKS #####################
            $fav_click = array();
            foreach ($click_raw as $value)
            {
                $fav_click[] = $value->click_time;
            }
//        print_r($fav_click);

            if (count($fav_click) > 0) {
                $result_click = array_count_values($fav_click);
                foreach ($result_click as $k1 => $v1)
                {
                    foreach ($hours as $k2 => $v2)
                    {
                        if ($k1 == $k2) {
                            $hours[$k1]['click'] = $result_click[$k1];
                        }
                    }
                }
            }
//        print_r($hours);//
//
//        exit;
            //josn here ...
            $json = "[";
            $open_chart = 0;
            $click_chart = 0;
            $today = date("Y-m-d");
            foreach ($hours as $key => $value)
            {
                $open_chart = $value['open'];
                $click_chart = $value['click'];
                $json .= "{ hour: '" . $today . " " . $key . ":00', a: " . $open_chart . " , b: " . $click_chart . "},";
            }
            $chart = rtrim($json, ',');
            $chart = $chart . "]";

            // END GRAPH TRACK


            // country wise open click tracking

            $country = DB::table('autoresponder_new_stats')
                ->select('country')
                ->where('user_id', Auth::user()->id)
                ->whereIn('email_campaign_title_id', $id)
                ->where('subject_id', '!=', '')
                ->where('email', '!=', '')
                ->where('country', '!=', '')
                ->whereNotNull('email')
                ->where('type', '=', $autoresponder_type)
                ->distinct()
                ->get();

            $countries = array();
            foreach ($country as $value)
            {
                $total_opened = DB::table('autoresponder_new_stats')
                    ->where('subject_id', '!=', '')
                    ->where('email', '!=', '')
                    ->where(['stat_type' => 'open', 'country' => $value->country, 'type' => $autoresponder_type])
                    ->whereIn( 'email_campaign_title_id', $id )
                    //->groupby('email')
                    ->count();
                $total_clicked = DB::table('autoresponder_new_stats')
                    ->where(['stat_type' => 'click', 'country' => $value->country, 'type' => $autoresponder_type])
                    ->whereIn( 'email_campaign_title_id', $id )
                    ->count();

                array_push($countries, [$value->country => [
                    'open' => $total_opened,
                    'click' => $total_clicked,
                    //'flag' => $flagapi[0]->flag
                ]]);
            }

            // world map json
            $wMap = array();
            foreach ($country as $value)
            {
                $country_stat = DB::table('autoresponder_new_stats')
                    ->select('country', 'lati', 'longi')
                    ->where(['country' => $value->country])
                    ->first();
                $total_opened = DB::table('autoresponder_new_stats')
                    ->where('email', '!=', '')
                    ->where('subject_id', '!=', '')
                    ->where(['stat_type' => 'open', 'country' => $value->country, 'type'=> $autoresponder_type])
                    ->whereIn( 'email_campaign_title_id', $id )
                    //->groupBy('email')
                    ->count();
                $total_clicked = DB::table('autoresponder_new_stats')
                    ->where(['stat_type' => 'click', 'country' => $value->country, 'type'=> $autoresponder_type])
                    ->whereIn( 'email_campaign_title_id', $id )
                    ->count();
                array_push($wMap, [$country_stat->country => ['open' => $total_opened, 'click' => $total_clicked, 'lati' => $country_stat->lati, 'longi' => $country_stat->longi]]);
            }
            $world_json = '[';
            foreach ($wMap as $key => $value)
            {
                foreach ($value as $index => $item)
                {
                    $world_json .= "{
                latLng : [" . $item['lati'] . ", " . $item['longi'] . "],
                name : '" . $item['open'] . " opens & " . $item['click'] . " clicks occurred in " . $index . " '
                },";
                }
            }
            $world_json = rtrim($world_json, ',');
            $world_json = $world_json . ']';

            return view('type_wise_report',
                compact(
                    'fav_open_time',
                    'fav_click_time',
                    'data',
                    'open',
                    'click',
                    'chart',
                    'countries',
                    'world_json',
                    'user_agent_open',
                    'user_agent_click',
                    'agent_label',
                    'type'
                )
            );
        }else{
            return redirect()->back();
        }
    }

    function reportById( Request $request)
    {

        if (!isset($request->id)) {
            return redirect()->back();
            exit;
        }

        $title_id = $request->id;

        // fetching group
        $group_id = DB::table('email_broadcasts')->where('email_campaign_title_id', $title_id)->value('group_id');
        $title_ids = DB::table('email_broadcasts')->select('email_campaign_title_id')->where('group_id', $group_id)->get()->toArray();

        $id = array();
        foreach($title_ids as $ids)
        {
            array_push($id , $ids->email_campaign_title_id);
        }


        // #############################    fav times  #######################//

        // fav open time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
            ->where('date_time', '!=','')
            ->where('email', '!=','')
            ->where('subject_id', '!=','')
            ->where([ 'user_id'=> Auth::user()->id, 'stat_type' => 'open'])
            ->whereIn('email_campaign_title_id', $id )
            ->get();

        $fav = array();
        foreach($fav_raw as $value)
        {
            $fav[] =  $value->open_time;
        }

        if (count($fav) > 0) {
            $c = array_count_values($fav);
            $fav_open_time = array_search(max($c), $c);
        }else{
            $fav_open_time = '0:0';
        }

        // fav click time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as click_time"))
            ->where('date_time', '!=','')
            ->where([ 'user_id'=> Auth::user()->id, 'stat_type' => 'click'])
            ->whereIn('email_campaign_title_id', $id )
            ->get();
        $fav = array();
        foreach($fav_raw as $value)
        {
            $fav[] =  $value->click_time;
        }


        if (count($fav) > 0) {
            $c = array_count_values($fav);
            $fav_click_time = array_search(max($c), $c);
        }else{
            $fav_click_time = '0:0';
        }
        // list wise report

        $listWiseEmails = DB::table('campaign_send_list')
            ->join('autoresponder_lists', 'campaign_send_list.list_id', 'autoresponder_lists.list_id')
            ->select(
                'autoresponder_lists.user_id',
                'campaign_send_list.type',
                'campaign_send_list.list_id',
                'campaign_send_list.type',
                'campaign_send_list.account_id',
                'autoresponder_lists.list_name'
            )
            ->where(['autoresponder_lists.user_id' => Auth::user()->id])
            ->whereIn('campaign_send_list.campaign_title_id', $id)
            //->whereNotIn('campaign_send_list.type', ['activecampaign', 'get_response'])
            ->groupBy('campaign_send_list.list_id')
            ->get();


        $net = $count = 0;
        $openClickByList = array();
        foreach($listWiseEmails as $item)
        { $count++;

            $total_open_list = DB::table('campaign_send_list')
                ->join('autoresponder_new_stats', 'campaign_send_list.campaign_title_id', 'autoresponder_new_stats.email_campaign_title_id')
                ->where(['autoresponder_new_stats.user_id' => Auth::user()->id])
                ->where('autoresponder_new_stats.email', '!=', '')
                ->where('autoresponder_new_stats.subject_id', '!=', "")
                ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
                ->where('campaign_send_list.list_id', $item->list_id)
                ->where('autoresponder_new_stats.stat_type', 'open')
                ->count('campaign_send_list.id');
            //$net = $net+ $total_open_list;
            $total_click_list = DB::table('campaign_send_list')
                ->join('autoresponder_new_stats', 'campaign_send_list.campaign_title_id', 'autoresponder_new_stats.email_campaign_title_id')
                ->where(['autoresponder_new_stats.user_id' => Auth::user()->id])
                ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
                ->where('campaign_send_list.list_id', $item->list_id)
                ->where('autoresponder_new_stats.stat_type', 'click')
                ->count('campaign_send_list.id');

            array_push($openClickByList, ['title' => $item->list_name,  'click' => $total_click_list, 'opened' => $total_open_list ]);
        }

        // end favourite time calculations

        // unopen emails
        $Emails = DB::table('campaign_send_list')
            ->join('autoresponder_new_stats', 'campaign_send_list.campaign_title_id', 'autoresponder_new_stats.email_campaign_title_id')
            ->select(
                'autoresponder_new_stats.email',
                'autoresponder_new_stats.email_category_id',
                'autoresponder_new_stats.email_campaign_title_id',
                'campaign_send_list.type',
                'campaign_send_list.list_id',
                'campaign_send_list.type as autores_type',
                'campaign_send_list.account_id'
            )
            ->where(['autoresponder_new_stats.user_id' => Auth::user()->id])
            ->where('autoresponder_new_stats.email', '!=', '')
            ->where('autoresponder_new_stats.subject_id', '!=', "")
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            //->groupBy('autoresponder_new_stats.email')
            ->where('autoresponder_new_stats.stat_type', 'open')
            ->get();



        $unOpenEmails = $list_id = $email = $type = $account_id = array();

        foreach($Emails as $value)
        {

            array_push($list_id, $value->list_id);
            array_push($email,$value->email);
            array_push($type, $value->autores_type);
            array_push($account_id, $value->account_id);
        }

        $list_id = array_unique( $list_id );
        $email = array_unique( $email );
        $type = array_unique( $type );
        $account_id = array_unique( $account_id );

        $data = DB::table('autoresponder_contact')
            ->whereIn('list_id', $list_id )
            ->whereIn('account_id',$account_id )
            ->whereIn('type', $type )
            ->whereNotIn('email', $email )
            ->where(['user_id' => Auth::user()->id] )
            ->groupBy('email')
            ->get();


        foreach($data as $item)
        {
            array_push( $unOpenEmails, [ 'ids' => $item->id, 'email_category' => $value->email_category_id, 'title_id' => $value->email_campaign_title_id ]);
        }

        //$unOpenEmails = rtrim($unOpenEmails, ',');


        // autoresponders type
        $type = DB::table('autoresponder_new_stats')
            ->select('type', 'email_campaign_title_id')
            ->where([ 'user_id' => Auth::user()->id ])
            ->whereIn('email_campaign_title_id', $id)
            ->where('email', '!=', '')
            ->where('type', '!=', '')
            ->where('subject_id', '!=', '')
            ->groupBy('type')
            ->distinct('email')
            ->get();


        $autoresponder_type = array();
        foreach( $type as $value)
        {

            $total_opened = DB::table('autoresponder_new_stats')->where('subject_id', '!=', '')
                ->where('email', '!=', '')
                ->where(['stat_type' => 'open', 'type'=> $value->type])->distinct('email')
                ->whereIn('email_campaign_title_id', $id)
                ->count();
            $total_clicked = DB::table('autoresponder_new_stats')
                ->where(['stat_type' => 'click', 'type'=> $value->type])
                ->whereIn('email_campaign_title_id', $id)
                ->count();
            array_push($autoresponder_type, [ 'id' => $value->email_campaign_title_id, 'type' => $value->type, 'open' => $total_opened, 'click' => $total_clicked ]);
        }

        // total open and clicks
        // total opens about title

        $open = DB::table('autoresponder_new_stats')

            ->join('email_broadcasts', 'autoresponder_new_stats.email_campaign_title_id', '=', 'email_broadcasts.email_campaign_title_id')
            ->where('autoresponder_new_stats.stat_type', '=', 'open')
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            ->where('autoresponder_new_stats.user_id', '=', Auth::user()->id)
            ->where('autoresponder_new_stats.email', '!=', '')
            ->where('autoresponder_new_stats.subject_id', '!=', "")
            ->whereColumn('email_broadcasts.boradcast_id', 'autoresponder_new_stats.email_brodcast_id' )
            //->groupBy('autoresponder_new_stats.email')
            ->count();

        $click = DB::table('autoresponder_new_stats')
            ->join('email_broadcasts', 'autoresponder_new_stats.email_campaign_title_id', '=', 'email_broadcasts.email_campaign_title_id')
            ->where('autoresponder_new_stats.stat_type', '=', 'click')
            ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
            ->where('autoresponder_new_stats.user_id', '=', Auth::user()->id)
            // ->where('autoresponder_new_stats.type', '=', $autoresponder_type)
            ->count();
        //end total and clicks

        $data = DB::table('email_campaign_title')
            ->select('title')
            ->where([ 'user_id' => Auth::user()->id ])
            ->whereIn('id', $id)
            ->first();

        $user_agent_subject = $data->title;
        // pc open
        $pc_open = $this->userAgentStats( $id, 'open', 'Windows');
        // pc click
        $pc_click = $this->userAgentStats( $id, 'click', 'Windows');
        // android open
        $android_open = $this->userAgentStats( $id, 'open', 'Android');
        // android click
        $android_click = $this->userAgentStats( $id, 'click', 'Android');
        // iPhone open
        $iPhone_open = $this->userAgentStats( $id, 'open', 'iPhone');
        // iPhone click
        $iPhone_click = $this->userAgentStats( $id, 'click', 'iPhone');
        // iPad open
        $iPad_open = $this->userAgentStats( $id, 'open', 'iPad');
        // iPad click
        $iPad_click = $this->userAgentStats( $id, 'click', 'iPad');
        // Macintosh open
        $Macintosh_open = $this->userAgentStats( $id, 'open', 'Macintosh');
        // Macintosh click
        $Macintosh_click = $this->userAgentStats( $id, 'click', 'Macintosh');

        $totalOpenByUserAgent = $pc_open + $android_open + $iPhone_open + $iPad_open + $Macintosh_open;

        $totalClickByUserAgent = $pc_click + $android_click + $iPhone_click + $iPad_click + $Macintosh_click;
        $other_open = $open - $totalOpenByUserAgent;
        $other_click = $click - $totalClickByUserAgent;

        $user_agent = array(
            'subject' => $user_agent_subject,
            'pc_open' => $pc_open,
            'pc_click' => $pc_click,
            'android_open' => $android_open,
            'android_click' => $android_click,
            'iPhone_open' => $iPhone_open,
            'iPhone_click' => $iPhone_click,
            'iPad_open' => $iPad_open,
            'iPad_click' => $iPad_click,
            'Macintosh_open' => $Macintosh_open,
            'Macintosh_click' => $Macintosh_click,
            'other_open' => $other_open,
            'other_click' => $other_click
        );
        // user agent create chart json

        $user_agent_open = "[";
        $user_agent_click = "[";

        $agent_label = array();

        if ($user_agent['pc_open'] != 0 || $user_agent['pc_click'] != 0 ) {

            $user_agent_open .= '{label: "Windows", value: '.$user_agent['pc_open'].' },';
            $user_agent_click .= '{label: "Windows", value: '.$user_agent['pc_click'].' },';

            array_push($agent_label, ['color' => 'pink', 'agent' => 'Windows' ]);
        }
        if ($user_agent['android_open'] != 0 || $user_agent['android_click'] != 0 ) {
            $user_agent_open .= '{label: "Andriod", value: '.$user_agent['android_open'].' },';
            $user_agent_click .= '{label: "Andriod", value: '.$user_agent['android_click'].' },';

            array_push($agent_label, ['color' => 'green', 'agent' => 'Andriod' ]);
        }
        if ($user_agent['iPhone_open'] != 0 || $user_agent['iPhone_click'] != 0 ) {

            $user_agent_open .= '{label: "iPhone", value: '.$user_agent['iPhone_open'].' },';
            $user_agent_click .= '{label: "iPhone", value: '.$user_agent['iPhone_click'].' },';

            array_push($agent_label, ['color' => 'blue', 'agent' => 'iPhone' ]);
        }
        if ($user_agent['iPad_open'] != 0 || $user_agent['iPad_click'] != 0 ) {
            $user_agent_open .= '{label: "iPad", value: '.$user_agent['iPad_open'].' },';
            $user_agent_click .= '{label: "iPad", value: '.$user_agent['iPad_click'].' },';

            array_push($agent_label, ['color' => 'yellow', 'agent' => 'iPad' ]);
        }
        if ($user_agent['Macintosh_open'] != 0 || $user_agent['Macintosh_click'] != 0 ) {
            $user_agent_open .= '{label: "Macintosh", value: '.$user_agent['Macintosh_open'].' },';
            $user_agent_click .= '{label: "Macintosh", value: '.$user_agent['Macintosh_click'].' },';

            array_push($agent_label, ['color' => 'red', 'agent' => 'Macintosh' ]);
        }

        if ($user_agent['other_open'] != 0 || $user_agent['other_click'] != 0 ) {
            $user_agent_open .= '{label: "Other", value: '.$user_agent['other_open'].' },';
            $user_agent_click .= '{label: "Other", value: '.$user_agent['other_click'].' },';

            array_push($agent_label, ['color' => 'black', 'agent' => 'Other' ]);
        }


        $user_agent_open = rtrim($user_agent_open, ',');
        $user_agent_open = $user_agent_open.']';

        $user_agent_click = rtrim($user_agent_click, ',');
        $user_agent_click = $user_agent_click.']';

        if ( $user_agent_open == '[]') {
            $user_agent_open = '[{label: "No open found yet", value: 0}]';
        }

        if ( $user_agent_click == '[]') {
            $user_agent_click = '[{label: "No click found yet", value: 0}]';
        }
        // end user agent code
//exit;
        // ###############  CHART DATA

        $current_date =  date("Y-m-d H:i:s");
        $last24 = date("Y-m-d H:i:s", strtotime('-24 hours', time()));

        $open_raw = DB::table('autoresponder_new_stats')
            //->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
            ->select(DB::raw(" date_format(date_time, '%H') as open_time"))
            ->where('date_time', '!=','')
            ->where('user_id', '=', Auth::user()->id)
            ->where('stat_type', '=', 'open')
            ->whereIn('email_campaign_title_id', $id)
            ->where('email', '!=', '')
            ->where('subject_id', '!=', '')
            ->whereBetween('date_time', [$last24, $current_date])
            //->groupBy('email')
            ->get();

        $click_raw = DB::table('autoresponder_new_stats')
            //->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
            ->select(DB::raw(" date_format(date_time, '%H') as click_time"))
            ->where('date_time', '!=','')
            ->where('user_id', '=', Auth::user()->id)
            ->where('stat_type', '=', 'click')
            ->whereIn('email_campaign_title_id', $id)
            //->where('type', '=', $autoresponder_type)
            ->whereBetween('date_time', [$last24, $current_date])
            ->get();

        //print_r($open_raw);
        //print_r($click_raw);

        $hours = array(
            '00' => array("open"=>0,"click"=>0),
            '01' => array("open"=>0,"click"=>0),
            '02' => array("open"=>0,"click"=>0),
            '03' => array("open"=>0,"click"=>0),
            '04' => array("open"=>0,"click"=>0),
            '05' => array("open"=>0,"click"=>0),
            '06' => array("open"=>0,"click"=>0),
            '07' => array("open"=>0,"click"=>0),
            '08' => array("open"=>0,"click"=>0),
            '09' => array("open"=>0,"click"=>0),
            '10' => array("open"=>0,"click"=>0),
            '11' => array("open"=>0,"click"=>0),
            '12' => array("open"=>0,"click"=>0),
            '13' => array("open"=>0,"click"=>0),
            '14' => array("open"=>0,"click"=>0),
            '15' => array("open"=>0,"click"=>0),
            '16' => array("open"=>0,"click"=>0),
            '17' => array("open"=>0,"click"=>0),
            '18' => array("open"=>0,"click"=>0),
            '19' => array("open"=>0,"click"=>0),
            '20' => array("open"=>0,"click"=>0),
            '21' => array("open"=>0,"click"=>0),
            '22' => array("open"=>0,"click"=>0),
            '23' => array("open"=>0,"click"=>0),
        );

        $fav_open = array();
        foreach($open_raw as $value)
        {
            $fav_open[] =  $value->open_time;
        }
        //print_r($fav_open);
        if (count($fav_open) > 0) {
            $result_open = array_count_values($fav_open);
            foreach ($result_open as $k1 => $v1)
            {
                foreach ($hours as $k2 => $v2)
                {
                    if ($k1 == $k2) {
                        $hours[$k1]['open'] = $result_open[$k1];
                    }
                }
            }
        }
        //print_r($hours);
        //######################    CLICKS #####################
        $fav_click = array();
        foreach($click_raw as $value)
        {
            $fav_click[] =  $value->click_time;
        }
//        print_r($fav_click);

        if (count($fav_click) > 0) {
            $result_click = array_count_values($fav_click);
            foreach ($result_click as $k1 => $v1)
            {
                foreach ($hours as $k2 => $v2)
                {
                    if ($k1 == $k2) {
                        $hours[$k1]['click'] = $result_click[$k1];
                    }
                }
            }
        }
//        print_r($hours);//
//
//        exit;
        //josn here ...
        $json = "[";
        $open_chart = 0;
        $click_chart = 0;
        $today = date("Y-m-d");
        foreach($hours as $key => $value)
        {
            $open_chart = $value['open'];
            $click_chart = $value['click'];
            $json .= "{ hour: '".$today." ".$key.":00', a: ".$open_chart." , b: ".$click_chart."},";
        }
        $chart =  rtrim($json,',');
        $chart = $chart."]";

        // END GRAPH TRACK
        // country wise open click tracking

        $country = DB::table('autoresponder_new_stats')
            ->select('country')
            ->where('user_id', Auth::user()->id)
            ->whereIn('email_campaign_title_id', $id)
            ->where('subject_id', '!=', '')
            ->where('email', '!=', '')
            ->where('country', '!=', '')
            ->whereNotNull('email')
            ->distinct()
            ->get();

        $countries = array();
        foreach( $country as $value )
        {
            $total_opened = DB::table('autoresponder_new_stats')
                ->where('subject_id', '!=', '')
                ->where('email', '!=', '')
                ->where(['stat_type'=> 'open', 'country' => $value->country])
                ->whereIn( 'email_campaign_title_id', $id )
                //->groupBy('email')
                ->count();
            $total_clicked = DB::table('autoresponder_new_stats')
                ->where(['stat_type'=> 'click', 'country' => $value->country])
                ->whereIn( 'email_campaign_title_id', $id)
                ->count();

            array_push( $countries,  [ $value->country => [
                'open' => $total_opened,
                'click' => $total_clicked,
                //'flag' => $flagapi[0]->flag
            ] ]  );
        }



        // world map json
        $wMap = array();
        foreach( $country as $value )
        {
            $country_stat = DB::table('autoresponder_new_stats')->select('country', 'lati', 'longi')->where(['country' => $value->country])->first();
            $total_opened = DB::table('autoresponder_new_stats')
                ->where('subject_id', '!=', '')
                ->where('email', '!=', '')
                ->where(['stat_type'=> 'open', 'country' => $value->country])
                ->whereIn( 'email_campaign_title_id', $id)
                //->groupBy('email')
                ->count();
            $total_clicked = DB::table('autoresponder_new_stats')
                ->where(['stat_type'=> 'click', 'country' => $value->country])
                ->whereIn( 'email_campaign_title_id', $id)
                ->count();
            array_push( $wMap,  [ $country_stat->country => [ 'open' => $total_opened, 'click' => $total_clicked, 'lati' => $country_stat->lati, 'longi' => $country_stat->longi ] ]  );
        }
        $world_json = '[';
        foreach( $wMap as $key => $value )
        {
            foreach( $value as $index  => $item )
            {

                $world_json .= "{
                latLng : [".$item['lati'].", ".$item['longi']."],
                name : '".$item['open']." opens & ".$item['click']." clicks occurred in ".$index." '
                },";
            }
        }
        $world_json = rtrim($world_json, ',');
        $world_json = $world_json.']';

        return view('report_by_id', compact('unOpenEmails',
            'title_id',
            'data',
            'open',
            'click',
            'chart',
            'countries',
            'world_json',
            'user_agent_open',
            'user_agent_click',
            'agent_label',
            'autoresponder_type',
            'fav_open_time',
            'fav_click_time',
            'openClickByList'
        ));
    }

}
