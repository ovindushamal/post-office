<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use WordsApi;
use App\Synonym;

class WordApiController extends Controller
{


    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $appIs = '';
        if (!isset($_REQUEST['word'])){
            $wordIs = $request->word;
        }else{
            $wordIs = $_REQUEST['word'];
        }


        $findWord = Synonym::where('word', '=', $wordIs)->first();

        if ($findWord){
            $synonyms = json_decode($findWord->details);

            $findWord->exists = 1;
            $findWord->hits   = $findWord->hits+1;

            if (Auth::User()){
                $checkUserId = strpos($findWord->user_id, (string)Auth::User()->id);
                if ($checkUserId===false){
                    $findWord->user_id = $findWord->user_id.",".Auth::User()->id;
                }
            }
            $findWord->save();
            //saving stats against searching
        }else{

            $word       = WordsApi::word($wordIs);
            $synonyms   = $word->synonyms();


            if (count($synonyms)>0 && $synonyms!=false){
                $obj            = new Synonym();
                $obj->word      = $wordIs;
                $obj->details   = json_encode($synonyms);
                $obj->hits      = 1;
                if (Auth::User()){ $obj->user_id = Auth::User()->id; }
                $obj->save();
            }

        }

        $span = '';
        if (count($synonyms)>0 && $synonyms!=false){
            $counter = 0;
            $span =  "<p> Synonym(s)  for: <strong>$wordIs</strong></p><p>";
            foreach($synonyms as $k=>$v){
                $span .= "<label style='min-width: 25%;' '><input type='radio' value='$v' name='replace_synonym' id='replace_synonym' > $v</label>";
            }
            $span .=  "<p>";
        }else{
            $span = 0;
        }
        echo $span;


        return '';

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function Curl($url,$body,$method)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 100);

        if ($method == "get")
            $test_key = 'u2Ih34oZOXmshlBtCkUvPlDX8kxsp1VQUbrjsnidF9NVYS9Jrl';
            $production_key = 'i6tgo0mNO5mshARXNYY3X1iih7sZp1D1w0njsnLTvGJJX5EVhr';
            //there are two types of keys test-key and production-key here is production key in use 2500/day requests are allowed for free
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Mashape-Key: $production_key"));

        if ($method == "post")
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Auth-Token: api-key 71978568577ffb1b4a4ed4dbfb5528de'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_PUT, true );


        if ($body!="")
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $filecontents=curl_exec($ch);

        curl_close($ch);

        return $filecontents;
    }
}
