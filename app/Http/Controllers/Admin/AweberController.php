<?php

namespace App\Http\Controllers\Admin;

use App\EmailBroadcast;
use App\Settings;
use App\UsedAutoResponder;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;


class AweberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    public function getAccessToken(Request $request)
    {

        $userSettings = Settings::where('user_id', '=', Auth::User()->id)->first();

        if ($userSettings) {

            //require_once(asset('/aweber_api/aweber_api.php'));

            require_once(getcwd().'/aweber_api/aweber_api.php');

            // Put the consumer key and secret from your App on labs.aweber.com below.
            $consumerKey    = $userSettings->aweber_key;
            $consumerSecret = $userSettings->aweber_secret;


            $aweber = new \AWeberAPI($consumerKey, $consumerSecret);

            // Put the callback URL of your app below or set to 'oob' if your app isnt
            // a web based application.
            // AweberCallback(Request $request) is callBack function
            $callbackURL    = route('aweber-callback');

            // get a request token
            list($key, $secret) = $aweber->getRequestToken($callbackURL);
            //list($accessTokenKey, $accessTokenSecret) = $aweber->getAccessToken();
            if ($secret) {
                $secretSaved = Settings::where('user_id', '=', Auth::User()->id)->update(['aweber_token'=>$secret]);
                $authorizationURL = $aweber->getAuthorizeUrl();
                if ($secretSaved) {
                    echo json_encode(array('success'=>1, 'redirect'=>$authorizationURL));
                }else{
                    echo 0;
                }
            }else{
                echo 2;
            }
        }
    }



    //this is callBack function of request Token to get accessKey and accessTokenSecret
    public function aweberCallback(Request $request)
    {

        $userSettings = Settings::where('user_id', '=', Auth::User()->id)->first();

        if ($userSettings) {
            require_once(getcwd().'/aweber_api/aweber_api.php');

            // Put the consumer key and secret from your App on labs.aweber.com below.
            $consumerKey    = $userSettings->aweber_key;
            $consumerSecret = $userSettings->aweber_secret;

            $aweber = new \AWeberAPI($consumerKey, $consumerSecret);
            $aweber->adapter->debug = false;

            $oauth_token = $_GET['oauth_token'];
            $oauth_verifier = $_GET['oauth_verifier'];
            $aweber->user->requestToken = $oauth_token;
            $aweber->user->verifier = $oauth_verifier;
            $aweber->user->tokenSecret = $userSettings->aweber_token;

            // get a access tokens
            list($accessTokenKey, $accessTokenSecret) = $aweber->getAccessToken();
            if ($accessTokenKey && $accessTokenSecret) {
                Settings::where('user_id', '=', Auth::User()->id)->update(['accessTokenKey'=>$accessTokenKey, 'accessTokenSecret'=>$accessTokenSecret]);
            }
            //after saving redirecting to save settings
            return redirect('settings');
        }
    }

    public function updateStats()
    {

        $aweber_brodcasts = EmailBroadcast::where('type', '=', 'aweber')->where('boradcast_id', '!=', '')->get();
        require_once(getcwd().'/aweber_api/aweber_api.php');

        $old_userId = '';
        if (count($aweber_brodcasts)>0) {
            foreach($aweber_brodcasts as $k=>$v)
            {
                $settings = Settings::where('user_id', '=', $v->user_id)->first();
                $aweber = new \AWeberAPI($settings->aweber_key, $settings->aweber_secret);
                $aweber->adapter->debug = false;

                $account = $aweber->getAccount($settings->accessTokenKey, $settings->accessTokenSecret);
                $account_id = $account->url;

                $listId = UsedAutoResponder::where('newsletterId', '=', $v->boradcast_id)->value('selectedCampaigns');

                $url2 = "$account_id/lists/$listId/broadcasts/$v->boradcast_id";
                $parameters = array();
                $res = $aweber->adapter->request('post', $url2, $parameters, array());
            }
        }

    }
}
