<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Categories;
use App\SubjectTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.category.list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.category.add");
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Categories::where(['category' =>  $request->category, 'user_id' => Auth::user()->id])->value('id');

        if ($category)
        {
            echo 'al';
        }else {


            $obj = new Categories();
            if ($request->id != '') {
                $obj = $obj->findOrFail($request->id);
            }
            $obj->fill($request->all());
            $obj->category_id = 3;
            $obj->type = 'manual';
            $obj->user_id = Auth::User()->id;
            if ($obj->save()) {

                echo  $obj->id;
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function loadEdit(Request $request)
    {
        $data = Categories::findOrFail($request->id);
        if ($data)
        {
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
