<?php

namespace App\Http\Controllers\Admin;

use App\Aweber_Setting;
use App\GetResponse_Setting;
use App\MailChimp_Setting;
use App\Activecampaign_Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Aweber;
use Auth;
use App\Settings;
use DB;

use Mailchimp\Mailchimp;
use Illuminate\Support\Facades\Config;
use App\Exceptions;

class SettingNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aweber_setting             =  Aweber_Setting::where('user_id', Auth::user()->id)->get();
        $getresponse_setting        =  GetResponse_Setting::where('user_id', Auth::user()->id)->get();
        $mailchimp_setting          =  MailChimp_Setting::where('user_id', Auth::user()->id)->get();
        $activecampaign_setting     =  Activecampaign_Setting::where('user_id', Auth::user()->id)->get();

        // mailchimp

        $mailchimp_data = array();
        foreach( $mailchimp_setting as $value ){

            try {

                $mc = new Mailchimp($value->mailchimp_key);
                $result = $mc->request('lists', []);
                $data = $result['lists'];
                $data = $data[0];
                if (isset($data->id)) {
                    $status = 'fa fa-check text-success';
                } else {
                    $status = 'fa fa-times text-danger';
                }
            } catch (\Exception $e)
            {
                $status = 'fa fa-times text-danger';
            }
            array_push($mailchimp_data, ['id' => $value->id, 'account_title' => $value->account_title , 'status' => $status, 'mailchimp_key' => $value->mailchimp_key ]);
        }

        // get response
        $get_response_data = array();
        foreach($getresponse_setting as $value){
            try {
                $url_newsletter_post = 'https://api.getresponse.com/v3/accounts?fields=accountId';
                $response = $this->curl($url_newsletter_post, $json = '', 'get', $value->get_response_key);
                $res_decoded = json_decode($response);

                if (isset($res_decoded->accountId)) {
                    $status = 'fa fa-check text-success';
                } else {
                    $status = 'fa fa-times text-danger';
                }
            }catch (\Exception $e)
            {
                $status = 'fa fa-times text-danger';
            }

            array_push( $get_response_data, ['id' => $value->id, 'account_title' => $value->account_title, 'get_response_key' => $value->get_response_key, 'status' => $status ]);
        }

        // aweber
        require_once(getcwd().'/aweber_api/aweber_api.php');
        $aweber_data = array();
        foreach( $aweber_setting as $v)
        {

            try {
                $aweber = new \AWeberAPI($v->aweber_key, $v->aweber_secret);
                $aweber->adapter->debug = false;
                $account = $aweber->getAccount($v->accessTokenKey, $v->accessTokenSecret);
                $account = $account->data;

                if (isset($account['id'])){
                    $status = 'fa fa-check text-success';
                }else{
                    $status = 'fa fa-times text-danger';
                }
                //throw new \Exception('Something bad');
            } catch (\Exception $e)
            {
                $status = 'fa fa-times text-danger';
            }

            array_push($aweber_data, ['accessTokenKey' => $v->accessTokenKey, 'aweber_token' => $v->aweber_token, 'id' => $v->id,'account_title' => $v->account_title, 'aweber_key' => $v->aweber_key, 'aweber_secret' => $v->aweber_secret, 'status' => $status ]);
        }
        // active campaign
        $active_campaign = array();
        foreach($activecampaign_setting as $active){
            try {
                $key = $active->activecampaign_key;
                $url = $active->activecampaign_domain . '/admin/api.php?api_action=account_view&api_output=json&api_key=' . $key;
                $method = 'get';
                $response = $this->curl($url, $body = '', $method, $get_response_key = '');
                $response = json_decode($response);
                if ($response->result_code == 1) {
                    $status = 'fa fa-check text-success';
                } else {
                    $status = 'fa fa-times text-danger';
                }
            }catch (\Exception $e)
            {
                $status = 'fa fa-times text-danger';
            }
            array_push($active_campaign, ['account_title' => $active->account_title, 'id' => $active->id ,'status' => $status, 'domain' => $active->activecampaign_domain, 'key' => $key ]);
        }

        return view("admin.settings.setting_new", [
            'aweber_data'        => $aweber_data,
            'get_response_data'   => $get_response_data,
            'mailchimp_data'     => $mailchimp_data,
            'active_campaign'      => $active_campaign,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // profile
    public function create()
    {
        //

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $is_category = false;
        $type = '';
        if ($request->has('import_category')) {
            $is_category = true;
        }

        if ($request->object == 'aweber_settings'){
            $obj = new Aweber_Setting();
            $type = 'aweber';
        }

        if ($request->object == 'getresponse_settings'){
            $obj = new GetResponse_Setting();

            $url_newsletter_post = 'https://api.getresponse.com/v3/accounts?fields=accountId';
            $response = $this->curl($url_newsletter_post, $json='', 'get', $request->get_response_key );
            $res_decoded = json_decode($response);

            //print_r($res_decoded);

            if (!isset($res_decoded->accountId)){

                echo json_encode(array('msg' => $res_decoded->message, 'res' => 3));
                exit;
            }else{
                $type = 'getresponse';
            }
        }

        if ($request->object == 'mailchimp_settings'){
            $obj = new MailChimp_Setting();

            try {
                $mc = new Mailchimp($request->mailchimp_key);
                $result = $mc->request('lists', []);
                $data = $result['lists'];
                $data = $data[0];
                if (isset($data->id)) {
                    $status = 1;
                } else {
                    $status = 2;
                }
            } catch (\Exception $e)
            {

                $msg =  $e->getMessage();
                $status = 2;
            }
            if ($status == 2){

                echo json_encode(array('msg' => $msg, 'res' => 3));
                exit;
            }else{
                $type = 'mailchimp';
            }
        }


        if ($request->object == 'activecampaign_settings'){
            $obj = new Activecampaign_Setting();
            $obj->activecampaign_domain = $request->activecampaign_domain;
            $key = $request->activecampaign_key;
            $url = $request->activecampaign_domain .'/admin/api.php?api_action=account_view&api_output=json&api_key='.$key;
            $method = 'get';
            $response = $this->curl($url, $body='', $method, $get_response_key ='');
            $response = json_decode($response);

            if (!$response->result_code == 1 && isset($response)){
                echo json_encode(array('msg' => $response->result_message, 'res' => 3));
                exit;
            }else{
                $type = 'activecampaign';
            }

        }
        // check is account is valid through api call \




        if ($request->id!=''){
            $obj = $obj->find($request->id);
        }else{
            $obj->user_id = Auth::User()->id;
        }

        $obj->fill($request->all());

        if ($is_category) {

            $category = DB::table('email_sub_category')->where(['user_id' => Auth::user()->id, 'category' => $request->import_category, 'type' => $type])->value('id');
            // check subcategory already exist show alert
            if (!$category) {

                $obj->save();
                $account_id = $obj->id;

                $subcategory_id = DB::table('email_sub_category')->insertGetId(
                    ['user_id' => Auth::User()->id, 'category' => $request->import_category, 'type' => $type, 'account_id' => $account_id, 'category_id' => 6]
                );

                echo json_encode(array('res' => 1));
                exit;
            }else{

                echo json_encode(array('msg' => 'Category already exist!', 'res' => 3));
                exit;
            }
        }


        if ($obj->save()){

            echo json_encode(array('res' => 1));
        }else{
            echo 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function sharingCodeGenerator(Request $request){

        $raw = $request->type . '-' . $request->account_id. '-'. Auth::user()->id;
        $encoded =  str_replace('==', '', base64_encode($raw) );

        $db_code = DB::table('sharing_code')->where(['code' => $encoded, 'user_id' => Auth::user()->id])->value('code');

        if (!$db_code) {
            $submit = DB::table('sharing_code')->insertGetId(['code' => $encoded, 'user_id' => Auth::user()->id]);
            echo  str_replace('==', '', $encoded) ;
        }else{
            echo $db_code;
        }
    }

    function fetchEmails(Request $request){

        $responder = $request->target;
        $setting_id = $request->account;
        $subcategory = $request->category;

        $type = str_replace('_settings','', $responder );

        $category = DB::table('email_sub_category')->where( ['user_id' => Auth::user()->id, 'category' => $subcategory, 'type' => $type] )->value('id');
        // check subcategory already exist show alert
        if (!$category) {
            $subcategory_id = DB::table('email_sub_category')->insertGetId(
                [ 'user_id' => Auth::User()->id, 'category' => $subcategory, 'type' => $type, 'account_id' => $setting_id, 'category_id' => 6 ]
            );
            echo 1;
        }else{
            echo 3;
            exit;
        }
    }

    private function curl($url, $body, $method, $get_response_key)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );

        //Get-Response API Request
        if ($get_response_key!='') {

            if ($method == "get")
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Auth-Token: api-key $get_response_key"));

            if ($method == "post")
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","X-Auth-Token: api-key $get_response_key"));

            //Aweber API Request
        }else{

            if ($method == "post")
                curl_setopt($ch, CURLOPT_POST, true );

            if ($method == "get")
                curl_setopt($ch, CURLOPT_HTTPGET, true );
        }


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_PUT, true );

        if ($method == "post")
            curl_setopt($ch, CURLOPT_POST, true );

        if ($method == "get")
            curl_setopt($ch, CURLOPT_HTTPGET, true );


        if ($body!="")
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $filecontents=curl_exec($ch);
        curl_close($ch);
        return $filecontents;
    }

    function cpanelAccount(){
        return view('admin.server_creation.create');
    }


}
