<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;

class ContactListController extends Controller
{
    function index(){
        $list = DB::table('autoresponder_lists')
            ->where(['user_id' => auth::user()->id ] )
            ->get();

    return view('admin.contacts.csv_contacts', compact('list'));

    }

    function store(Request $request){
        $data = 2;

        if($request->type == 'update_list'){
            $list_UP = DB::table('autoresponder_lists')
                ->where('id', $request->id)
                ->update(['list_name' => $request->list_name]);
            if($list_UP){
                $data = 1;
            }

        }else if($request->type == 'add_list'){

            if($request->list_name!=''){
                // insert data to list
                $unique = uniqid();
                $list_id = DB::table('autoresponder_lists')
                    ->insertGetId(
                        [
                            'list_name' => $request->list_name,
                            'user_id' => auth::user()->id,
                            'list_id' => $unique
                        ]
                    );
                if($list_id){
                    return response()->json(['msg' => 1, 'list_id' => $list_id]);
                }
            }

        }else if($request->type == 'add'){
            if($request->email!='' && $request->name!=''){

                $raw = explode('%', $request->list);

                $list_id = $raw[0];
                $list_name = $raw[1];

                /* add to contact */
                $addList = DB::table('autoresponder_contact')
                    ->insertGetId(
                        [
                            'list_name' => $list_name,
                            'user_id' => auth::user()->id,
                            'name' => $request->name,
                            'email' => $request->email,
                            'list_id' => $list_id,
                            'type' => 'Manual',
                            'status' => 1
                        ]
                    );
                if($addList){
                    $data=1;
                }
            }


        }else if($request->type == 'import') {



            $validator = Validator::make($request->all(), [
                'file' => 'required|mimes:csv,txt',
                'list' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['msg' => 2, 'error' => $validator->errors()->all()]);
            }

            $file = time() . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(base_path() . '/assets/csv/', $file);

            $file_path = base_path() . '/assets/csv/' . $file;

            $data = $this->csvToArray($file_path, $request->list);
        }

        if($data == 2){
            return response()->json(['msg' => 2]);
        }else{
            return response()->json(['msg' => 1]);
        }
    }


    private function csvToArray($filename, $list_id, $delimiter = ',')
    {
        try {
            if (!file_exists($filename) || !is_readable($filename))
                return false;

            // insert data to list

            $unique = $list_id = DB::table('autoresponder_lists')->where('id', $list_id)->value('list_id');




            if ($list_id) {

                $header = null;
                if (($handle = fopen($filename, 'r')) !== false) {
                    while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            if ($row[1] != '') {
                                $name = $row[0];
                                $email = $row[1];
                                /* populate contacts */

                                $list_name = DB::table('autoresponder_lists')->where('list_id', $list_id)->value('list_name');


                                $list_id = DB::table('autoresponder_contact')
                                    ->insertGetId(
                                        [
                                            'list_name' => $list_name,
                                            'user_id' => auth::user()->id,
                                            'name' => $name,
                                            'email' => $email,
                                            'list_id' => $unique,
                                            'type' => 'Manual',
                                            'status' => 1
                                        ]
                                    );
                            }
                        }
                    }
                    fclose($handle);
                    unlink($filename);
                }
            }

        }catch(\Exception $e){
            return 2;
        }


    }

    function loadList(Request $request){
        if($request->id!=''){
            $list_name = DB::table('autoresponder_lists')->where('id', $request->id)->value('list_name');
            return response()->json(['msg'=> 1, 'rec' => $list_name, 'id' => $request->id]);
        }
    }



}