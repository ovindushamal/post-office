<?php

namespace App\Http\Controllers\Admin;
use App\appSettings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use ZipArchive;

class AppSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {
        $data = DB::table('app_settings')->first();
        return view('admin.settings.web_settings', compact('data') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //print_r($request->all());
        $obj = new appSettings();

        if($request->id!=''){
            $obj = $obj->findOrFail($request->id);
        }

        $obj->fill($request->all());

        /* logo background */
        if ($request->hasFile('logo')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->logo )){
                unlink(base_path() . '/assets/img/app/'.$obj->logo);
            }
            $logo = $input['logo'] = rand(1,10).time() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(base_path() . '/assets/img/app/',$input['logo']);
            $obj->logo = $logo;
        }
        /* logo sm image */
        if ($request->hasFile('logo_sm')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->logo_sm )){
                unlink(base_path() . '/assets/img/app/'.$obj->logo_sm);
            }
            $logo_sm = $input['logo_sm'] = rand(1,10).time() . '.' . $request->logo_sm->getClientOriginalExtension();
            $request->logo_sm->move(base_path() . '/assets/img/app/',$input['logo_sm']);
            $obj->logo_sm = $logo_sm;
        }
        /* favicon image */
        if ($request->hasFile('favicon')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->favicon )){
                unlink(base_path() . '/assets/img/app/'.$obj->favicon);
            }
            $favicon = $input['favicon'] = rand(1,10).time() . '.' . $request->favicon->getClientOriginalExtension();
            $request->favicon->move(base_path() . '/assets/img/app/',$input['favicon']);
            $obj->favicon = $favicon;
        }


        /* login background */
        if ($request->hasFile('login_bg')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->login_bg )){
                unlink(base_path() . '/assets/img/app/'.$obj->login_bg);
            }
            $login_bg = $input['login_bg'] = rand(1,10).time() . '.' . $request->login_bg->getClientOriginalExtension();
            $request->login_bg->move(base_path() . '/assets/img/app/',$input['login_bg']);
            $obj->login_bg = $login_bg;
        }

        /* register background */
        if ($request->hasFile('register_bg')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->register_bg )){
                unlink(base_path() . '/assets/img/app/'.$obj->register_bg);
            }
            $register_bg = $input['register_bg'] = rand(1,10).time() . '.' . $request->register_bg->getClientOriginalExtension();
            $request->register_bg->move(base_path() . '/assets/img/app/',$input['register_bg']);
            $obj->register_bg = $register_bg;
        }
        /* forgot password background */
        if ($request->hasFile('forgot_password_bg')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->forgot_password_bg )){
                unlink(base_path() . '/assets/img/app/'.$obj->forgot_password_bg);
            }
            $forgot_password_bg = $input['forgot_password_bg'] = rand(1,10).time() . '.' . $request->forgot_password_bg->getClientOriginalExtension();
            $request->forgot_password_bg->move(base_path() . '/assets/img/app/',$input['forgot_password_bg']);
            $obj->forgot_password_bg = $forgot_password_bg;
        }

        /* sidebar  background */
        if ($request->hasFile('sidebar_bg')) {
            if(file_exists( base_path() . '/assets/img/app/'.$obj->sidebar_bg )){
                unlink(base_path() . '/assets/img/app/'.$obj->sidebar_bg);
            }
            $sidebar_bg = $input['sidebar_bg'] = rand(1,10).time() . '.' . $request->sidebar_bg->getClientOriginalExtension();
            $request->sidebar_bg->move(base_path() . '/assets/img/app/',$input['sidebar_bg']);
            $obj->sidebar_bg = $sidebar_bg;
        }

        if($obj->save()){
           return back()->with('success', 'App Settings saved successfully!');
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function getServerURL()
    {
        $serverName = $_SERVER['SERVER_NAME'];
        $filePath = $_SERVER['REQUEST_URI'];
        $withInstall = substr($filePath,0,strrpos($filePath,'/')+1);
        $serverPath = $serverName.$withInstall;
        $applicationPath = $serverPath;

        if(strpos($applicationPath,'http://www.')===false)
        {
            if(strpos($applicationPath,'www.')===false)
                $applicationPath = 'www.'.$applicationPath;
            if(strpos($applicationPath,'http://')===false)
                $applicationPath = 'http://'.$applicationPath;
        }

//$url = $applicationPath.'uploads/';

        return $applicationPath;
    }

    function serverUpdate(Request $request){

        ini_set('max_execution_time','9000000');

        $old_ver = DB::table('app_settings')->value('version');

        if(!isset($_GET['ver']))
        {
            $ver=$old_ver;
        }
        else
            $ver=$_GET['ver'];
        $url=$this->getServerURL();
        $time=time();
        /* echo '<body style="border: 1px solid #fff; width: 600px; height: 100px; margin: 100px auto;">
         <img src="'.asset('assets/images/loader1.gif').'" style="float: left; margin-top: 20; margin-right: 20px"><h2>Please wait....</h2><hr></body>';*/
        $json=file_get_contents("http://apps.ranksol.com/app_updates/email_markeeting/update.php?url=$url&ver=$ver&time=$time");

        ///////////////log mysql file
        if(isset($_GET['log']) && $_GET['log'] == "true"){
            echo "<b>Json recieved</b>".$json."<b>Version---$old_ver</b><hr>";
        }
        $arr=json_decode($json,true);
        if($arr['error'] == "no")
        {
            if(is_array($arr['sql']) && count($arr['sql'])>0){
                //  print_r($arr['sql']);
                // die();

                foreach($arr['sql'] as $key => $val){

                    $file= @file_get_contents("http://apps.ranksol.com/app_updates/email_markeeting/sql/$val?time=$time");
                    $queryArray = array();
                    $queryArray = explode(';',$file);
                    for($i=0;$i<count($queryArray);$i++)
                        if(trim($queryArray[$i])!='')
                            //mysql_query($queryArray[$i]);
                            DB::statement($queryArray[$i]);

                    ///////////////log mysql file
                    if(isset($_GET['log']) && $_GET['log'] == "true"){
                        echo "<b>My sql contents------</b>".$file."------<hr>";
                    }
                    ////////////////////end log
                }
            }
///echo "http://woottext.com/wbsms_updates/update/$arr[zip]";
            if(strlen($arr['zip'])> 3)
            {
                file_put_contents($arr['zip'], file_get_contents("http://apps.ranksol.com/app_updates/email_markeeting/update/$arr[zip]?time=$time"));
                if(class_exists('ZipArchive'))
                {
                    //$dir=dirname(__FILE__);
                    $dir=getcwd();
                    $zip = new ZipArchive;
                    $res = $zip->open("$arr[zip]");
                    if ($res === TRUE) {
                        // echo 'ok';
                        $zip->extractTo("$dir/");
                        $zip->close();
                        ///////////////////log zip
                        if(isset($_GET['log']) && $_GET['log'] == "true"){
                            echo "<b>Zip------</b>".$arr['zip']."------<hr>";
                        }
                        ///////////////////end log zip/////////////////
                    } else {
                        echo 'failed, code:' . $res;
                    }
                }
                else{
                    include_once(getcwd().'/pclzip.lib.php');
                    $archive = new PclZip($arr['zip']);
                    $v_list=$archive->extract();

                    if ($v_list == 0) {
                        die("Error : ".$archive->errorInfo(true));
                    }
                    ///////////////////log zip lib
                    if(isset($_GET['log']) && $_GET['log'] == "true"){
                        echo "<b>Zip lib------</b>".$arr['zip']."------<hr>";
                    }
                    ///////////////////end log zip/////////////////
                }
                //@unlink($arr['zip']);
            }
            if(is_array($arr['del']) && count($arr['del'])>0)
            { foreach($arr['del'] as $val_d)
            {@unlink($val_d);
                ///////////////////log unlink
                if(isset($_GET['log']) && $_GET['log'] == "true"){
                    echo "<b>unlink------</b>".$val_d."------<hr>";
                }
                ///////////////////end log unlink/////////////////
            }
            }
            if(isset($arr['version']) && $arr['version'] !="")
            {
                $sql = appSettings::where('id', 1)->update(['version' => $arr['version'] ]);
                //echo "<h2>".$_SESSION['message'] =  "Application has been Updated Successfully";
                @unlink(getcwd().'/'.$arr['zip']);
            }
            if(!isset($_GET['log']))
            {
                return response()->json(['success' => 1]);
            }
        }else{

            return response()->json($arr);
        }
    }
}