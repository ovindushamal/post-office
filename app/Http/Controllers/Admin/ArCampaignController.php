<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use App\Settings;

class ArCampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.ar_campaign.my_emails");
    }

    function sharedEmails( Request $request)
    {
        return view("admin.ar_campaign.shared_emails");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // profile
    public function create()
    {
        return view('admin.ar_campaign.import_emails');
    }

    function fetchSharedEmails(Request $request)
    {

        // stop if already used
        $code = $request->shared_code;
        $explode_code = explode('-', base64_decode( $code ) );

        if (!isset($explode_code[0]) || !isset($explode_code[1]) || !isset($explode_code[2]) )
        {
            echo 4; // wrong code inserted
            exit;
        }

        if (!isset($explode_code[3]))
        {
            $type = $explode_code[0];
            $account_id = $explode_code[1];
            $user_id = $explode_code[2];
            $column = 'account_id';
        }else{
            $type = $explode_code[1];
            $account_id = $explode_code[2];
            $user_id = $explode_code[3];
            $column = 'id';
        }


        $validate = DB::table('sharing_code')
            ->where(['code' => $code, 'user_id' => $user_id ])
            ->where('user_id', '!=', Auth::user()->id )
            ->value('id');

        if ($validate)
        {

            $sub_cat_id = DB::table('email_sub_category')->where(['type' => strtolower( $type), 'user_id' => $user_id, $column => $account_id ])->value('id');
            $users_code = Auth::user()->email_shared_id;

           if ($users_code == "")
           {
                $update = DB::table('users')->where(['id' => Auth::user()->id ])->update(['email_shared_id' => $sub_cat_id]);
                if ($update)
                {
                    echo 1; // success
                }
            }else{

                $user_assigned = explode(',' , $users_code);

                if (in_array($sub_cat_id, $user_assigned))
                {
                    echo 3; // code already in use
                }else{
                    $update = DB::table('users')->where(['id' => Auth::user()->id ])->update(['email_shared_id' => $users_code.','.$sub_cat_id]);
                    if ($update)
                    {
                        echo 1; // success
                    }
                }
            }
        }else{
            echo 4; // invalid code
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sub_id = $id;

        return view("admin.ar_campaign.shared_emails_detail", compact('sub_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
