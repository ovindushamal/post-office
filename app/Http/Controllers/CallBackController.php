<?php

namespace App\Http\Controllers;

use App\EmailBroadcast;
use App\Categories;
use App\EmailTemplate;
use App\SubjectTemplate;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;

class CallBackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function callBack(Request $request){

        $data = $_REQUEST;

        if (isset($data['action'])){

            $action = $data['action'];

            if ($data['MESSAGE_ID']!=''){

                $check = EmailBroadcast::where('boradcast_id', '=', $data['MESSAGE_ID'])->first();

                if ($check){

                    if ($action=='open'){
                        $check->update(['opened'=>$check->opened+1]);
                    }

                    if ($action=='click'){
                        $check->update(['clicked'=>$check->clicked+1]);
                    }

                    if ($action=='unsubscribe'){
                        $check->update(['unsubscribed'=>$check->unsubscribed+1]);
                    }
                }

            }
        }
    }
}
