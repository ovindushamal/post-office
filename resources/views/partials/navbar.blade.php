<?php
$page =  Request::route()->getName();
$setting = DB::table('app_settings')->first();
$version_update = \App\appSettings::updateCurl('http://apps.ranksol.com/app_updates/email_markeeting/check_update.php?ver='.$setting->version, 'post');
if(isset($version_update) && $version_update!=''){
    \Session::put('up_version', json_decode($version_update) );
}

?>
<div class="sidebar" data-active-color="{{ isset($setting->sidebar_active) && $setting->sidebar_active !='' ? $setting->sidebar_active : 'rose' }}" data-background-color="{{ isset($setting->sidebar_bg_color) && $setting->sidebar_bg_color !='' && $setting->sidebar_bg_color !='' ? $setting->sidebar_bg_color : 'black' }}" data-image="{{ isset($setting->sidebar_bg) && $setting->sidebar_bg !='' ? asset('assets/img/app/'.$setting->sidebar_bg) : asset('assets/img/sidebar.jpg') }}">
    <div class="logo">
        <a href="" class="logo-mini">
            <img src="{{ isset($setting->logo_sm) && $setting->logo_sm != ''? asset('assets/img/app/'.$setting->logo_sm) : asset('assets/images/logo/iconwhite-40.png') }}" alt="logo" class="-m-l-10">
        </a>
        <a href="" class="logo-normal">
            <img src="{{ isset($setting->logo) && $setting->logo != ''? asset('assets/img/app/'.$setting->logo) : asset('assets/images/logo/wordmarkwhite-40.png') }}" alt="logo" class="">
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                @if(Auth::user()->image!='')
                <img src="{{ asset('assets/images/users/'.Auth::user()->image) }}" />
                @else
                <img src="{{ asset('assets/images/user_hidden.png')}}" />
                @endif
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <span>
                        <span id="user_name">{{ Auth::user()->name }}</span>
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse {{ ($page == 'profile' )? 'in' : '' }}" id="collapseExample">
                    <ul class="nav">
                        <li class="{{ ($page == 'profile')? 'active' : '' }}">
                            <a href="{{ route('profile') }}">
                                <span class="sidebar-mini"> <i class="material-icons">account_box</i> </span>
                                <span class="sidebar-normal"> My Profile </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ Route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span class="sidebar-mini"> <i class="material-icons">power_settings_new</i> </span>
                                <span class="sidebar-normal"> Logout </span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            @if( Session::has('up_version') && $version_update!='')
                <li class="">
                    <a href="{{ url('update-version') }}" class=" active">
                        <i class="fas fa-sync"></i>

                        <p>Update Version  <?php  $ver = \Session::get('up_version'); echo $ver->version;  ?> <span class="label label-rose">New</span> </p>
                    </a>
                </li>
            @endif
            <li class="{{ ($page == 'home')? 'active' : '' }}">
                <a href="{{ route('home') }}" >
                    <i class="material-icons">dashboard   </i>
                    <p> Dashboard </p> </a>
            </li>

            @if(!Auth::guest() && Auth::User()->type=='a')
                <li class="{{ ($page == 'users.index')? 'active' : '' }}">
                    <a href="{!! Route('users.index') !!}">
                        <i class="fa fa-users"></i>
                        <p> Manage Users </p>
                    </a>
                </li>
                <li class="{{ ($page == 'library.index')? 'active' : '' }}">
                    <a href="{!! Route('library.index') !!}" >
                        <i class="material-icons">image</i>
                        <p>Library </p>
                    </a>
                </li>
            @endif

            <li class="{{ ($page == 'new-template.create')? 'active' : '' }}">
                <a href="{!! Route('new-template.create') !!}" >
                    <i class="fa fa-send"></i>
                    <p> Broadcast</p>
                </a>
            </li>

            <li>
                <a data-toggle="collapse" href="#Emails">
                    <i class="fa fa-envelope"></i>
                    <p> Emails
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse {{ ($page == 'ar-campaigns.index' )? 'in' : '' }}" id="Emails">
                    <ul class="nav ">
                        <li class="{{ ($page == 'ar-campaigns.index')? 'active' : '' }}">
                            <a class="nav-border-b" href="{!! Route('ar-campaigns.index') !!}">
                                <span class="sidebar-mini"> ME </span>
                                <span class="sidebar-normal"> My Emails </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- settings -->
            <li>
                <a data-toggle="collapse" href="#Settings">
                    <i class="fa fa-cog"></i>
                    <p> Settings
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ ( $page == 'app-settings.index' || $page == 'settings.create' || $page == 'email-header-footer-settings.index' || $page == 'gen_settings.index' || $page == 'email-settings.index' )? 'in' : '' }}" id="Settings">
                    <ul class="nav">
                        <li class="{{ ($page == 'gen_settings.index')? 'active' : '' }}">
                            <a href="{!! Route('gen_settings.index') !!}">
                                <span class="sidebar-mini"> AR </span>
                                <span class="sidebar-normal"> Auto Responders </span>
                            </a>
                        </li>
                        @if( Auth::user()->type == 'a' )
                            <li class="{{ ($page == 'email-settings.index')? 'active' : '' }}">
                                <a href="{!! Route('email-settings.index') !!}">
                                    <span class="sidebar-mini"> <i class="fa fa-envelope"></i> </span>
                                    <span class="sidebar-normal"> Emails </span>
                                </a>
                            </li>
                            <li class="{{ ($page == 'app-settings.index')? 'active' : '' }}">
                                <a href="{!! Route('app-settings.index') !!}" >
                                    <i class="fa fa-cogs"></i>
                                    <p>Application Settings </p>
                                </a>
                            </li>
                        @endif
                        <li class="{{ ($page == 'email-header-footer-settings.index')? 'active' : '' }}">
                            <a href="{!! Route('email-header-footer-settings.index') !!}">
                                <span class="sidebar-mini"> <i class="far fa-credit-card"></i></span>
                                <span class="sidebar-normal"> Header Footer </span>
                            </a>
                        </li>
                        <li class="{{ ($page == 'settings.create')? 'active' : '' }}">
                            <a class="nav-border-b" href="{!! Route('settings.create') !!}">
                                <span class="sidebar-mini"> <i class="fa fa-rocket"></i> </span>
                                <span class="sidebar-normal"> Email Sender </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            <li class="{{ ($page == 'email-sub-category.index')? 'active' : '' }}">
                <a href="{{ Route('email-sub-category.index') }}" >
                    <i class="fa fa-file-text"></i>
                    <p> Category </p>
                </a>
            </li>

            <li class="{{ ($page == 'contact-list.index')? 'active' : '' }}">
                <a href="{{ Route('contact-list.index') }}" >
                    <i class="fa fa-address-card"></i>
                    <p> Contacts </p>
                </a>
            </li>


            <li class="{{ ($page == 'help')? 'active' : '' }}">
                <a href="{!! route('help') !!}" >
                    <i class="fa fa-question-circle"></i>
                    <p> Tutorial </p>
                </a>
            </li>

            <li>
                <a href="http://ranksol.com/help/" target="_blank" >
                    <i class="fa fa-file-video-o"></i>
                    <p> Help </p>
                </a>
            </li>

        </ul>
    </div>
</div>
<div class="main-panel">
<!--  Transparent nav  -->
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}"> Dashboard  &nbsp;
            </a>

        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ route('gen_settings.index') }}">
                        <i class="fa fa-cog"></i>
                        <p class="hidden-lg hidden-md">Settings</p>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">person</i>
                        <p class="hidden-lg hidden-md">Profile</p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('profile') }}">
                                <span class="sidebar-mini"> <i class="material-icons">account_box</i> </span>
                                <span class="sidebar-normal"> My Profile </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ Route('gen_settings.index') }}">
                                <span class="sidebar-mini"> <i class="fa fa-cog"></i> </span>
                                <span class="sidebar-normal"> Settings </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ Route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span class="sidebar-mini"> <i class="material-icons">power_settings_new</i> </span>
                                <span class="sidebar-normal"> Logout </span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>

                <li class="separator hidden-lg hidden-md"></li>
            </ul>
        </div>
    </div>
    @if(isset($setting->mail_conf) && $setting->mail_conf ==0)
        <style>
            .main-panel>.content{
                margin-top: 100px!important;
            }
        </style>
        <center><a href="{{ route('email-settings.index') }}"><div class="alert alert-danger font-16 " style="margin-top: -20px"><i class="fa fa-warning text-white"></i>  Note: Please config <a href="{{ route('email-settings.index') }}" class=" btn-link btn-simple">Email Settings </a> </div></a></center>
    @endif
</nav>
