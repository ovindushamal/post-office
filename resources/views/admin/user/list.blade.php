@extends('layouts.app')
@section('title', 'Users')
@section('content')
<style>
    form#add_user .loader {
        float: right;
        margin-left: 5px;
        margin-top: 6px;
    }
    form#add_user .loader img{
        display: none;
    }
    .add_more{
        float: right;
        margin-bottom: 5px;
    }

</style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Users </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header card-header-text" data-background-color="rose">
                    <h3 class="card-title"> Manage Users </h3>
                </div>
                <div class="add_more m-r-10">
                    <button type="button" class="btn btn-success w-md " onclick='addUser()' >Create User</button>
                </div>
                <div class="card-content">
                    <div class="material-datatables">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Dated</th>
                                <th>Status&nbsp;|&nbsp;Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">No Record found yet.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
<!--  add user -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title text-center">Add New User</h4>
            </div>

            <form id="add_user" method="post" role="form" >
                <input type="hidden" name="id" value="" id="id" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Name:</label>
                                <input type="text" name="name" class="form-control" value="" id="name" placeholder="name" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Email:</label>
                                <input type="email" name="email" class="form-control" value="" id="email" placeholder="valid email" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Password:</label>
                                <input type="text" name="password" class="form-control" value="" id="plain_password" placeholder="password" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Email category:</label>
                                <select name="email_category[]" class="form-control" id="email_category" multiple required >
                                    <option value="0">None</option>
                                    @foreach($email_category as $value)
                                    <option value="{{ $value->id }}">{{ $value->category }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="c_info"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-xs" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success btn-xs">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- usre stats modal  -->
<div id="user_stat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> </div>

<!-- /.modal -->
<script>
    $(document).ready(function(){

        //saving new user
        $("#add_user").submit(function(){
            $('#loading').show();
            var data = new FormData(this);
            $.ajax({
                url: "<?php echo route('users.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    $('#loading').hide();

                    var json_obj = JSON.parse(result);

                    if(json_obj.msg!='0'){
                        $('#add_user')[0].reset();
                        $("#id").val("");
                        $("#con-close-modal").modal('hide');
                        //$("#c_info").html(result);
                        //$("#c_info").append(result);
                        refreshTable();
                        if(json_obj.msg=='1'){
                            swal("Success!", "User has been saved successfully", "success");
                        }
                    }else{
                        swal("Error!", "Something went wrong.", "error");
                    }
                }
            });
            return false;
        });

        $("[name='status']").bootstrapSwitch();

        $('#load_datatable').DataTable({
            "pageLength":25,
            "order": [[0, 'desc']],
            processing: true,
            serverSide: true,
            responsive:true,
            "initComplete": function (settings, json) {

            },
            "drawCallback": function () {
                //table_draw();
                $("[name='status']").bootstrapSwitch();
                changeStatus();
            },
            ajax: "{!! Route('load-users') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });

    function addUser(){
        $("#con-close-modal").modal('show');
    }
    function editRow(x){
        $('#loading').show();
        if(x!=''){
            $.post("<?php echo url('user/loadEdit'); ?>", {id: x}, function(result){
                if(result!='0'){
                    var data = JSON.parse(result);
                    var cat_array = data.email_category.split(',');
                    $.each(data, function(k,v){
                        var ref = $("#add_user").find("#"+k);
                        $(ref).val(v);
                        $("#con-close-modal").modal('show');
                    });

                    $('#email_category > option').attr('selected',false);

                    $('#email_category > option').each(function() {

                        $(this).attr('selected', false).removeClass('bg-gray');
                        $(this).prop('selected', false).removeClass('bg-gray');

                        for( var i = 0; i < cat_array.length; i++){
                            if( cat_array[i] == $(this).val() ){
                                $(this).prop('selected', 'selected').addClass('bg-gray');
                            }
                        }
                    });

                    var cat_array = data.email_category.split(',');
                    //console.log(cat_array_length);
                }else{
                    swal("Error!", "Something went wrong.", "error");
                }
                $('#loading').hide();
            }
            );
        }
    }

    function showStats(e){
        $('#loading').show();
         var user_id =   $(e).data('id');
         if( user_id != '' ){
            $.post("<?php echo route('user-stats'); ?>", {user_id: user_id}, function(result){
                if(result!='0'){

                    $('#user_stat').html(result);

                    $('#user_stat').modal('show');
                    $('#loading').hide();
                }else{
                    swal("Error!", "Something went wrong.", "error");
                }
                $('#loading').hide();
            }
            );
        }
    }

</script>
@endsection