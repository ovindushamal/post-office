@extends('layouts.app')
@section('title', 'Email Template')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">

                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="{{ url('/library') }}">Email Template</a></li>
                        <li class="active"> List </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">


                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif


                <div class="card-box">

                    <div class="row">
                        <div class="col-xs-12 bg-white">
                            <table id="load_datatable" class="table table-bordered table-no-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Subject</th>
                                    <!--<th>Category</th>-->
                                    <th>Dated</th>
                                    <th>Status&nbsp;|&nbsp;Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="5">No Record found yet.</td>
                                    &nbsp;
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
            // datatable
                $('#load_datatable').DataTable({
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    responsive:true,
                    "drawCallback": function () {
                        //table_draw();
                        $("[name='status']").bootstrapSwitch();
                        changeStatus();
                    },
                    ajax: "{!! Route('load-email-content') !!}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'subject', name: 'subject'},
                        //{data: 'category', name: 'category'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                        //{ data: 'updated_at', name: 'updated_at' }
                    ]
                });
            });
            function addCategory(){
                $("#con-close-modal").modal('show');
            }
        </script>
    </div>
@endsection