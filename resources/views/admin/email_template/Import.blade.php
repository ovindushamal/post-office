@extends('layouts.app')
@section('title', 'Import Template')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">

                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li> <a href="#">Template</a></li>
                            <li class="active"> Import </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="card-box">

                    <div class="row">
                        <div class="col-xs-12 bg-white">

                            <form class="form-horizontal" role="form" method="POST" action="{{ url('templates/importPost') }}">
                                {{ csrf_field() }}


                                <div class="form-group">
                                    <label class="col-md-2  control-label">Attachment</label>
                                    <div class="col-md-10">
                                        <input type="file" class="form-control" >
                                    </div>
                                </div>
                                

                                <div class="form-group account-btn m-t-10">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-xs-10">
                                        <button class="btn w-md btn-bordered btn-primary  " type="submit">Import</button>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>

                </div>

            </div>

        </div>


    </div>



@endsection
