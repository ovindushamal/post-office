@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Header footer </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">


                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="">
                    <div class="row">
                        <div class="card">
                            <div class="card-header card-header-text" data-background-color="rose">
                                <h3 class="card-title">Email Header Footer Settings</h3>
                            </div>
                            <div class="card-content">
                                <form class="form-horizontal" role="form" id="email_settings" >
                                    {{ csrf_field() }}

                                    <input type="hidden" name="id" value="{{ (isset($data->id) && $data->id !="")? $data->id :''  }}">

                                    <div class="form-group">
                                        <div class="alert alert-info">Email Default Header</div>
                                    </div>

                                        <div class="form-group ">
                                            <label class="control-label">Header Content:</label>
                                            <div class="">
                                                <textarea class="form-control" id="registration_content" rows="5" placeholder="some details" > {!! (isset($data->header)) ? $data->header : '' !!}</textarea>

                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <div class="alert alert-info">Email Default Footer</div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label">Footer Content:</label>
                                        <div class="">

                                            <textarea class="form-control" id="status_content" rows="5" placeholder="some details" > {!! (isset($data->footer)) ? $data->footer : '' !!}</textarea>

                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="pull-right">
                                            <button class="btn w-md btn-bordered btn-success  " type="submit">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{!! asset('assets/plugins/ckeditor/ckeditor.js') !!}"></script>
    <script>

        $(document).ready(function() {
            // ck editor1
            CKEDITOR.replace('status_content',{
                allowedContent: true
            });
            // ck editor2
            CKEDITOR.replace('registration_content',{
                allowedContent: true
            });

            // ajax submit form
            $("#email_settings").submit(function(){
                $('#loading').show();
                var data = new FormData(this);

                data.append('header', CKEDITOR.instances.registration_content.getData());
                data.append('footer', CKEDITOR.instances.status_content.getData());

                $.ajax({
                    url: "<?php  echo route('email-header-footer-settings.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        $('#loading').hide();
                        if(result == '1'){
                            swal({
                                title: "Good job!",
                                text: "Email header footer have been saved successfully!",
                                type: "success",
                                confirmButtonText: "OK"
                            });

                        }else{
                            swal("Error!", "Something went wrong.", "error");
                            $('#loading').hide();
                        }
                    }
                });

                return false;
            });

        });

    </script>

@endsection
