@extends('layouts.app')
@section('content')
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box pull-right">
                        <ol class="breadcrumb p-0 m-0">
                            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a> </li>
                            <li class="active">Contacts</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-md-12 col-xs-12 col-xxs-12">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title"> <i class="fa fa-download"></i> Contacts</h3>
                        </div>
                        <div class="card-content">
                            <a download href="{{asset('assets/example.csv')}}" class="pull-right btn btn-sm btn-rose"><i class="fa fa-download"></i> Download sample file</a>
                            <div class="card-content p-t-0">
                                <ul class="nav nav-pills nav-pills-rose">
                                    <li class="active">
                                        <a href="#importTab" data-toggle="tab"> Contacts</a>
                                    </li>
                                    <li>
                                        <a href="#addTab" data-toggle="tab">Add Contacts</a>
                                    </li>
                                    <li>
                                        <a href="#manageListTab" data-toggle="tab">Manage list</a>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-rose btn-xs pull-right" title="Add new List" data-toggle="tooltip" onclick="addCategory()" ><i class="fa fa-plus"></i> List<div class="ripple-container"></div></button>
                                <div class="clearfix"></div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="importTab">
                                        <div class="file_error"></div>
                                        <form action="" id="importCsvForm">
                                            <input type="hidden" name="type" value="import">
                                            <div class="form-group">
                                                <select name="list" id="listSelect" data-title="select list" required class="selectpicker" data-live-search="true" data-style="select-with-transition">
                                                    @foreach($list as $value)
                                                        <option value="{{$value->list_id}}%{{$value->list_name}}">{{$value->list_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <label for="">Select CSV file </label>
                                                <input type="file" name="file" class="btn btn-rose" accept=".csv">
                                                <small><i class="fa fa-warning text-danger"></i> CSV file type is acceptable only. </small>
                                            </div>

                                            <div class=" m-t-30">
                                                <button type="submit"  class="btn btn-success btn-update"> <i class="fas fa-download"></i> Import </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="addTab">
                                        <form action="" id="addContactForm">
                                            <input type="hidden" name="type" value="add">
                                            <div class="form-group">
                                                <label for="">Name:</label>
                                                <input type="text" name="name" class="form-control" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Email:</label>
                                                <input type="email" name="email" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <select name="list" id="listSelect" data-title="select list" required class="selectpicker" data-live-search="true" data-style="select-with-transition">
                                                    @foreach($list as $value)
                                                        <option value="{{$value->list_id}}%{{$value->list_name}}">{{$value->list_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class=" m-t-30">
                                                <button type="submit"  class="btn btn-success btn-update"> <i class="fas fa-download"></i> Save </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="manageListTab">

                                        <div class="material-datatables">
                                            <table id="load_datatable_list" class="table table-colored table-inverse table-hover table-striped" style="width: 100%">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>List name</th>
                                                    <th>Created At</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td colspan="4">No Record found yet.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> <i class="fa fa-address-card"></i> Contact List</h3>
                    </div>
                    <button class="btn btn-xs btn-rose pull-right" data-toggle="tooltip" title="Refresh Record" onclick="refreshTable()"><i class="fa fa-refresh"></i> </button>
                </div>

            <div class="card-content">
                <div class="material-datatables">
                    <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped" style="width: 100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>List name</th>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Area Code</th>
                            <th>Country</th>
                            <th>Region</th>
                            <th>Longitude</th>
                            <th>Latitude</th>
                            <th>Subscribed at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4">No Record found yet.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
                    </div>
                </div>
            </div>


    <!-- Add new category modal -->
    <div id="add_listModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">Add New List</h4>
                </div>
                <form method="post" id="ListForms" action="#">
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="add_list">
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="col-md-2 control-label">List Name:</label>
                            <div class="col-md-8">
                                <input type="text" name="list_name" id="listName" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-success replace_now">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- edit list -->

    <div id="edit_listModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">Edit List</h4>
                </div>
                <form method="post" id="editListForms" action="#">
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="update_list">
                    <input type="hidden" id="editId" name="id" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-2 control-label">List Name:</label>
                            <div class="col-md-8">
                                <input type="text" name="list_name" id="editlistName" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-success replace_now">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        
        function editList(e) {
            var id = $(e).attr('data-id');
            if(id!=''){
                $.get('load_list',
                    {id:id},
                    function (data) {
                    if(data.msg == 1){
                        $('#editlistName').val(data.rec);
                        $('#editId').val(data.id);
                        $('#edit_listModal').modal('show');
                    }

                });
            }
        }
        
        
        // load add category modal
        function addCategory(){
            $("#add_listModal").modal('show');
        }
        $(document).ready(function () {

            // edit list
            $('#editListForms').submit(function () {
                var data = new FormData(this);

                $('#loading').show();

                $.ajax({
                    url: "<?php  echo route('contact-list.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        if(result.msg==1){
                            $('#load_datatable_list').DataTable().ajax.reload();
                            $('#edit_listModal').modal('hide');
                            notify('top', 'right', 'success', 'List Edited successfully!');
                        }else{
                            notify('top', 'right', 'rose', "To update make change!");
                        }

                        $('#loading').hide();

                    }
                });
                return false;
            });


            $('#ListForms').submit(function () {
                var data = new FormData(this);

                $('#loading').show();

                $.ajax({
                    url: "<?php  echo route('contact-list.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){

                        var category = $('#listName').val();

                        $('.selectpicker').append('<option selected value="'+result.list_id+'">'+category+'</option>').selectpicker('refresh');
                        $("#add_listModal").modal('hide');
                        $('#loading').hide();
                    }
                });
                return false;
            });

           $('#importCsvForm').submit(function () {
               $('.file_error').html('');
               $('#loading').show();
               var data = new FormData(this);
               $.ajax({
                   url: "<?php  echo route('contact-list.store'); ?>",
                   data: data,
                   contentType: false,
                   processData: false,
                   type: 'POST',
                   success: function(result){
                    if(result.msg == 1){
                        notify('top', 'right', 'success', 'Contacts imported successfully.');
                        refreshTable();
                        $('#importCsvForm')[0].reset();
                    }else{
                        $.each(result.error, function( index, element ) {
                            $('.file_error').append('<li class="text-danger">'+element+'</li>');
                        });
                    }
                    $('#loading').hide();
                   }
               });
               return false;
           });

           /*  add contact */
            $('#addContactForm').submit(function () {
                $('.file_error').html('');
                $('#loading').show();
                var data = new FormData(this);
                $.ajax({
                    url: "<?php  echo route('contact-list.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        if(result.msg == 1){
                            notify('top', 'right', 'success', 'Contacts imported successfully.');
                            refreshTable();
                            $('#addContactForm')[0].reset();
                        }else{
                            $.each(result.error, function( index, element ) {
                                $('.file_error').append('<li class="text-danger">'+element+'</li>');
                            });
                        }
                        $('#loading').hide();
                    }
                });
                return false;
            });


                $('#load_datatable').DataTable({
                    "pageLength":25,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    "initComplete": function (settings, json) {
                    },

                    ajax: "{!! Route('load-contacts') !!}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'list_name', name : 'list_name'},
                        {data: 'type', name : 'type'},
                        {data: 'name', name : 'name'},
                        {data: 'email', name : 'email'},
                        {data: 'phone', name : 'phone'},
                        {data: 'area_code', name : 'area_code'},
                        {data: 'country', name : 'country'},
                        {data: 'region', name : 'region'},
                        {data: 'longitude', name : 'longitude'},
                        {data: 'latitude', name : 'latitude'},
                        {data: 'subscribed_at', name : 'subscribed_at'},
                        {data: 'action', name : 'action'},
                        //{ data: 'updated_at', name: 'updated_at' }
                    ]
                });



                $('#load_datatable_list').DataTable({
                    "pageLength":25,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    "initComplete": function (settings, json) {
                    },

                    ajax: "{!! Route('load-lists') !!}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'list_name', name : 'list_name'},
                        {data: 'created_at', name : 'created_at'},
                        {data: 'action', name : 'action'},
                        //{ data: 'updated_at', name: 'updated_at' }
                    ]
                });
        });

    </script>
@endsection