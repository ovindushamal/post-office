@extends('layouts.app')
@section('title', 'Email Template')
@section('content')
<?php header('Content-Type: text/html; charset=utf-8'); ?>
        <!--Form Wizard-->
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.steps.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/components.css') !!}" />

<style>
    .load-subject{
        margin-top: 20px;
    }
    .load-subject li{
        list-style: none;
        padding: 1px 3px 0px 3px;
        border-bottom: 1px solid #e2e2e2;
        background-color: #f3f3f3;
    }
    .load-subject li:hover{
        background-color: #00acc1;
        color: white;
        cursor: pointer;
    }

    .mobile-body{
        position: absolute;
        top: 12.5%;
        left: 8%;
        width: 85%;
        text-align: justify;
        height: 75%;
        overflow-x: hidden;
        padding-right: 8px;
    }
    .mobile-view, #desktop, #mobile, #landscape{
        position: relative;
    }

    #desktop img, .landscape-view img, .mobile-view img{
        width: 100%;
    }
    .desktop-body{
        position: absolute;
        top: 3.7vw;
        width: 89.3%;
        left: 3.5vw;
        text-align: justify;
        height: 60%;
        overflow-x: hidden;
        padding: 11px;
    }
    .landscape-body{
        position: absolute;
        top: 7%;
        width: 73%;
        text-align: justify;
        height: 86%;
        overflow-y: auto;
        left: 15%;
        padding: 8px;
    }
    #search_subject, .input-group, #search_subject input, #search_subject label {
        width: 100%;
        text-align: left;
    }
    div.dataTables_wrapper div.dataTables_processing{
        top:5% !important;
    }
    .pagination{
        width: 100%;
    }
    /* */
    @media (max-width:1266px){
        .desktop-body{
            width: 89%;
        }
    }

    @media (max-width:1222px){
        .desktop-body{
            width: 89%;
        }
    }

    @media (max-width:1116px){
        .desktop-body{
            top: 3.45vw;
        }
    }

    @media (max-width:1040px){
        .desktop-body{
            top: 4.3vw;
            width: 90%;
        }
        .mobile-body{
            /*left: 1.7vw;*/
            /*width: 87%;*/
        }
    }
    /* mobile */
    @media (max-width:991px){
        .mobile-body{
            top: 12.3%;
            left: 7%;
            width: 86.4%;
            height: 75.5%;
        }
        .landscape-body{
            top: 7%;
            width: 73.8%;
            left: 13%;
        }
    }

    @media (max-width:600px){
        .desktop-body{
            top: 4.1vw;
        }
    }
    @media (max-width:500px){
        .desktop-body{
            top: 3.7vw;
            left: 3vw;
        }
        .landscape-body{
            width: 70%;
            left:15%;
        }
    }


</style>

<div class="content-page">
    <!-- Start content -->
    <div class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">

                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="#">Email Template</a></li>
                        <li class="active"> Add </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-xs-12">

            <div class="panel panel-color panel-inverse">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Email Template</h3>
                </div>
                <div class="panel-body">


                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    <form id="subject-template" action="#" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" name="id" value="{!! ( isset( $content->id) )?$content->id:'' !!}">
                        <input type="hidden" name="subject_id"  value="{!! (isset($subject->id))?$subject->id:'' !!}">

                        <input type="hidden" name="category_id"  value="0">

                        <div>
                            <h3>Search Swipes</h3>
                            <section>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label class="col-md-2  control-label test-right"></label>
                                    <div class="col-md-8">
                                        <div class="input-group">

                                            <div id="search_subject"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!--  <div class="load-subject col-md-12"></div>-->
                                    <table id="load_datatable" class="table table-colored table-inverse table-striped table-bordered dataTable no-footer">
                                        <thead>
                                        <tr>
                                            <th>Result</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="5">No Record found yet.</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="clearfix"></div>
                            </section>

                            <h3>Modify Email</h3>
                            <section>
                                <div class="clearfix"></div>

                                <div class="form-group pull-right">
                                    <button type="button" class="btn btn-success  w-md " onclick='addGallery()' >Gallery</button>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2  control-label">Subject:</label>
                                    <div class="col-md-8">
                                        <input id="searched_subject" name="subject" value="{!! (isset($subject->subject)) ? $subject->subject : '' !!}" class="form-control" required autocomplete="off" >
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="col-lg-12">
                                        <textarea class="form-control" id="content" rows="5" placeholder="some details" > {!! (isset($content->content)) ? $content->content : '' !!}</textarea>
                                        <br>
                                        <button type="button" class="btn btn-success  w-md " onclick='emailTips()' > Email Tips</button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </section>

                            <h3>Preview</h3>
                            <section>
                                <div class="clearfix"></div>
                                <div class="form-group ">
                                    <div class="col-md-2 pull-right">

                                        <!--<button id="email_download" type="submit" class="btn btn-success  w-md "> <i class="fa fa-download"></i> Download</button>-->
                                        <button id="send_file" type="button" onclick='$("#download_file").submit();' class="btn btn-success  w-md "> <i class="fa fa-download"></i> Download</button>
                                    </div>

                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#desktop" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="fa fa-laptop"></i></span>
                                                <span class="hidden-xs">Desktop</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#profile" data-toggle="tab" aria-expanded="false">
                                                <span class="visible-xs"><i class="fa fa-mobile"></i></span>
                                                <span class="hidden-xs">Mobile</span>
                                            </a>
                                        </li>

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="desktop">

                                            <img src="{{ asset('assets/images/desktop.png')  }}" alt="">

                                            <div class="desktop-body">
                                                <div class="subject">
                                                    <!-- loading subject -->
                                                    <b><span></span></b>
                                                </div>
                                                <div class="email-body">
                                                    <!-- loading content -->
                                                    <p><span></span> </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile">
                                            <div class="col-md-4" id="mobile">

                                                <div class="text-center mobile-view">


                                                    <img src="{{ asset('assets/images/mob.png')  }}" alt="">

                                                    <div class="mobile-body">

                                                        <div class="mobile-subject">
                                                            <b><span></span></b>
                                                        </div>
                                                        <div class="email-body slimScrollBar">
                                                            <p><span></span> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8" id="landscape">
                                                <div class="text-center landscape-view">
                                                    <img src="{{ asset('assets/images/landscape.png')  }}" alt="">
                                                    <div class="landscape-body">
                                                        <div class="subject">
                                                            <b><span></span></b>
                                                        </div>
                                                        <div class="email-body">
                                                            <p><span></span> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </section>

                            <h3>Send</h3>
                            <section>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12">
                                    <label class="col-md-2  control-label">Select Autoresponder:</label>
                                    <div class="col-md-8">
                                        <select name="type" id="" class="form-control" required="" onchange="load_ar_campains(this)">
                                            <option></option>
                                            <option value="aweber">Aweber</option>
                                            <option value="get_response">Get response</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="append_campaigns col-md-12" >

                                </div>

                                <div class="clearfix"></div>
                            </section>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <form id="download_file" action="{!! route('download-email') !!}" method="post" style="display: none;" >
        <input type="hidden" name="file_subject" value="" >
        <input type="hidden" name="file_content" value="">
    </form>

    <!-- modal gallery -->
    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;"> </div>

    <!-- email tips -->
    <div id="email_tips" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="modal-title text-center" id="custom-width-modalLabel">5 simple steps to make your email design responsive</h3>
                </div>
                <div class="modal-body">
                    <p>Email newsletters created with MailerLite are optimized for mobile devices automatically. In order to achieve the responsive look we’ve made a few changes to the email styles:</p>
                    <ul class="">
                        <li> Font size for  headlines – 24 px
                        <li> Font size for text – 16px </li>
                        <li> All images are shown in one column and over the entire length </li>
                        <li> All buttons are shown in one column and over the entire length </li>
                        <li> While most of the work is done for you, there are extra things you can do to make your emails look even better on a mobile device! </li>
                    </ul>
                    <h4>#1. Short subject line + preheader</h4>

                    <p><img src="{{asset('assets/images/email-tip-1.jpg')}}" alt="" class="img-responsive"></p>

                    <p>The preheader is the first bit of text that shows up in many email clients immediately after the subject line.</p>
                    <p>Those snippets appear in most mobile devices and in some desktop clients, such as Gmail and Outlook.</p>
                    <p>iPhone cuts off subject lines after 35 characters in portrait view, but displays about 140 characters of preheader or two full lines (that’s a lot!).</p>
                    <p></p>Don’t use preheader to say… Can’t see images? Click here to view this email in your browser.</p>
                    <p>It makes way more sense to write short subject line and then expand your message with preheader.</p>
                    <p>For example, instead of subject line “Summer is finally here: 50% off all dresses�?, write “Summer if finally here�? as a subject line and then “50% off all dresses�? in preheader.</p>
                    <h4>#2. CTA designed for fingers, not a mouse</h4>
                    <p><img src="{{asset('assets/images/email-tip-2.jpg')}}" alt="" class="img-responsive"></p>
                    <p>Your call-to-action is the most important part of your email.</p>
                    <p>If you include several links in the text, make sure there’s enough space between then and can’t tap the wrong link.</p>
                    <p>The way we code the buttons in MailerLite the whole area is clickable, not just the text in the middle. This way it’s even more user friendly on smaller devices.</p>
                    <h4>#3. Short paragraphs</h4>

                    <p>Keep paragraphs short, so it’s easy for readers to skim the message and understand what they are supposed to do next.</p>
                    <p>Also a longer paragraph will mean more vertical scrolling on mobile device, so the rule “less is more�? applies very well here..</p>

                    <h4>#4. Power of images</h4>
                    <p> Image is a powerful tool to convince your customers to act. Let’s say you are selling plane tickets to Nice in France. What will you include in your newsletter? Price. Date. And… Photo of sunset in Nice. That will greatly help your customers to imagine what they can get if they purchase the ticket. It’s much easier to sell sunsets than flight tickets, right?</p>
                    <p>Use images (photos, videos or infographics) that convey or support your message and get strategic about where you place those images, as they’re key to drawing the eye (and moving the scroll bar) down the screen.</p>
                    <h4>#5. Mobile friendly landing pages</h4>
                    <p>Now you have an email that looks incredible on any mobile device. A reader clicks on your call-to-action and finds a mess of a landing page. That’s it.</p>
                    <p>Your reader closes the window, eats breakfast and forgets you exist. What a shame! It’s so important to have landing pages optimized for mobile devices.</p>
                    <p>We have several landing page templates that are ready out-of-the-box, fully responsive and just waiting for you to add your texts and media.</p>

                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <!-- /.modal -->


</div>
<script src="{!! asset('assets/js/ckeditor/ckeditor.js') !!}"></script>
<script>
    $(document).ready(function(){

        // ck editor
        CKEDITOR.replace('content',{
            allowedContent: true
        });
        // datatable

        $('#load_datatable').DataTable({
            "order": [[0, 'desc']],
            processing: true,
            serverSide: true,
            "initComplete": function (settings, json) {
                $("#load_datatable_filter").detach().appendTo('#search_subject');
            },
            "drawCallback": function () {

            },

            ajax: "{!! Route('choose-subject2') !!}",
            columns: [
                {data: 'subject', name: 'subject'}

//                {data: 'action', name: 'action', orderable: false, searchable: false}
                //{ data: 'updated_at', name: 'updated_at' }
            ]

        });








    // ajax submit form
    $("#subject-template").submit(function(){
        $('#loading').show();
        var data = new FormData(this);
        data.append('content', CKEDITOR.instances.content.getData());
        data.append('content_copy', CKEDITOR.instances.content.getData());

        var check = checkGetResponseChecked();
        if(check==0){ $('#loading').hide(); return false;  }

        $.ajax({
            url: "<?php  echo route('new-template.store'); ?>",
            data: data,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(result){

                if(Number(result) == 1 || Number(result) > 1){
                    $('#loading').hide();
                    // swal("Good job!", "Subject has been saved successfully.", "success")
                    if(Number(result) > 1){
                        $('input[name="id"]').val(result);
                    }
                    swal({
                        title: "Good job!",
                        text: "Email has been saved successfully!",
                        type: "success",
                        confirmButtonText: "OK"
                    })

                }else{
                    swal("Error!", "Something went wrong.", "error");
                    $('#loading').hide();
                }
            }
        });
        return false;
    });
    //end ajax submit form

    $("#emailSearchBtn").click(function(){
        //alert('ok');
        $('.load-subject').show();
        var subject = $("#search_subject").val();
        if(subject !="" ){
            $('.load-subject').html('loading...');
            $.post("<?php echo route('choose-subject'); ?>", {subject:subject},
                    function(data){
                        $('.load-subject').html(data);
                    }
            )
        }else{
            $('.load-subject').hide();
        }
    });

    // download file
    $("#download_file").submit(function(){
        $("[name='file_subject']").val($("[name='subject']").val());
        $("[name='file_content']").val(CKEDITOR.instances.content.getData());
        return true;
    });
    // on next button click loading data to preview
    $('.actions ul li:nth-child(2), #steps-uid-0-t-3').click(function(){

        var subject = $("#searched_subject").val();
        var content =  CKEDITOR.instances.content.getData();
        $(".subject span").html(subject);
        // short subject for mobile view
        $(".mobile-subject span").html(subject.substring(0,33)+"...");

        $(".email-body span").html(content);
        // toastr["success"]("Status Updated successfully!");
    });



    });


    // load modal
    function addGallery(){

        $.get("<?php echo route('gallery.index'); ?>", function(data){
            $( "#con-close-modal").html(data);
        });

        $("#con-close-modal").modal('show');
    }
    // Email tips

    function emailTips(){
        $("#email_tips").modal('show');
    }

    function load_sub(e){
        var for_id = $(e).attr('id');
        var subject = $('label[for="'+for_id+'"] span b').html();
        $('#searched_subject').val(subject);
    }

    function load_content(e){
        var content_id =   $(e).val();

        $.post("<?php echo route('load-searched-email'); ?>", { content_id:content_id},
                function(data){
                    //$('#content').val(data);
                    CKEDITOR.instances.content.setData(data);
                });
    }

    function load_ar_campains(x){

        $(".append_campaigns").html("");
        var type = $(x).val();

        if(type==''){ return false; }

        $('#loading').show();
        $.get("<?php echo route('load_ar_campaings'); ?>", { type:type},
                function(res){

                    if(res=='get_response_api_failed'){
                        swal("Error!", "Get Response API Key not found. Please provide you API Key in Settings!", "error");
                    }

                    if(res=='aweber_credentials_failed'){
                        swal("Error!", "Please provide aweber Consumer Key and Secret, and generate Aweber Access Token in Settings page.!", "error");
                    }

                    if(res!='get_response_api_failed' && res!='aweber_credentials_failed'){
                        $(".append_campaigns").html(res);
                        $('[data-toggle="tooltip"]').tooltip();
                    }

                    $('#loading').hide();

                });
    }

    //checking all required info for get response newslettter sending
    function checkGetResponseChecked(){
        var checkbox = $("#send_get_response");

        if(checkbox){
            var msg = '';
            if($(checkbox).is(":checked")){

                var title       = $('input[name="newsletter_title"]').val();
                var fromFieldId = $('select[name="fromFieldId"]').val();
                var campaignId  = $('select[name="campaignId"]').val();

                if(title=='' || title==null){ msg = 'Please select Campaign.' }
                if(fromFieldId=='' || fromFieldId==null){ msg = 'Please select From Email address.' }
                if(campaignId=='' || campaignId==null){ msg = 'Please select Campaign.' }
            }
            if(msg!=''){
                swal("Error!", msg, "error");
                return 0;
            }
        }
        return 1;
    }

</script>

<!--Form wizard-->
<script src="{!! asset('assets/js/jquery.steps.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/js/jquery.validate.min.js') !!}" type="text/javascript"></script>

<!--wizard initialization-->
<script src="{!! asset('assets/pages/jquery.wizard-init.js') !!}" type="text/javascript"></script>
@endsection
