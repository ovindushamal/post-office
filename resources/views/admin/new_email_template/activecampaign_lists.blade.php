@if(isset($list))

<?php
$star = '<span class="text-danger">*</span>';
?>
<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<!--<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-2 control-label">Subject {!! $star !!}:</label>
        <div class="col-md-8">
            <input type="text" name="active_title[]" value="" class="form-control active_title" required autocomplete="off" >
        </div>
    </div>
</div>-->

<div class="col-md-12 hidden">
    <div class="form-group">
        <label class="col-md-2 control-label">From Name {!! $star !!}:</label>
        <div class="col-md-8">
            <input type="text" name="active_from_name[]" value="{{ Auth::user()->name }}" class="form-control" required autocomplete="off" >
        </div>
    </div>
</div>

<div class="col-md-12 hidden">
    <div class="form-group">
        <label class="col-md-2 control-label">From Email {!! $star !!}:</label>
        <div class="col-md-8">
            <input type="email" name="active_from[]" value="{{ Auth::user()->email }}" class="form-control" required autocomplete="off" >
        </div>
    </div>
</div>



<div class="col-md-12 hidden">
    <div class="form-group">
        <label class="col-md-2 control-label">reply To {!! $star !!}:</label>
        <div class="col-md-8">
            <input type="email" name="active_reply_to[]" value="{{ Auth::user()->email }}" class="form-control" required autocomplete="off" >
        </div>
    </div>
</div>

<!--<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-2 control-label">Campaign Title {!! $star !!}:</label>
        <div class="col-md-8">
            <input type="text" name="active_campaign_title[]" value="" class="form-control" required autocomplete="off" >
        </div>
    </div>
</div>-->
<?php
$count = 0;
foreach($list as $value){
    if( isset($value->name)){
        $count++;
    }
}

?>

    <div class="col-md-12 m-t-20" {{ ($count == 1)? 'hidden' : '' }}>
    <div class="form-group">
        <label class="col-md-2 control-label">Select List(s) {!! $star !!}:</label>
        <div class="col-md-8">
            <select name="list_ids[{{$id}}][]" id="activecampaign_dropdown{{$id}}" class="form-control" required multiple>

                <?php foreach ($list as $value){
                    if( isset($value->name)){  ?>
                    <option {{ ($count == 1)? 'selected' : '' }} value="<?= $value->id ?>"> <?= $value->name ?></option>
               <?php
                    }
                }
                ?>
            </select>

        </div>
    </div>
</div>

<div class="col-md-12 m-t-20 hidden">
    <div class="form-group">
        <label class="col-md-2  control-label"></label>
        <div class="col-md-8">
            <label for="send_sendlane_broadcast">
                <input type="checkbox" checked name="activecompaign" id="activecompaign" value="1" > Send Email
            </label>
        </div>
    </div>
</div>
<script src="{!! asset('assets/js/bootstrap-multiselect.js') !!}?v={{ time() }}"></script>
<script>
    $(document).ready(function () {

        $('#activecampaign_dropdown{{$id}}').multiselect({
            allSelectedText: 'All',
            maxHeight: 200,
            includeSelectAllOption: true,
            nonSelectedText: 'Select List(s)',
            nSelectedText: 'selected',
            allSelectedText: 'All selected',
            dropUp: true,
        });
    });
</script>

@else
    <div class="col-md-2"></div>
    <div class="col-md-10">
        <p class="help-text text-danger"><small>No list found, Please create List and add subscriber in your Activecampaign Account.</small></p>
    </div>
@endif