
<?php $id = time() ?>

<div class="col-md-12 {{ (count($api_list) == 1)? 'hidden' : '' }}" style="margin-top: 20px;" >
    <div class="form-group" >
        <label class="col-md-2  control-label">Select API <span class="text-danger">*</span>:</label>
        <div class="col-md-8" >
            <select name="api_key" id="api_key{{ (count($api_list) == 1)? $id : '' }}" class="form-control" required onchange="load_ar_campains(this)" >
                @if(count($api_list) != 1))
                    <option disabled selected ></option>
                @endif
                <?php
                if(count($api_list)>0){
                    foreach($api_list as $k=>$v){
                        ?><option {{ (count($api_list) == 1)? 'selected' : '' }} value="{!! $v->id !!}">{!! $v->account_title !!}</option><?php
                    }
                }
                ?>
            </select>
            <p class="help-text"><small>Select an API key.</small></p>
        </div>
    </div>
</div>

<?php
if((count($api_list) == 1)){
    $list_id='';
    foreach($api_list as $k=>$v){
        $list_id = $v->id;
    }
?>
<script>
$(document).ready(function () {
    $('#api_key{{ (count($api_list) == 1)? $id : '' }}').val(<?= $list_id ?>).change();

});
</script>

<?php } ?>
