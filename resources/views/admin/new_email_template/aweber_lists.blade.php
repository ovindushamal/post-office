@if(!isset($data))
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <div class="col-md-2"></div>
    <div class="col-md-10">
        <p class="help-text text-danger">{{ $err }}</p>
    </div>
   @else

<?php
$star = '<span class="text-danger">*</span>';
?>

    <div class="col-md-12 {{ (count($data) == 1)? 'hidden' : '' }}" style="margin-top: 20px;" >
        <div class="form-group" >
            <label class="col-md-2  control-label">Select List(s) {!! $star !!}:</label>
            <div class="col-md-8" >
                <select name="selectedList[{{$id}}][]" id="aweber_dropdown{{$id}}" class="form-control" multiple="multiple" required >

                    <?php
                    if(count($data)>0){
                        foreach($data as $k=>$v){   ?>
                        <option {{ (count($data) == 1)? 'selected' : '' }} value="{!! $v['id'] !!}">{!! $v['name'] !!}</option><?php
                        }
                    }
                    ?>
                </select>
                <div class="clearfix"></div>
                <p class="help-text"><small>Select a List(s) to which you want to send this Broadcast.</small></p>
            </div>
    </div>
</div>


<div class="col-md-12 hidden">
    <div class="form-group">
        <label class="col-md-2  control-label"></label>
        <div class="col-md-8">
            <label for="send_aweber_broadcast">
                <input type="checkbox" checked name="send_aweber_broadcast" id="send_aweber_broadcast" value="1" > Send Email
            </label>
        </div>
    </div>
</div>
    <script src="{!! asset('assets/js/bootstrap-multiselect.js') !!}?v={{ time() }}"></script>
    <script>
        $(document).ready(function () {

            $('#aweber_dropdown{{$id}}').multiselect({
                allSelectedText: 'All',
                maxHeight: 200,
                includeSelectAllOption: true,
                nonSelectedText: 'Select List(s)',
                nSelectedText: 'selected',
                allSelectedText: 'All selected',
                dropUp: true,
            });
        });
    </script>

@endif
