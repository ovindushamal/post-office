<?php

namespace App\Http\Controllers\Admin;

use App\Campaign;
use App\Link;
use App\LinkStat;
use App\OptinsSaleStats;
use App\Rotator;
use App\RotatorLink;
use App\RotatorStat;
use App\Settings;
use App\Shortcut;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    private  $notification_subject, $notification_to_email = "";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$domains = DB::table('cpanels')->select('id')->where('domain', '=', $v)->first();

        return view("super_admin.user.list");
    }

    public function create()
    {

    }

    public function loadProfileSetting(){
        $user = User::all()->where('id', Auth::user()->id)->first();
        return view("admin.user.profile_settings")->with('user', $user);
    }

    // profile setting
    function profileSetting(Request $request){

        $is_file = true;
        $old_file = Auth::user()->image;

        $user =   User::find(Auth::user()->id);
        if($user){
            if ($request->hasFile('image')) {
                $is_file = true;
                $file_name = $input['image'] = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(base_path() . '/assets/images/users/', $input['image']);
                $update_data = array('time_zone' => $request->time_zone ,'password' => bcrypt($request->password), 'name' => $request->name, 'plain_password' => $request->password, 'image' => $file_name);
            }else{
                $file_name = $old_file;
                $update_data = array('time_zone' => $request->time_zone ,'password' => bcrypt($request->password), 'name' => $request->name, 'plain_password' => $request->password );
            }


            $update = DB::table('users')
                ->where('id', Auth::user()->id)
                ->update( $update_data );

            if($update){
                // Delete old image
                if($is_file) {
                    if (file_exists(base_path('assets/images/users/' . $old_file)) && $old_file != null ) {
                        unlink(base_path('assets/images/users/' . $old_file));
                    }
                }
                return $file_name;
            }else{
                return 2;
            }
        }
    }


    public function store(Request $request)
    {
        if (User::where('email', $request->email)->where('id','!=', $request->id)->exists()) {
            return response()->json(['msg'=>2]);
            exit;
        }

        $obj = new User();
        if($request->id!=''){
            $obj = $obj->findOrFail($request->id);
        }

        /* Start for request on add-user */
        $status = 'Inactive';
        if($request->status==1){
            $obj->status = $request->status;
            $status = 'Active';
        }
        /* Ends for request on add-user */

        $obj->fill($request->all());
        $obj->account_type = 1;
        $obj->password = bcrypt($request->password);
        $obj->unencrypted_password = $request->password;

        if($obj->save()){
            $email_temp = DB::table('email_settings')->select('registration_subject','registration_content')->where('user_id', Auth::user()->id)->first();

            $subject = $email_temp->registration_subject;
            $content = $email_temp->registration_content;
            $content = str_replace('%email%', $request->email, $content);
            $content = str_replace('%name%', $request->first_name . ' ' .$request->last_name, $content);
            $content = str_replace('%status%', $status, $content);
            $content = str_replace('%password%', $request->password, $content);

            $this->notification_to_email = $request->email;
            $this->notification_subject  = $subject;
//             view('admin.user.email_notification',['subject' => $subject, 'content' => $content]);

            $data = array('email' => $request->email, 'content' => $content, 'subject' => $subject );

            Mail::send('super_admin.account_email.email_notification', $data, function($msg){
                $msg->from('info@uopl.co', 'UOP Link Tracker');
                $msg->subject($this->notification_subject);
                $msg->to($this->notification_to_email);
            });

            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function loadEdit(Request $request)
    {
        $data = User::findOrFail($request->id);
        if($data){
            return response()->json($data);
        }else{
            echo 0;
        }
    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function loadUsers(Request $request)
    {
        $count = 0;
        $data = User::select('*');
        return DataTables::of($data)
            ->editColumn('id', function ($data) {
                global $count;
                $count++;
                return $count;
            })
            ->addColumn('action', function ($data) {
                $is_checked = $data->status==1? "checked" : "";
                $d=$s=$e='';
                if($data->user_type != 1) {
                    $s .= ' <label class="switch m-t-15 m-r-5">
                                <input type="checkbox" '.$is_checked.'  data-id="'.$data->id.'" onchange="change_status(this)">
                                <span class="slide round"></span>
                            </label>';
                    $d = '<a onclick="deleteRow(this)" data-id="' . $data->id . '" data-obj="users" href="javascript:;" title="Delete" class="text-danger"><i class="glyphicon glyphicon-trash"></i></a> ';
                    $e = '<a onclick="editRow(' . $data->id . ')" href="javascript:;" title="Edit" class="text-primary"><i class="glyphicon glyphicon-edit"></i></a> ';
                }
                return $s.$e.$d;
            })
            ->addColumn('type', function($data){
                $type= '';
                if($data->account_type == 1){$type = '<span class="text-info">By Admin</span>';}
                if($data->account_type == 2){$type = '<span class="text-warning">Uopl sign up</span>';}
                if($data->account_type == 3){$type = '<a href="javascript:void(0)" onclick="infPkgDetail(this)" data-id="'.$data->id.'" class="text-primary">InfusionSoft</a>';}
                if($data->account_type == 0 && $data->user_type == 1){$type = '<span class="label label-primary">Admin</span>';}
                return $type;
            })
            ->rawColumns(['type', 'action'])
            ->make(true);
    }

    function deleteUser(Request $request){
        if($request->id!=''){
            $up = User::findOrFail($request->id)->delete();
            if($up){
                /*delete campaigns */
                Campaign::join('tags', 'tags.campaign_id', 'campaign.id')
                ->where('campaign.user_id', $request->id)
                ->delete();
                Link::where('user_id', $request->id)->delete();
                LinkStat::where('user_id', $request->id)->delete();
                OptinsSaleStats::where('user_id', $request->id)->delete();
                DB::table('share')->where('user_id', $request->id)->delete();
                Settings::where('user_id', $request->id)->delete();
                Shortcut::where('user_id', $request->id)->delete();
                Rotator::join('rotator_links', 'rotators.id', 'rotator_links.rotator_id')
                ->where('rotators.user_id', $request->id)->delete();

                RotatorStat::where('user_id', $request->id)->delete();
                echo 1;
            }else{
                echo 2;
            }
        }else{
           echo 2;
        }
    }
    /* change user status  */
    function changeStatus(Request $request){

        if($request->id != '') {
            $user = User::whereId($request->id)->first();
            if ($user->status == 1) {
                $status = 0;
            }
            if ($user->status == 0) {
                $status = 1;
            }
            if ($user->status == 3) {
                $status = 1;
            }

            $update = DB::table('users')->where(['id' => $request->id])->update(['status' => $status]);
            if($update) {
                return response()->json(array('msg' => $status));
            }
        }
    }
    function infuPkgInfo(Request $request){
        if($request->id!=''){
            $data = DB::table('user_subscriptions')->first();
            return view('super_admin.user.infusion_info', compact('data'));
        }
    }
}