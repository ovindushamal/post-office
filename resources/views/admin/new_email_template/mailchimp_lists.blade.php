 <?php

 if(isset($data)){

$star = '<span class="text-danger">*</span>';
$count = 0;
?>
 <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
 <!--<div class="col-md-12">
     <div class="form-group">
         <label class="col-md-2 control-label">Title {!! $star !!}:</label>
         <div class="col-md-8">
             <input type="text" name="title[]" id="title" class="form-control title" required >
         </div>
     </div>
 </div>-->

 <div class="col-md-12 hidden">
     <div class="form-group">
         <label class="col-md-2 control-label">From name {!! $star !!}:</label>
         <div class="col-md-8">
             <input type="text" name="from_name[]" id="from_name" value="{{ Auth::user()->name }}" class="form-control" required >
             <p class="help-text"><small>Name of the Sender.</small></p>
         </div>
     </div>
 </div>

 <div class="col-md-12 ">
     <div class="form-group">
         <label class="col-md-2 control-label">Reply to {!! $star !!}:</label>
         <div class="col-md-8">
             <input title="Email of your registered domain at mailchimp account!" type="email" name="reply_to[]" id="reply_to" placeholder="example@yourdomain.com" value="" class="form-control" required >
             <p class="help-text"><small>An email address at which you can receive reply from Receiver.</small></p>
         </div>
     </div>
 </div>

    <div class="col-md-12 {{ (count($data) == 1)? 'hidden' : '' }}" style="margin-top: 20px;" >
    <div class="form-group" >
        <label class="col-md-2  control-label">Select List(s) {!! $star !!}:</label>
        <div class="col-md-8" >
            <select name="list_id[{{$id}}][]" id="mauilchimp_dropdown{{$id}}" multiple="multiple" class="form-control" required >

                <?php
                if(count($data)>0){
                    $count = count($data);
                    foreach($data as $k=>$v){
                        ?><option {{ (count($data) == 1)? 'selected' : '' }} value="{!! $v->id !!}">{!! $v->name !!}</option><?php
                        //echo $v->id.' - '.$v->web_id.' - '.$v->name."<br>";
                    }
                }
                ?>
            </select>
            <p class="help-text"><small>Select a List(s) to which you want to send mail.</small></p>
            <?php
                if($count==0){
                    echo '<p class="help-text text-danger"><small>No list found, Please create List and add subscriber in your MailChimp Account.</small></p>';
                }
            ?>
        </div>
    </div>
</div>

<div class="col-md-12 m-t-20 hidden">
    <div class="form-group">
        <label class="col-md-2  control-label"></label>
        <div class="col-md-8">
            <label for="send_mailchimp_broadcast">
                <input type="checkbox" checked name="send_mailchimp_broadcast" id="send_mailchimp_broadcast" value="1" > Send Email
            </label>
        </div>
    </div>
</div>
 <script src="{!! asset('assets/js/bootstrap-multiselect.js') !!}?v={{ time() }}"></script>
 <script>
     $(document).ready(function () {

         $('#mauilchimp_dropdown{{$id}}').multiselect({
             allSelectedText: 'All',
             maxHeight: 200,
             includeSelectAllOption: true,
             nonSelectedText: 'Select List(s)',
             nSelectedText: 'selected',
             allSelectedText: 'All selected',
             dropUp: true,
         });
     });
 </script>

 <?php }else{ ?>
 <div class=" col-md-12">
     <div class="col-md-2"></div>
     <div class="col-md-10">
         <p class="help-text text-danger">{{ $err }}</p>
     </div>
 </div>
<?php } ?>
