<?php

$star = '<span class="text-danger">*</span>';
?>

<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-2 control-label">Title:</label>
        <div class="col-md-8">
            <input type="text" name="broadcast_title[]" value="" class="form-control broadcast_title" required autocomplete="off" >
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px;" >
    <div class="form-group" >
        <label class="col-md-2  control-label">Send Time {!! $star !!}:</label>
        <div class="col-md-8" >
            <div>
                <div class="col-md-2" >
                    <select name="hour[]" class="form-control" required >
                        <option value="01">1</option>
                        <option value="02">2</option>
                        <option value="03">3</option>
                        <option value="04">4</option>
                        <option value="05">5</option>
                        <option value="06">6</option>
                        <option value="07">7</option>
                        <option value="08">8</option>
                        <option value="09">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12" selected="">12</option>
                    </select>
                </div>
                <div class="col-md-2" >
                    <select name="minute[]" class="form-control" required >
                        <option value="00" selected="">00</option>
                        <option value="5">05</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="25">25</option>
                        <option value="30">30</option>
                        <option value="35">35</option>
                        <option value="40">40</option>
                        <option value="45">45</option>
                        <option value="50">50</option>
                        <option value="55">55</option>
                    </select>
                </div>
                <div class="col-md-2" >
                    <select name="meridian[]" class="form-control" required >
                        <option selected="">am</option>
                        <option>pm</option>
                    </select>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="col-md-12" style="margin-top: 20px;" >
    <div class="form-group" >
        <label class="col-md-2  control-label">Select List(s) {!! $star !!}:</label>
        <div class="col-md-8" >
            <select name="selectedList[]" id="selectedList" class="form-control" required >
                <option disabled selected ></option>
                <?php
                if(count($data)>0){
                foreach($data as $k=>$v){
                ?><option value="{!! $v->list_id !!}">{!! $v->list_name !!}</option><?php
                }
                }
                ?>
            </select>
            <p class="help-text"><small>Select a List(s) to which you want to send this Broadcast.</small></p>
        </div>
    </div>
</div>


<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-2  control-label"></label>
        <div class="col-md-8">
            <label for="send_sendlane_broadcast">
                <input type="checkbox" name="send_sendlane_broadcast" id="send_sendlane_broadcast" value="1" > Send Email
            </label>
        </div>
    </div>
</div>
