@extends('layouts.app')

@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">

                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active"> Server creation</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">

                        <div class=" p-t-30">
                            <div class="panel panel-color panel-inverse">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Server Creation</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="col-xs-12">
<iframe width="100%" height="700px" src="http://nimblemessaging.com/cpanel_account/create_cpanel.php?action={{$_REQUEST['action']}}&domain={{$_REQUEST['domain']}}&user={{$_REQUEST['user']}}&password={{$_REQUEST['password']}}&email={{$_REQUEST['email']}}&db_name={{$_REQUEST['db_name']}}&db_user={{$_REQUEST['db_user']}}&db_pass={{$_REQUEST['db_pass']}}" frameborder="0"></iframe>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="clearfix"></div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

