@extends('layouts.app')
@section('title', 'Categories')
@section('content')
    <style>
        form#add_category .loader {
            float: right;
            margin-left: 5px;
            margin-top: 6px;
        }
        form#add_category .loader img{
            display: none;
        }
        .add_more{
            float: right;
            margin-bottom: 5px;
        }

        span a{ color: white; }
        .modal-lg{ width:95% }
        .m-b-10{
            margin-bottom: 2px !important; display: inline-block;
        }

    </style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Email Sender Settings </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="col-xs-12">
                    <div class="row">
                        <div class="card">
                            <div class="card-header card-header-text" data-background-color="rose">
                                <h3 class="card-title"> <i class="fa fa-cog"></i> Email Sender Settings</h3>
                            </div>
                            <div class="card-content">
                                <!-- checkbox sync  -->
                                <div class="togglebutton">
                                    <label>
                                        <input id="checkbox-signup" name="remember" {{ (Auth::user()->sync == 1)? 'checked': '' }} type="checkbox" class="sync_contacts">
                                        Please enable data Sync to see the detailed statistics and use of SMTP services.
                                    </label>
                                </div>
                                <!--<li id="sync_contacts"><a href="javascript:void(0)">synchronize Contacts</a></li>-->
                                <hr>
                                <ul class="nav nav-pills nav-pills-warning">
                                    <li class="active"><a data-toggle="tab" href="#sendgrid_tab">SendGrid</a></li>
                                    <li><a data-toggle="tab" href="#sparkpost_tab">SparkPost</a></li>
                                    <li><a data-toggle="tab" href="#smtp_tab">Smtp</a></li>
                                    <li><a data-toggle="tab" href="#mailgun_tab">MailGun</a></li>
                                    <li><a data-toggle="tab" href="#mailjet_tab">MailJet</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="elasticmail_tab" class="tab-pane fade in">
                                        <small class="label label-danger m-b-10"> <strong>Note: </strong> ElasticMail services will be available soon! </small>
                                    </div>

                                    <div id="mailreq_tab" class="tab-pane fade in">
                                        <small class="label label-danger m-b-10"> <strong>Note: </strong> Mailreq services will be available soon! </small>
                                    </div>

                                    <div id="sendmail_tab" class="tab-pane fade in">
                                        <small class="label label-danger m-b-10"> <strong>Note: </strong> SendMail services will be available soon! </small>
                                    </div>

                                    <div id="pickupdirectory_tab" class="tab-pane fade in">
                                        <small class="label label-danger m-b-10"> <strong>Note: </strong> Pickup Directory services will be available soon! </small>
                                    </div>

                                    <div id="mandrill_tab" class="tab-pane fade in">
                                        <small class="label label-danger m-b-10"> <strong>Note: </strong> Mandrill services will be available soon! </small>
                                    </div>

                                    <div id="sendgrid_tab" class="tab-pane fade in active">
                                        <!--  ************** sendgrid Information ***************** -->
                                        <div class="form-group">
                                            <div class="alert alert-info">
                                                Sendgrid Information
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-purple" data-toggle="modal" data-target="#aweberModal" >Add More</button>

                                        </div>
                                        <table class="table table-response table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="20%">Title</th>
                                                <th>Api Key</th>
                                                <th>Sender Name</th>
                                                <th>Sender Email</th>
                                                <th>Status</th>
                                                <th colspan="2">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(count($data_sendgrid)>0){
                                            foreach($data_sendgrid as $k=>$v){
                                            ?>
                                            <tr>
                                                <td class="sparkpost_{!! $v->id !!}">{!! $v->title !!}</td>
                                                <td>{!! '**************'. substr( $v->api_key, -4 ) !!}</td>
                                                <td>{{ $v->from_name }}</td>
                                                <td>{{ $v->from_email }}</td>

                                                <td> <i class="fa fa-{{ ($v->status == 0)? 'times text-danger':'check text-success' }}"></i></td>

                                                <td width="100px">
                                                    <a onclick="deleteRow(this)" data-id="{!! $v->id !!}" data-obj="sendgrid_settings" href="javascript:;" title="Delete" data-toggle="tooltip" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            }else{
                                                echo '<tr><td colspan="6">No data found</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <div id="aweberModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                        <h4 class="modal-title text-center">Add/Sendgrid </h4>
                                                    </div>
                                                    <form class="form-horizontal" role="form" id="sendgrid_form" >

                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="">
                                                        <input type="hidden" name="object" value="sendgrid_settings">

                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="title" class="col-md-12">Title:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="title" id="title" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_name" class="col-md-12">From Name:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="from_name" id="from_name" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_email" class="col-md-12">From Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email" placeholder="example@yourdomain.com" name="from_email"  id="from_email" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Api Key:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text"  name="api_key" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Test Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email"  name="test_email" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-xs btn btn-default " data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class=" btn-xs btn btn-info">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- /.modal -->
                                    </div>
                                    <div id="mailjet_tab" class="tab-pane fade in">
                                        <!--  ************** mailjet Information ***************** -->
                                        <div class="form-group">
                                            <div class="alert alert-info">
                                                MailJet Information
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-purple" data-toggle="modal" data-target="#mailjetModal" >Add More</button>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-response table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th width="20%">Title</th>
                                                    <th>Public Api Key</th>
                                                    <th>private Api Key</th>
                                                    <th>Sender Name</th>
                                                    <th>Sender Email</th>
                                                    <th>Status</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(count($data_mailjet)>0){
                                                foreach($data_mailjet as $k=>$v){
                                                ?>
                                                <tr>
                                                    <td class="sparkpost_{!! $v->id !!}">{!! $v->title !!}</td>
                                                    <td>{!! '**************'. substr( $v->api_key, -4 ) !!}</td>
                                                    <td>{!! '**************'. substr( $v->private_api_key, -4 ) !!}</td>
                                                    <td>{{ $v->from_name }}</td>
                                                    <td>{{ $v->from_email }}</td>
                                                    <td> <i class="fa fa-{{ ($v->status == 0)? 'times text-danger':'check text-success' }}"></i></td>

                                                    <td width="100px">
                                                        <a onclick="deleteRow(this)" data-id="{!! $v->id !!}" data-obj="mailjet_settings" href="javascript:;" title="Delete" data-toggle="tooltip" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                }else{
                                                    echo '<tr><td colspan="7">No data found</td></tr>';
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="mailjetModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                        <h4 class="modal-title text-center">Add/Mailjet </h4>
                                                    </div>
                                                    <form class="form-horizontal" role="form" id="mailjet_form" >
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="">
                                                        <input type="hidden" name="object" value="mailjet_settings">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="title" class="col-md-12">Title:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="title" id="title" class="form-control" required value="" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="from_name" class="col-md-12">From Name:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="from_name" id="from_name" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_email" class="col-md-12">From Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email" placeholder="example@yourdomain.com" name="from_email"  id="from_email" class="form-control" required value="" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Public Api Key:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text"  name="public_api_key" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Private Api Key:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text"  name="private_api_key" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Test Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email"  name="test_email" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-xs btn btn-default " data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class=" btn-xs btn btn-info">Save</button>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div><!-- /.modal -->
                                    </div>

                                    <div id="sparkpost_tab" class="tab-pane fade">
                                        <!--  ************** Get Response Information ***************** -->
                                        <div class="form-group">
                                            <div class="alert alert-info">
                                                Sparkpost Information
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-purple" data-toggle="modal" data-target="#getResponseModal" >Add More</button>
                                        </div>
                                        <table class="table table-response table-striped  table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="20%">Title</th>
                                                <th>Api Key</th>
                                                <th>Sender Name</th>
                                                <th>Sender Email</th>
                                                <th>Status</th>
                                                <th colspan="2">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(count($data_sparkpost)>0){
                                            foreach($data_sparkpost as $k=>$v){
                                            ?>
                                            <tr>
                                                <td class="sparkpost_{!! $v->id !!}">{!! $v->title !!}</td>
                                                <td>{!! '**************'. substr( $v->api_key, -4 ) !!}</td>
                                                <td>{{ $v->from_name }}</td>
                                                <td>{{ $v->from_email }}</td>
                                                <td> <i class="fa fa-{{ ($v->status == 0)? 'times text-danger':'check text-success' }}"></i></td>
                                                <td width="100px">
                                                    <a onclick="deleteRow(this)" data-id="{!! $v->id !!}" data-obj="sparkpost_settings" href="javascript:;" title="Delete" data-toggle="tooltip" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            }else{
                                                echo '<tr><td colspan="6">No data found</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                        <div id="getResponseModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                        <h4 class="modal-title text-center">Add/Update Sparkpost</h4>
                                                    </div>
                                                    <form class="form-horizontal" role="form" id="sparkpost_form" >
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="">
                                                        <input type="hidden" name="object" value="sparkpost_settings">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="title" class="col-md-12">Title:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="title" id="title" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_name" class="col-md-12">From Name:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="from_name" id="from_name" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_email" class="col-md-12">From Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email" placeholder="example@yourdomain.com" name="from_email"  id="from_email" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Api Key:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text"  name="api_key" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Test Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email"  name="test_email" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-xs btn btn-default " data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class=" btn-xs btn btn-info">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- /.modal -->
                                    </div>
                                    <div id="smtp_tab" class="tab-pane fade">
                                        <!--  ************** smtp Information ***************** -->
                                        <div class="form-group">
                                            <div class="alert alert-info">
                                                SMTP Information
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-purple" data-toggle="modal" data-target="#SmtpModal" >Add More</button>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-response table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th width="20%">Title</th>
                                                <th>From Name</th>
                                                <th>From Email</th>
                                                <th>Host</th>
                                                <th>Port</th>
                                                <th>User</th>
                                                <th>Password</th>
                                                <th> SSL </th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(count($data_smtp)>0){
                                            foreach($data_smtp as $k=>$v){
                                            ?>
                                            <tr>
                                                <td class="sparkpost_{!! $v->id !!}">{!! $v->title !!}</td>
                                                <td>{{ $v->from_name }}</td>
                                                <td>{{ $v->from_email }}</td>
                                                <td>{{ $v->host }}</td>
                                                <td>{{ $v->port }}</td>
                                                <td>{{ $v->user }}</td>
                                                <td>{!! '*******'. substr( $v->password, -4 ) !!}</td>
                                                <td> {{ ( $v->smtp_ssl == 0 )? 'No': 'Yes' }} </td>

                                                <td width="100px">
                                                    <a onclick="deleteRow(this)" data-id="{!! $v->id !!}" data-obj="smtp_settings" href="javascript:;" title="Delete" data-toggle="tooltip" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            }else{
                                                echo '<tr><td colspan="7">No data found</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div id="SmtpModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                        <h4 class="modal-title text-center">Add/Update SMTP</h4>
                                                    </div>
                                                    <form class="form-horizontal" role="form" id="smtp_form" >
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="">
                                                        <input type="hidden" name="object" value="smtp_settings">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="title" class="col-md-12">Title:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="title" id="title" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_name" class="col-md-12">From Name:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="from_name" id="from_name" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_email" class="col-md-12">From Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email" placeholder="example@yourdomain.com" name="from_email"  id="from_email" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="host" class="col-md-12">Host:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="host" id="host" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="port" class="col-md-12">Port:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="port" id="port" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="username" class="col-md-12">Username:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="username" id="username" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="password" class="col-md-12">Password:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="password" id="password" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input id="ssl" name="ssl" type="checkbox" value="1" >
                                                                            <span class="text-muted">SSL</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="test_email" class="col-md-12">Test Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email" name="test_email" id="test_email" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-xs btn btn-default " data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class=" btn-xs btn btn-info">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- /.modal -->
                                    </div>
                                    <div id="mailgun_tab" class="tab-pane fade in">
                                        <!--  ************** mailgun Information ***************** -->
                                        <div class="form-group">
                                            <div class="alert alert-info">
                                                MailGun Information
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-purple" data-toggle="modal" data-target="#mailgunModal" >Add More</button>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th width="20%">Title</th>
                                                    <th>Domain</th>
                                                    <th>Api Key</th>
                                                    <th>Sender Name</th>
                                                    <th>Sender Email</th>
                                                    <th>Status</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(count($data_mailgnu)>0){
                                                foreach($data_mailgnu as $k=>$v){
                                                ?>
                                                <tr>
                                                    <td class="mailgun_{!! $v->id !!}">{!! $v->title !!}</td>
                                                    <td>{!! $v->domain !!}</td>
                                                    <td>{!! '**************'. substr( $v->api_key, -4 ) !!}</td>
                                                    <td>{{ $v->from_name }}</td>
                                                    <td>{{ $v->from_email }}</td>

                                                    <td> <i class="fa fa-{{ ($v->status == 0)? 'times text-danger':'check text-success' }}"></i></td>

                                                    <td width="100px">
                                                        <a onclick="deleteRow(this)" data-id="{!! $v->id !!}" data-obj="mailgun_settings" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                }else{
                                                    echo '<tr><td colspan="7">No data found</td></tr>';
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="mailgunModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                        <h4 class="modal-title text-center">Add/MailGun </h4>
                                                    </div>
                                                    <form class="form-horizontal" role="form" id="mailgun_form" >
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="">
                                                        <input type="hidden" name="object" value="mailgun_settings">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="title" class="col-md-12">Title:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="title" id="title" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_name" class="col-md-12">From Name:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" name="from_name" id="from_name" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="from_email" class="col-md-12">From Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email" placeholder="example@yourdomain.com" name="from_email"  id="from_email" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="domain" class="col-md-12">Domain:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text"  name="domain" id="domain" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Api Key:</label>
                                                                <div class="col-md-12">
                                                                    <input type="text"  name="api_key" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="api_key" class="col-md-12">Test Email:</label>
                                                                <div class="col-md-12">
                                                                    <input type="email"  name="test_email" id="api_key" class="form-control" required value="" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn-xs btn btn-default " data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class=" btn-xs btn btn-info">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- /.modal -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!--  add category -->

    <script>


        $(document).ready(function() {
            // ajax submit form


            $("#mailjet_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#sendgrid_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#sparkpost_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#smtp_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });
            $("#mailgun_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });
            $("#dyn_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#amazon_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#sendinblue_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#leadersend_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#tipimail_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

        });

        function commonSubmitReq(data){
            $.ajax({
                url: "<?php  echo route('email_sender_settings'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    $('#loading').hide();

                    var obj = $.parseJSON(result);

                    if(obj.err ==1){
                        swal("Error!", obj.message, "error");
                    }else
                    if(obj.success == 1){
                        swal({
                            title: "Good job!",
                            text: "Record has been saved successfully!",
                            type: "success",
                            confirmButtonText: "OK"
                        }, function(){
                            $('#loading').show();
                            location.reload();
                        });

                    }else{
                        swal("Error!", "Something went wrong.", "error");
                    }
                },
                error: function(error){
                    swal("Error!", "" +
                            "Invalid account credentials!", "error");
                    $('#loading').hide();
                }
            });
        }

    </script>

@endsection
