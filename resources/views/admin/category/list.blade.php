@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<style>
    form#add_category .loader {
        float: right;
        margin-left: 5px;
        margin-top: 6px;

    }
    form#add_category .loader img{
        display: none;
    }
    .add_more{
        float: right;
        margin-bottom: 5px;;
    }

</style>


    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">

                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="#">Categories </a></li>
                        <li class="active"> List </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">


                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif


                <div class="card-box">

                    <div class="row">
                        <div class="add_more">
                            <button type="button" class="btn btn-success  w-md " onclick='addCategory()' >Add More</button>
                        </div>

                        <div class="col-xs-12 bg-white">
                            <div class="material-datatables">
                                <table id="load_datatable" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Dated</th>
                                        <th>Status&nbsp;|&nbsp;Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4">No Record found yet.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!--  add category -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h4 class="modal-title text-center">Add New Category</h4>
            </div>
            <form id="add_category" method="post" role="form" >

                <input type="hidden" name="id" value="" id="id" >

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="category" class="form-control" value="" id="category" placeholder="category name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <textarea class="form-control" id="details" name="details" placeholder="Write something about category" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info  ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->



<script>

    $(document).ready(function(){

        //saving new category
        $("#add_category").submit(function(){

            $('#loading').show();

            var data = new FormData(this);

            $.ajax({
                url: "<?php echo route('category.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                responsive:true,
                type: 'POST',
                success: function(result){
                    $('#loading').hide();
                    if(result=='1'){
                        $('#add_category')[0].reset();
                        $("#id").val("");
                        $("#con-close-modal").modal('hide');
                        swal("Good job!", "Category has been saved successfully.", "success")
                        refreshTable();
                    }else{
                        $('.loader img').css('display','none');
                        swal("Error!", "Something went wrong.", "error");
                    }
                }
            });

            return false;
        });


        $('#load_datatable').DataTable({
            "order": [[0, 'desc']],
            processing: true,
            serverSide: true,
            responsive: true,

            "drawCallback": function () {
                //table_draw();
                $("[name='status']").bootstrapSwitch();
                changeStatus();
            },


            ajax: "{!! Route('load-categories') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'category', name: 'category'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
                //{ data: 'updated_at', name: 'updated_at' }
            ]
        });

    });



    function addCategory(){
        $("#con-close-modal").modal('show');
    }


    function editRow(x){

        $('#loading').show();

        if(x!=''){
            $.post("<?php echo url('category/loadEdit'); ?>", {id: x}, function(result){
                        if(result!='0'){
                            var data = JSON.parse(result);
                            $.each(data, function(k,v){
                                var ref = $("#add_category").find("#"+k);
                                $(ref).val(v);
                                $("#con-close-modal").modal('show');
                            });
                        }else{
                            swal("Error!", "Something went wrong.", "error");
                        }
                        $('#loading').hide();
                    }

            );
        }
    }





</script>

@endsection
