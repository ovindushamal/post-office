@extends('layouts.app')

@section('content')

    <style>

    .auto-stats span{
        font-size: 23px;
    }

    </style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">

                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active"> My Emails </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">

                <!-- imported emails templates  -->
                <div class="col-xs-12">
                    <div class="card-box">
                        <table id="load_datatable_imported" class="table table-bordered table-no-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Subject</th>
                                <th>Category</th>
                                <th width="70">Type</th>
                                <!--<th>Send</th>-->

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="5">No Record found yet.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <form action="{{ route('new-template.create') }}" method="get" style="display: none;" id="use_me">
            <input type="hidden" name="use_me" value="">
        </form>

        <script>
            function submitUseMe(x){
                $('input[name="use_me"]').val($(x).data('id'));
                $('#use_me').submit();
            }
        </script>

        <script>

            $(document).ready(function(){

                $('#load_datatable_imported').DataTable({

                    "pageLength":25,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    "initComplete": function (settings, json) {
                    },
                    ajax: "{!! Route('shared-emails-detail') !!}?sub_id={{ $sub_id }}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'subject', name: 'email_subjects_test.subject'},
                        {data: 'category', name: 'email_sub_category.category'},
                        {data: 'type', name: 'email_sub_category.type'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                        //{data: 'sent', name: 'sent', orderable: false, searchable: false},

                    ]
                });
            });

        </script>


    </div>
    <!-- Counter js  -->
    <script src="{{ asset('plugins/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('plugins/counterup/jquery.counterup.min.js') }}"></script>
@endsection