@extends('layouts.app')
@section('content')
    <style>
    .auto-stats span{
        font-size: 23px;
    }
    </style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box pull-right">
                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active"> Shared Emails </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title"> Import Emails </h3>
                        </div>

                        <div class="card-content">
                            <form role="form" id="import_shared_emails" >
                                {{ csrf_field() }}
                                <input type="hidden" class="account_id" name="account">
                                <input type="hidden" class="target" name="target">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label for="category">Import code:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="shared_code" id="shared_code" class="form-control" placeholder="Enter code here" value="" required autocomplete="off">

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <button type="submit" class="btn btn-sm btn-success pull-right m-t-10">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- imported emails templates  -->
                <div class="col-xs-12">
                    <div class="card-box">
                        <div class="material-datatables">
                            <table id="load_datatable_imported" class="table table-bordered table-no-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>From</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="5">No Record found yet.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="{{ route('new-template.create') }}" method="get" style="display: none;" id="use_me">
            <input type="hidden" name="use_me" value="">
        </form>

        <script>
            function submitUseMe(x){
                $('input[name="use_me"]').val($(x).data('id'));
                $('#use_me').submit();
            }
        </script>

        <script>

            $(document).ready(function(){

                $('#load_datatable_imported').DataTable({

                    "pageLength":25,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    "initComplete": function (settings, json) {
                    },
                    "drawCallback": function () {
                        //table_draw();
                        /*$("[name='status']").bootstrapSwitch();
                         changeStatus();*/
                    },
                    ajax: "{!! Route('load-shared-emails') !!}",
                    columns: [
                        {data: 'index', name: 'index'},
                        {data: 'title', name: 'title'},
                        {data: 'user_name', name: 'user_name'},
                        {data: 'total', name: 'total'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                        //{data: 'sent', name: 'sent', orderable: false, searchable: false},

                    ]
                });

                // import emails code
                $("#import_shared_emails").submit(function(){
                    $('#loading').show();
                    var data = new FormData(this);
                    $.ajax({url: "{{ route('fetch-shared-emails') }}",data: data,
                        contentType: false,
                        processData: false,
                        responsive: true,
                        type: 'POST',
                        success: function(result){
                            $('#loading').hide();
                            if(result == '1'){
                                $('#import_shared_emails_pop').modal('hide');
                                $('#shared_code').val('');
                                swal({  title: "Good job!", text: "Shared Emails are fetched successfully!", type: "success" });
                            }else if(result == 3){
                                swal("Warning!", "The code is already used!", "warning");
                            }else if(result == 4){
                                swal("Warning!", "Invalid code", "warning");
                            }else if (result == 5){
                                swal("Warning!", "Shared code haven't email to import", "warning");
                            }
                            else{
                                swal("Error!", "Something went wrong.", "error");
                            }
                        }
                    });
                    return false;
                });

            });

        </script>


    </div>
    <!-- Counter js  -->
    <script src="{{ asset('plugins/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('plugins/counterup/jquery.counterup.min.js') }}"></script>
@endsection