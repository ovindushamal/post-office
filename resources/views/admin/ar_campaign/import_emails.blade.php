@extends('layouts.app')@section('content')
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box pull-right">

                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li> <a href="{{ url('/shared-emails') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active"> List </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- imported emails templates  -->
                <div class="col-xs-12">
                    <div class="card">

                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title">Import Emails</h3>
                        </div>
                        <div class="card-content">
                            <form role="form" id="import_shared_emails" >
                                {{ csrf_field() }}
                                <input type="hidden" class="account_id" name="account">
                                <input type="hidden" class="target" name="target">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label for="category">Import code:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="shared_code" id="shared_code" class="form-control" placeholder="Enter code here" value="" required autocomplete="off">

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <button type="submit" class="btn btn-sm btn-success pull-right m-t-10">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {

            $("#import_shared_emails").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                $.ajax({url: "{{ route('fetch-shared-emails') }}",data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        $('#loading').hide();
                        if(result == '1'){
                            $('#import_shared_emails_pop').modal('hide');
                            $('#shared_code').val('');
                            swal({  title: "Good job!", text: "Shared Emails are fetched successfully!", type: "success" });
                        }else if(result == 3){
                            swal("Warning!", "The code is already used!", "warning");
                        }else if(result == 4){
                            swal("Warning!", "Invalid code", "warning");
                        }  else{
                            swal("Error!", "Something went wrong.", "error");
                        }
                    }
                });
                return false;
            });
        })
    </script>
@endsection