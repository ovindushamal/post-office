@extends('layouts.app')
@section('title', 'Email Template')
@section('content')

<div class="content">
    <!-- Start content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Help </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header card-header-text" data-background-color="rose">
                    <h3 class="card-title">Help <i class="fa fa-question-circle"></i></h3>
                </div>
                <div class="card-content">

                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>How to Integrate Aweber</h4>
                                <hr />
                                <div class="col-md-12">
                                    <video width="100%" controls>
                                        <source src="{!! asset('assets/support/aweber.MP4') !!}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="col-md-12">
<pre>
    <strong>Missing Step in video:</strong>
    Follow these steps after Copy Consumer and Secret key(s):
    Allow permission from App <a href="javascript:void(0);" data-target="#con-close-modal1" data-toggle="modal" class="link">How to allow permission?</a>
</pre>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>How to Integrate Get Response</h4>
                                <hr />
                                <div class="col-md-12">
                                    <video width="100%" controls>
                                        <source src="{!! asset('assets/support/get-response.MP4') !!}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="col-md-12">
<pre>
    <strong>Missing Step in video:</strong>
    Follow these steps after Copy your key:
    Add This link as mentioned in <a href="javascript:void(0);" data-target="#con-close-modal" data-toggle="modal" class="link">How to add link?</a>
    {{ url('callback') }}
</pre>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Aweber missing steps -->
    <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                    <h4 class="modal-title text-center">Allow Permission From Aweber Labs Account</h4>
                </div>
                <form id="add_category" method="post" role="form" >

                    <div class="modal-body">

                        <h4>Step: 1</h4>
                        <p>Click on Arrow</p>
                        <img class="img img-responsive" src="{!! asset('assets/support/aweber1.jpg') !!}" >

                        <hr />

                        <h4>Step: 2</h4>
                        <p>Click on Permission Settings.</p>
                        <img class="img img-responsive" src="{!! asset('assets/support/aweber2.jpg') !!}" >

                        <hr />

                        <h4>Step: 3</h4>
                        <p>Check the mentioned checkbox and Save</p>
                        <img class="img img-responsive" src="{!! asset('assets/support/aweber3.jpg') !!}" >

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->


    <!-- Get response missing steps -->
    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                    <h4 class="modal-title text-center">Add Link in Get-Response</h4>
                </div>
                <form id="add_category" method="post" role="form" >

                    <div class="modal-body">

                        <h4>Step: 1</h4>
                        <p>Click on Edit Button</p>
                        <img class="img img-responsive" src="{!! asset('assets/support/get-response-callback-1.jpg') !!}" >

                        <hr />

                        <h4>Step: 2</h4>
                        <p>Check the all checkboxes and page the link in text field.</p>
                        <img class="img img-responsive" src="{!! asset('assets/support/get-response-callback-2.jpg') !!}" >

                        <hr />

                        <h4>Step: 3</h4>
                        <p>Reload the page and it will looks like</p>
                        <img class="img img-responsive" src="{!! asset('assets/support/get-response-callback-3.jpg') !!}" >

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->


</div>
@endsection
