<?php
if(!Auth::User()){
    ?>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?php
}
?>

<div class="col-xs-12">

    <div class="panel panel-default">
        <div class="panel-heading"><strong>Help</strong></div>
        <div class="panel-body">

            <div class="col-md-6">

                <div class="form-group">
                    <h4>How to Integrate Aweber</h4>
                    <hr />
                    <div class="col-md-12">
                        <video width="100%" controls>
                            <source src="{!! asset('assets/support/aweber.MP4') !!}" type="video/mp4">
                        </video>
                    </div>
                </div>

            </div>


            <div class="col-md-6">

                <div class="form-group">
                    <h4>How to Integrate Get Response</h4>
                    <hr />
                    <div class="col-md-12">
                        <video width="100%" controls>
                            <source src="{!! asset('assets/support/get-response.MP4') !!}" type="video/mp4">
                        </video>
                    </div>
                </div>

            </div>

        </div>
        <?php
        if(!Auth::User()){
            ?>
            <div class="panel-footer">All rights are reserved.  2016 &copy; Email UpShot.</div>
            <?php
        }
        ?>
    </div>

</div>