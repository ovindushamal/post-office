@extends('layouts.app')
@section('content')
<?php
$setting = DB::table('app_settings')->first();
?>
<div class="content">
    <!-- Start content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a> </li>
                        <li class="active"> Update Version</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-md-12 col-xs-12 col-xxs-12">
                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> <i class="fas fa-sync"></i> Update version</h3>
                    </div>
                    <div class="card-content">
                        <button class="btn btn-info pull-right"> Version  <?php $ver = \Session::get('up_version');  ?> {{ isset($ver->version) && $ver->version !=''? $ver->version : '' }}&nbsp;  <span class="badge badge-danger pull-right"> New</span></button>
                        <button class="btn btn-tumblr pull-right">Current version {{ $setting->version }} </button>
                        <div class="clearfix"></div>
                        <h3> Whats new in <span class="label label-rose">{{ isset($ver->version) && $ver->version !=''? $ver->version : '' }} </span> </h3>
                        <p>{!!  isset($ver->detail) && $ver->detail !=''? $ver->detail : '' !!}</p>

                        <div class=" m-t-30 pull-right">
                            <a href="javascript:void(0)"  onclick="updateVersion()" class="btn btn-success btn-lg btn-update"> <i class="fas fa-sync"></i> Update to {{ isset($ver->version) && $ver->version !=''? $ver->version : '' }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function updateVersion(){
        $('.btn-update i.fas').addClass('fa-spin');
        $.post('{{ url('updates') }}',{'_token': '{{csrf_token()}}'}, function(data){
            if(data.success == 1){
                swal('Success', 'Application updated successfully!', 'success');
                window.location.href='{{ url('home') }}';
            }else if(data.error != ''){
                swal('OOPS!', ' Some error occurred please contact with support ', 'error');
            }
            $('.btn-update i.fas').removeClass('fa-spin');
        });
    }
</script>
@endsection