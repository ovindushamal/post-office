@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<style>
    form#add_category .loader {
        float: right;
        margin-left: 5px;
        margin-top: 6px;

    }
    form#add_category .loader img{
        display: none;
    }
    .add_more{
        float: right;
        margin-bottom: 5px;;
    }

</style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="{{ route('email-sub-category.index') }}">Categories </a></li>
                        <li class="active"> List </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="">

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                    <?php
                    if($category->category_id == 6) $cat_type = 'imported Emails';
                    if($category->category_id == 3) $cat_type = 'User Created Emails';
                    ?>


                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> {{ $category->category }} </h3>
                        <p class="category">{{ $cat_type }}</p>
                    </div>
                        <div class="card-content">
                            <div class="material-datatables">
                                <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Subject</th>
                                        <th>Dated</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4">No Record found yet.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--  add category -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h4 class="modal-title text-center">Add New Category</h4>
            </div>
            <form id="add_category" method="post" role="form" >

                <input type="hidden" name="id" value="" id="id" >

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="category" class="form-control" value="" id="category" placeholder="category name" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info btn-xs">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<form action="{!! Route('new-template.create') !!}" method="get" style="display: none;" id="use_me">
    <input type="hidden" name="use_me" value="">
</form>

<script>
    function submitUseMe(x){
        $('input[name="use_me"]').val($(x).data('id'));
        $('#use_me').submit();
    }
</script>


<script>

    $(document).ready(function(){

        $('#load_datatable').DataTable({
            "order": [[0, 'desc']],
            processing: true,
            serverSide: true,
            responsive: true,
            "initComplete": function (settings, json) {

            },
            "drawCallback": function () {
                //table_draw();
                $("[name='status']").bootstrapSwitch();
                changeStatus();
            },


            ajax: "{!! Route('load-sub-categories-emails') !!}?id={{$category->id}}",
            columns: [
                {data: 'id', name: 'email_subjects.id'},
                {data: 'subject', name: 'email_subjects.subject'},
                {data: 'created_at', name: 'email_subjects.created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
                //{ data: 'updated_at', name: 'updated_at' }
            ]
        });

    });



    function addCategory(){
        $("#con-close-modal").modal('show');
    }


    function editRow(x){

        $('#loading').show();

        if(x!=''){
            $.post("<?php echo url('email-sub-category/loadEdit'); ?>", {id: x}, function(result){
                        if(result!='0'){
                            var data = JSON.parse(result);
                            $.each(data, function(k,v){
                                var ref = $("#add_category").find("#"+k);
                                $(ref).val(v);
                                $("#con-close-modal").modal('show');
                            });
                        }else{
                            swal("Error!", "Something went wrong.", "error");
                        }
                        $('#loading').hide();
                    }

            );
        }
    }





</script>

@endsection
