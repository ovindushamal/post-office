@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<style>
    form#add_category .loader {
        float: right;
        margin-left: 5px;
        margin-top: 6px;
    }
    form#add_category .loader img{
        display: none;
    }
    .add_more{
        float: right;
        margin-bottom: 5px;
    }
</style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Categories </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> Email Categories</h3>
                    </div>
                    <div class="add_more m-r-5">
                        <button type="button" class="btn btn-rose" onclick='addCategory()' > <i class="fa fa-plus"></i> Add More</button>
                    </div>
                    <div class="card-content">

                        <div class="material-datatables">
                            <table id="load_datatable" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Total</th>
                                    <th>Dated</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="4">No Record found yet.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--  add category -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h4 class="modal-title text-center">Add New Category</h4>
            </div>
            <form id="add_category" method="post" role="form" >

                <input type="hidden" name="id" value="" id="id" >

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="category" class="form-control" value="" id="category" placeholder="category name" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info btn-xs">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<!-- common modal for share emails -->
<div id="shareSettingsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h4 class="modal-title text-center" id="share_pop_title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="category" class="col-md-12">Sharing Code:</label>
                    <div class="col-md-6">
                        <p id="created_code"></p>
                    </div>
                    <div class="col-md-6">
                        <button onclick="copyToClipboard('hidden_code')" type="button" class="btn btn-primary btn-sm clipboard"> Copy</button>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="modal-footer">
                <input type="hidden" value="" id="hidden_code">
                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


<div id="AssignModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                <h4 class="modal-title text-center"> Assign Category </h4>
            </div>
            <form id="assignCatForm" action="#">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="category" class="col-md-12">Select Category:</label>
                        <div id="load_assign_data">

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="modal-footer">
                    <input type="hidden" value="" id="category_id" name="tobedelete">
                    <button type="submit" class="btn btn-success btn-xs">Save</button>
                    <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<script>

    // assign form submit

$(document).ready(function () {

    $('#assignCatForm').submit(function () {

        $('#loading').show();

        var data = new FormData(this);

        $.ajax({
            url: "{{ route('save-assign-category') }}",
            data: data,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(result){

                if(result == 1){

                    refreshTable();
                    swal('Success', 'Category deleted successfully!', 'success');
                    $('#AssignModal').modal('hide');

                }else{
                    swal('Error', 'Unknown error!', 'error');
                }

                $('#loading').hide();

            }
        });
        return false;
    });
})


    // delete
    function deleteIt(x){

        var id = $(x).data('id');
        var obj = $(x).data('obj');
        var link = $("#delete_link").val();

        if(id!='' && obj!=''){
          swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary Record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
             }).then(function() {

                            $("#loading").show();

                            $.post('{{ route('delete') }}', {id: id, obj: obj}, function(result){
                                    // delete image gallery check to prevent error
                                    if(obj != 'gallery') {
                                        refreshTable();
                                    }
                                    if(result!='0'){
                                        var data = JSON.parse(result);

                                        if(data.type == 'success'){
                                            swal("Success!", data.msg, "success");
                                        }

                                        if(data.type == 'error'){

                                            $('#loading').hide();

                                                swal({
                                                    title: 'OOPS!',
                                                    text: data.msg,
                                                    type: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#398439',
                                                    cancelButtonColor: '#d57171',
                                                    confirmButtonText: 'Yes, Change !'
                                                }).then(function(isConfirm) {

                                                    if(isConfirm){
                                                        assignCategory(id);
                                                    }
                                                });
                                        }

                                    }else{
                                        swal("Error!", "Something went wrong.", "error");
                                    }
                                    $('#loading').hide();
                                }
                            );

                    });

        }else{
            swal("Error!", "Information Missing. Please reload the page and try again.", "error");
        }
    }

    // assign category

    function assignCategory(id){
        $('#loading').show();
        $('#category_id').val(id);
        $.get('{{ route('assign-category') }}',
                {id:id}, function (data) {

                    $('#load_assign_data').html(data);
                    $('#loading').hide();
                    $('#AssignModal').modal('show');
        });
    }

    // share emails popup
    function share_emails_code(e){

        $('.clipboard').html('Copy');
        $('.clipboard').addClass('btn-primary').removeClass('btn-success');

        var type = $(e).data('title');
        var account_id = $(e).data('id');
        $('#loading').show();

        $.post('{{route('sharing-code-gen')}}',
                {type:type, account_id: account_id},
                function(data) {
                    if (data == 3) {
                        swal("Warning!", "The code is already generated!", "warning");
                    } else {

                        $('#created_code').html(data);
                        $('#hidden_code').val(data);
                        $('#share_pop_title').html(type + ' sharing code');
                        $('#shareSettingsModal').modal('show');
                        $('#loading').hide();
                    }
                }
        );
    }

    // copy to clipboard

    function copyToClipboard(elementId) {

        // Create a "hidden" input
        var aux = document.createElement("input");

        aux.setAttribute("value", document.getElementById(elementId).value);
        // Append it to the body
        document.body.appendChild(aux);
        // Highlight its content
        aux.select();
        // Copy the highlighted text
        document.execCommand("copy");
        // Remove it from the body
        document.body.removeChild(aux);

        $('.clipboard').text('copied to clipboard');
        $('.clipboard').addClass('btn-success').removeClass('btn-primary');
    }


    $(document).ready(function(){



        //saving new category
        $("#add_category").submit(function(){

            $('#loading').show();

            var data = new FormData(this);

            $.ajax({
                url: "<?php echo route('email-sub-category.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    $('#loading').hide();
                    if(result=='1'){
                        $('#add_category')[0].reset();
                        $("#id").val("");
                        $("#con-close-modal").modal('hide');
                        swal("Good job!", "Category has been saved successfully.", "success")
                        refreshTable();
                    }else{
                        $('.loader img').css('display','none');
                        swal("Error!", "Something went wrong.", "error");
                    }
                }
            });

            return false;
        });


        $('#load_datatable').DataTable({
            "order": [[0, 'desc']],
            processing: true,
            serverSide: true,
            responsive: true,
            "drawCallback": function () {
                //table_draw();
                $("[name='status']").bootstrapSwitch();
                changeStatus();
            },


            ajax: "{!! Route('load-sub-categories') !!}",
            columns: [
                {data: 'index', name: 'index'},
                {data: 'category', name: 'category'},
                {data: 'type', name: 'type'},
                {data: 'total', name: 'total'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
                //{ data: 'updated_at', name: 'updated_at' }
            ]
        });

    });



    function addCategory(){
        $("#con-close-modal").modal('show');
    }


    function editRow(x){

        $('#loading').show();

        if(x!=''){
            $.post("<?php echo url('email-sub-category/loadEdit'); ?>", {id: x}, function(result){
                        if(result!='0'){
                            var data = JSON.parse(result);
                            $.each(data, function(k,v){
                                var ref = $("#add_category").find("#"+k);
                                $(ref).val(v);
                                $("#con-close-modal").modal('show');
                            });
                        }else{
                            swal("Error!", "Something went wrong.", "error");
                        }
                        $('#loading').hide();
                    }

            );
        }
    }





</script>

@endsection
