@extends('layouts.app')
@section('title', 'Import Template')
@section('content')
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li> <a href="#">Template</a></li>
                            <li class="active"> Add </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">


                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Add New Template</h3>
                    </div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('template.store') }}">
                            {{ csrf_field() }}


                            <div class="form-group">
                                <label class="col-md-2  control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" placeholder="title of template">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">Details</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" placeholder="some details" ></textarea>
                                </div>
                            </div>

                            <div class="form-group account-btn m-t-10">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-xs-10">
                                    <button class="btn w-md btn-bordered btn-primary  " type="submit">Add</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>


        </div>


    </div>

@endsection
