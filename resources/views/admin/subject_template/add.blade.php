@extends('layouts.app')
@section('title', 'Import Template')
@section('content')
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">

                        <ol class="breadcrumb p-0 m-0">
                            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                            <li> <a href="{{ url('/library') }}">Email Template</a></li>
                            <li class="active"> Add </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Add New Subject</h3>
                    </div>
                    <div class="panel-body">


                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif


                        <form class="form-horizontal" method="post" role="form" id="subject-template" >
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{!! (isset($data->id) && $data->id!='')?$data->id:'' !!}">
                            <div class="form-group">
                                <label class="col-md-2  control-label">Subject:</label>
                                <div class="col-md-10">
                                    <input type="text" value="{!! (isset($data->subject) && $data->subject!='')?$data->subject:'' !!}" name="subject" class="form-control" placeholder="subject of template">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">Details:</label>
                                <div class="col-md-10">
                                    <textarea class="form-control"  name="details" rows="5" placeholder="some details" >{!! (isset($data->details) && $data->details!='')?$data->details:'' !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group account-btn m-t-10">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-xs-10">
                                    <button class="btn w-md btn-bordered btn-primary  " type="submit">Add</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>


        </div>


    </div>


    <script>
        $(document).ready(function(){


            $("#subject-template").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                $.ajax({
                    url: "<?php echo route('library.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        if(result == '1'){
                            $('#loading').hide();
//                            swal("Good job!", "Subject has been saved successfully.", "success")
                            swal({
                                        title: "Good job!",
                                        text: "Subject has been saved successfully!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    }).then(function(isConfirm) {
                                        if (isConfirm) {
                                            window.location.href = "<?php echo route('library.index'); ?> ";
                                        }
                                    });

                        }else{
                            swal("Error!", "Something went wrong.", "error");
                            $('#loading').hide();
                        }
                    }
                });

                return false;
            });

        });
    </script>

@endsection
