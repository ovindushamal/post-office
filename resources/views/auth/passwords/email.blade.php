@extends('layouts.app')
@section('content')
<?php
$setting = DB::table('app_settings')->first();
?>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" filter-color="black" data-image="{{ isset($setting->forgot_password_bg) && $setting->forgot_password_bg!=''? asset('assets/img/app/'.$setting->forgot_password_bg) :  asset('assets/img/register.jpeg') }}">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <div class="card card-login card-hidden">
                            <div class="card-header text-center" data-background-color="rose">
                                <h4 class="card-title">Forgot Password</h4>
                                <div class="social-line">
                                </div>
                            </div>
                            <p class="category text-center">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                                </p>
                                <div class="card-content">
                                    <form class="form-horizontal" id="forgotPassword">
                                        {{ csrf_field() }}

                                        <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <span class="input-group-addon">
                                                <i class="material-icons">mail</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email</label>
                                                <input id="email" type="email"  class="form-control" name="email" value="{{ old('email') }}" required autocomplete="off">
                                                @if ($errors->has('email'))
                                                    <small class="text-danger"> <strong>{{ $errors->first('email') }}</strong> </small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button class=" sub-btn btn w-md btn-bordered btn-rose  "  type="submit">Send Email  </button>
                                            </div>
                                        </div>
                                        <div class="divider m-b-10"></div>
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <a href="{{ route('login') }}" class="text-muted"><span class="material-icons">fingerprint</span>Sign in</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#forgotPassword').submit(function(){
            $('#loading').show();
            var data = new FormData(this);
            $.ajax({
                url: "<?php  echo route('send-password'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    if(result == 2){
                        swal('Warning', "We can't find a user with that e-mail address." ,'warning');
                    }
                    else{
                        $('.sub-btn').attr('disabled','disabled');
                        swal('Success', 'Your account information is send to your email!' ,'success');
                    }
                    $('#loading').hide();
                }
            });
            return false;
        });
    });

    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });

    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

@endsection


