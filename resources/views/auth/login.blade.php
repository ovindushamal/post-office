@extends('layouts.app')
@section('content')
<?php
$setting = DB::table('app_settings')->first();
?>

<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" filter-color="black" data-image="{{ isset($setting->login_bg) && $setting->login_bg!='' ? asset('assets/img/app/'.$setting->login_bg) :  asset('assets/img/login.jpeg') }}">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <div class="card card-login card-hidden">
                            <div class="card-header text-center" data-background-color="rose">
                                <h4 class="card-title">Login</h4>
                                <div class="social-line">
                                </div>
                            </div>
                            <p class="category text-center">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if(session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                            </p>
                            <div class="card-content">
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}" autocomplete="off">
                                    {{ csrf_field() }}

                                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">mail</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Email</label>
                                            <input id="email" type="email"  class="form-control" name="email" value="{{ old('email') }}" autofocus>
                                            @if ($errors->has('email'))
                                                <small class="text-danger"> <strong>{{ $errors->first('email') }}</strong> </small>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Password</label>
                                            <input id="password" type="password"  class="form-control" name="password" value="{{ old('password') }}">
                                            @if ($errors->has('password'))
                                                <small class="text-danger"> <strong>{{ $errors->first('password') }}</strong> </small>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input id="checkbox-signup" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }} >
                                            <span class="text-muted">Remember me</span>
                                        </label>
                                    </div>

                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-rose  btn-wd btn-">Let's go</button>
                                    </div>
                                    <div class="divider m-b-10"></div>
                                    <div class="col-sm-12">
                                        <div class="pull-left">
                                            <a href="{{ route('password.request') }}" class="text-muted"><span class="material-icons">lock_open</span><small> Forgot your password?</small></a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ route('register') }}" class="text-muted"><span class="material-icons">person_add</span><small>Register</small></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $().ready(function() {
            demo.checkFullPageBackgroundImage();

            setTimeout(function() {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700)
        });
    </script>
@endsection
