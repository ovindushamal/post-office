@extends('layouts.app')
@section('title', 'Categories')
@section('content')

<!--Morris Chart CSS -->
<link href="{{ asset('assets/plugins/morris/morris.css') }}" rel="stylesheet">
<!-- jvectormap -->
<link href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />

<style>
    .f-20{font-size: 20px;}
    .f-24{font-size: 24px;}
    .m-t-3{
        margin-top: 3px;
    }
</style>
<div class="content">
    <!-- Start content -->
    <div class="content-fluid">
        <div class="col-xs-12">
            <div class="page-title-box pull-right">
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
                    <li class="active">Autoresponders Report</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12">
            <h2 class="page-title">
                {{ ucfirst( $data->title ) }} Report
            </h2>
        </div>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

    @if(Auth::user()->sync == 1)
            <a href="{{ url('opened-report') }}?id={{$title_id}}">

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fas fa-envelope-open"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Opened</p>
                            <h3 class="card-title">{{ $open }}</h3>
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{ url('unopened-report') }}?id={{$title_id}}">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Unopened</p>
                            <h3 class="card-title">{{ count($unOpenEmails) }}</h3>
                        </div>
                    </div>
                </div>

            </a>
        @endif

        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">mouse</i>
                </div>
                <div class="card-content">
                    <p class="category">clicked</p>
                    <h3 class="card-title">{{ $click }}</h3>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="fa fa-line-chart"></i>
                </div>
                <div class="card-content">
                    <p class="category">Most open & click</p>
                    <p class="card-title m-b-5 m-t-5">
                        <span class="text-muted"> <i class="fa fa-mouse-pointer"></i> <span> {{ $fav_open_time  }} </span> </span>
                        <span class="text-muted"><i class="fas fa-envelope-open"></i><span> {{ $fav_click_time }}</span> </span>
                    </p>

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="card-box">
                    <h3> Autoresponders type wise Report  </h3>
                    <?php $color = array('rose', 'green', 'blue', 'orange') ?>

                        @foreach($autoresponder_type as $index => $value)
                            <a href="{{ route('type-wise-stats', 'id='.$value['id'].'&type='.$value['type'].'') }}">
                                <div class="col-lg-6 col-md-6 col-sm-6" title="{{ ucfirst( $value['type'] ) }} Report" data-toggle="tooltip">
                                    <div class="card card-stats">
                                        <div class="card-header" data-background-color="{{ $color[$index] }}">
                                            <i class="material-icons">mouse</i>
                                        </div>
                                        <div class="card-content">
                                            <p class="category">{{ $value['type'] }}</p>
                                            <p class="card-title m-b-5 m-t-5">
                                                <span class="text-muted"> <i class="fa fa-mouse-pointer"></i> <span> {{ $value['open'] }} </span> </span>
                                                <span class="text-muted"><i class="fas fa-envelope-open"></i><span> {{ $value['click'] }}</span> </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                        <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
        </div>

        <div class="col-xs-12">
                <!-- 24 hour performance  -->
                <div class="row">
                    <div class="clearfix"></div>
                    <!--  Line Chart -->
                    <div class="col-lg-12">
                        <div class="card">
                            <!-- /primary heading -->
                            <div class="card-header card-header-text" data-background-color="rose">
                                <h3 class="card-title"> <i class="fa fa-line-chart"></i> 24-hour performance</h3>
                            </div>
                            <div class="card-content">
                                <div class="text-center">
                                    <ul class="list-inline chart-detail-list">
                                        <li>
                                            <h5><i class="fa fa-circle m-r-5" style="color: #7e57c2;"></i>Open</h5>
                                        </li>
                                        <li>
                                            <h5><i class="fa fa-circle m-r-5" style="color: #34d3eb;"></i>Click</h5>
                                        </li>
                                    </ul>
                                </div>
                                <div id="morris-line-example" style="height: 300px;"></div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>
                </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="row">
                        <!-- Donut Chart -->
                        <div class="col-lg-6">
                            <div class="card">
                                <!-- /primary heading -->
                                <div class="card-header card-header-text" data-background-color="rose">
                                    <h3 class="card-title"> Top locations by opens</h3>
                                </div>

                                    <div class="card-content">
                                        <div class="material-datatables">
                                            <table id="load_datatable" class="table table-bordered table-no-bordered table-hover" cellspacing="0">
                                                <thead>
                                                <tr>
                                                    <th>Location</th>
                                                    <th>Open</th>
                                                    <th>Click</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $count = 0;
                                                if(count($countries) > 0){
                                                foreach( $countries as $key => $value ){

                                                foreach( $value as $index  => $item ){ $count++; ?>
                                                <tr>

                                                    <td> <?= $index ?></td>
                                                    <td><?= $item['open'] ?></td>
                                                    <td><?= $item['click'] ?></td>
                                                </tr>
                                                <?php  } } } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                </div>
                            </div>
                            <!-- /Portlet -->
                        </div>
                        <!-- stats about pc, mobile, iphone -->
                        <!-- Donut Chart -->
                        <div class="col-lg-6">
                            <div class="card">
                                <!-- /primary heading -->
                                <div class="card-header card-header-text" data-background-color="rose">
                                    <h3 class="card-title"> User Agents </h3>
                                </div>
                                <div class="card-content">
                                    <div class="col-md-6" style="border-right: 1px solid #e1e1e1;">
                                        <div class="portlet-body">
                                            <h4 class="portlet-title text-dark"> Open </h4>
                                            <div id="morris-donut-open" style="height: 300px;"></div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet-body">
                                            <h4 class="portlet-title text-dark"> Click </h4>
                                            <div id="morris-donut-click" style="height: 300px;"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <?php foreach( $agent_label as $value){ ?>
                                                <li>
                                                    <h5><i class="fa fa-circle m-r-5" style="color: <?= $value['color'] ?>"></i> <?= $value['agent'] ?> </h5>
                                                </li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /Portlet -->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title"> Autoresponders List Wise Report </h3>
                        </div>
                        <div class="card-content">
                            <div class="material-datatables">
                                <table id="load_List_datatable" class="table table-bordered table-no-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>List</th>
                                        <th>Open</th>
                                        <th>Click</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $count = 0;
                                    foreach( $openClickByList as  $item ){ $count++; ?>
                                    <tr>
                                        <td> <?= $count ?></td>
                                        <td><?= $item['title'] ?></td>
                                        <td><?= $item['opened'] ?></td>
                                        <td><?= $item['click'] ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- World map report  -->
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title">Autoresponders World Map Report</h3>
                        </div>
                            <div class="card-content">
                                <div id="world-map-markers" style="height: 500px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- world map  -->

<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
{{--<script src="{{ asset('assets/jvectormap/gdp-data.js') }}"></script>--}}

{{--<script src="{{ asset('assets/jvectormap/jvectormap.init.js') }}"></script>--}}
<script>

    $(document).ready(function(){
        // datatable stats
        $('#load_datatable, #load_List_datatable').DataTable({
        });
    });

    var marker = {!! $world_json !!}

    ! function($) {
        "use strict";

        var VectorMap = function() {
        };

        VectorMap.prototype.init = function() {
            //various examples
            $('#world-map-markers').vectorMap({
                map : 'world_mill_en',
                scaleColors : ['#ea6c9c', '#ea6c9c'],
                normalizeFunction : 'polynomial',
                hoverOpacity : 0.7,
                hoverColor : false,
                regionStyle : {
                    initial : {
                        fill : '#5fbeaa'
                    }
                },
                markerStyle: {
                    initial: {
                        r: 9,
                        'fill': '#a288d5',
                        'fill-opacity': 0.9,
                        'stroke': '#fff',
                        'stroke-width' : 7,
                        'stroke-opacity': 0.4
                    },

                    hover: {
                        'stroke': '#fff',
                        'fill-opacity': 1,
                        'stroke-width': 1.5
                    }
                },
                backgroundColor : 'transparent',


                markers : marker
            });


        },
            //init
                $.VectorMap = new VectorMap, $.VectorMap.Constructor =
                VectorMap
    }(window.jQuery),

//initializing
            function($) {
                "use strict";
                $.VectorMap.init()
            }(window.jQuery);
</script>

<!--Morris Chart-->
<script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/plugins/morris/raphael-min.js') }}"></script>
<script>
    var chart_data = {!! $chart !!};
    /**
     * Theme: Ubold Admin Template
     * Author: Coderthemes
     * Morris Chart
     */
    !function($) {
        "use strict";
        var MorrisCharts = function() {};
        // donut chart
        //creates Donut chart
        MorrisCharts.prototype.createDonutChart = function(element, data, colors) {
            Morris.Donut({
                element: element,
                data: data,
                resize: true, //defaulted to true
                colors: colors
            });
        },
        //creates line chart
        MorrisCharts.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
            Morris.Line({
                element: element,
                data: data,
                xkey: xkey,
                ykeys: ykeys,
                labels: labels,
                fillOpacity: opacity,
                pointFillColors: Pfillcolor,
                pointStrokeColors: Pstockcolor,
                behaveLikeLine: true,
                gridLineColor: '#eef0f2',
                hideHover: 'auto',
                resize: true, //defaulted to true
                lineColors: lineColors
            });
        },
                MorrisCharts.prototype.init = function() {

                    //create line chart
                    var $data  = chart_data;
                    this.createLineChart('morris-line-example', $data, 'hour', ['a', 'b'], ['Opens', 'Clicks'],['0.1'],['#ffffff'],['#999999'], ['#7e57c2', '#34d3eb']);
 //creating donut chart
        var $donutOpen = <?= $user_agent_open ?> ;
        var $donutClick = <?= $user_agent_click ?> ;
        this.createDonutChart('morris-donut-open', $donutOpen, [
            <?php foreach( $agent_label as $value){ ?>
                <?= "'". $value['color'] . "',";  ?>
            <?php } ?>
        ]);
        this.createDonutChart('morris-donut-click', $donutClick, [
            <?php foreach( $agent_label as $value){ ?>
                <?= "'". $value['color'] . "',";  ?>
            <?php } ?>

        ]);
    },
            //init
                $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
    }(window.jQuery),
//initializing
            function($) {
                "use strict";
                $.MorrisCharts.init();
            }(window.jQuery);
</script>
@endsection
