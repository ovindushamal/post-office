@extends('layouts.app')
@section('content')
    <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/morris/morris.css') }}" rel="stylesheet">
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fa fa-file-text"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Subject Library</p>
                            <h3 class="card-title"><?php echo $subject_count; ?></h3>
                        </div>
                    </div>
                </div>

                @if(Auth::User()->type=='a')
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="orange">
                                <i class="fa fa-envelope-square"></i>
                            </div>
                            <div class="card-content">
                                <p class="category">Email Templates</p>
                                <h3 class="card-title"><?php echo $your_templates; ?></h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="orange">
                                <i class="fas fa-users"></i>
                            </div>
                            <div class="card-content">
                                <p class="category">Users</p>
                                <h3 class="card-title"><?php echo $user_count; ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="orange">
                                <i class="fa fa-line-chart"></i>
                            </div>
                            <div class="card-content">
                                <p class="category m-b-18">Most open & click</p>
                                <p class="card-title">
                                    <span class="text-muted font-16"> <i class="fa fa-mouse-pointer"></i> <span> {{ $fav_open_time  }} </span> </span>
                                    <span class="text-muted font-16"><i class="fas fa-envelope-open"></i><span> {{ $fav_click_time  }}</span> </span>
                                </p>

                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="orange">
                                <i class="material-icons">mail</i>
                            </div>
                            <div class="card-content">
                                <p class="category">Email</p>
                                <h3 class="card-title"><?php echo $your_templates; ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="orange">
                                <i class="fa fa-line-chart"></i>
                            </div>
                            <div class="card-content">
                                <p class="category m-b-18">Most open & click</p>
                                <p class="card-title">
                                    <span class="text-muted font-16"> <i class="fa fa-mouse-pointer"></i> <span> {{ $fav_open_time  }} </span> </span>
                                    <span class="text-muted font-16"><i class="fas fa-envelope-open"></i><span> {{ $fav_click_time  }}</span> </span>
                                </p>

                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fas fa-envelope-open"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Opened</p>
                            <h3 class="card-title"><?php echo $total_opened; ?></h3>
                        </div>
                    </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fa fa-mouse-pointer"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Clicked</p>
                            <h3 class="card-title"><?php echo $total_clicked; ?></h3>
                        </div>
                    </div>
                </div><!-- end col -->
            </div>
            <div class="row">
                <!-- top subjects for admin  / latest campaign for user -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <h3 class="header-title"><i class="material-icons">equalizer</i> Open and Click</h3>

                                </div>
                                <!-- start of graph -->
                                <div class="card-content">
                                    <div class="pull-right m-b-30">
                                        <div id="reportrange" class="form-control">
                                            <i class="fa fa-calendar"></i>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="morris-line-example" style="height: 240px;"></div>
                                    <p class="text-muted m-b-0 m-t-15 font-13 text-overflow"></p>
                                    <div class="button-group">
                                        <button class="btn btn-success">Sent {{$sent}}</button>
                                        <button class="btn btn-danger">Error {{$sent_error}}</button>
                                        <button class="btn btn-info">Queued {{$que}}</button>
                                    </div>
                                </div>
                            </div>
                            @include('top_used_subjects')
                        </div>
                        <!-- Allover  Campaigns -->

                </div>
                <div class="clearfix"></div>
                </div>

                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h4 class="card-title"><i class="material-icons">assignment</i> Broadcast Report</h4>
                        </div>
                        <div class="card-content">
                            <div class="material-datatables">
                                <button onclick="refreshTable()" title="Refresh table data" data-toggle="tooltip" class="btn btn-rose cursor btn-xs"> <i class="fa fa-refresh" aria-hidden="true"></i> </button>
                                <table id="load_datatable" class="table table-bordered table-no-bordered table-hover" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Subject</th>
                                        <th>Status</th>
                                        <th>Category</th>
                                        <th>Send List</th>
                                        <th>Open</th>
                                        <th>Click</th>
                                        <th>Dated</th>
                                        <th width="150">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="5">No Record found yet.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <input type="hidden" id="dates" data-start="<?= $start_date ?>" data-end="<?= $end_date ?>" value="" >
        </div>
        <!-- error modal -->

        <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center" id="modelTitleId"><i class="fa fa-bug" aria-hidden="true"></i> Autoresponder Error</h4>
                    </div>
                    <div class="modal-body">
                        <div id="Errro" class="text-danger text-capitalize"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <form action="{!! Route('new-template.create') !!}" method="get" style="display: none;" id="use_me">
            <input type="hidden" name="use_me" value="">
        </form>

        <script>
            function submitUseMe(x){
                $('input[name="use_me"]').val($(x).data('id'));
                $('#use_me').submit();
            }
        </script>
        <!--Morris Char-->
        <script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/raphael/raphael-min.js') }}"></script>
        {{--<script src="{{ asset('assets/pages/jquery.morris.init.js') }}"></script>--}}

        <script src="{{ asset("assets/js/moment.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}"></script>

        <script>
            // error show
            function show_error(e){
               var lst =  $(e).find('td:last-child');
                $(lst).click(function (event) {
                    console.log('last td clicked');
                    return false;
                });
                var id = $(e).data('for');
                $('#loading').show();
                $.get('{{ route('show_boradcast_error') }}',{id:id}, function (data) {
                    if(data !=""){
                        $('#Errro').html(data);
                        $('#loading').hide();
                        $('#errorModal').modal('show');
                    }
                });
            }

            $(document).ready(function(){
                // datatable stats

                $('#load_datatable').DataTable({
                    //"dom": '<"top"lfip<"clear">>rt<"bottom"lfip<"clear">>',
                    "pageLength":25,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    "initComplete": function (settings, json) {
                        $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
                    },
                    "drawCallback": function () {
                        $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
                    },

                    columnDefs:[
                        { className: "cat_width", "targets": [3] },
                        { className: "action_width", "targets": [8] }
                    ],
                    "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                        if (aData.status == 'error'){
                            //console.log('ok');
                            $(nRow).attr('class', 'bg-danger cursor-pointer');
                            $(nRow).attr('onclick', 'show_error(this)');
                            $(nRow).attr('data-for', aData.title_id);
                        }
                    },
                    ajax: "{!! Route('load-your-email-content') !!}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'title', name: 'title', orderable: true, searchable: true},
                        {data: 'subject', name: 'subject', orderable: true, searchable: true},
                        {data: 'status', name: 'status', orderable: true, searchable: true},
                        {data: 'category', name: 'category', orderable: true, searchable: true},
                        {data: 'send_list_total', name: 'send_list_total', searchable: false},
                        {data: 'opened', name: 'opened', orderable: true, searchable: false},
                        {data: 'clicked', name: 'clicked', orderable: true, searchable: false},
                        {data: 'created_at', name: 'created_at', orderable: true, searchable: false},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            });
        </script>
        <!-- graph scripts  -->
        <script>
            // delete
            function deleteThis(e){
                var id = $(e).data('id');
                if(id!=''){

                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        confirmButtonText: 'Yes, delete it!',
                        buttonsStyling: false
                    }).then(function() {
                        $("#loading").show();

                        $.post('{{ route('delete-email') }}', {id: id}, function(result){
                                if(result == 1){
                                    refreshTable();
                                    swal("Success!", " Email deleted successfully! ", "success");

                                }else{
                                    swal("Error!", "Something went wrong.", "error");
                                }
                                $('#loading').hide();
                            }
                        );

                    }).catch(swal.noop);
                }else{
                    swal("Error!", "Information Missing. Please reload the page and try again.", "error");
                }
            }
            // make frivate

            function makePrivate(e){
                $('#loading').show();
                var id = $(e).data('id');
                var action = 'title';
                $.post('{{ route('private-email') }}',
                        {id:id, action:action},
                        function(data){

                            var obj = $.parseJSON(data);

                            if(obj.res == 0){
                               refreshTable();
                                swal('Success', 'Email marked as public successfully!' , 'success');
                            }
                            if(obj.res == 1){
                               refreshTable();
                                swal('Success', 'Email marked as private successfully!' , 'success');
                            }


                    $('#loading').hide();
                });
            }

            var $data  = <?php echo $broadcast_chart; ?>;
            var $data_type  = <?php echo $broadcast_type; ?>;

            loadChart('');
            function loadChart(e){

                $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

                $('#reportrange').daterangepicker({
                    format: 'YYYY-MM-DD',
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment(),

                    minDate: '01/01/2017',

                    dateLimit: {
                        days: 60
                    },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                        'This Year': [moment().startOf('year'), moment().subtract(1, 'days')]
                    },
                    opens: 'left',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-success',
                    cancelClass: 'btn-default',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        cancelLabel: 'Cancel',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                }, function (start, end, label) {

                    //console.log(start.format('YYYY-MM-DD'));
                    var type ="";

                    $('#loading').show();
                    $.post('{{route('load-broadcast-chart')}}',
                            {start_date: start.format('YYYY-MM-DD'), end_date: end.format('YYYY-MM-DD')},
                            function(data){


                                var result = jQuery.parseJSON(data);

                                if(result.length == 0 ){

                                    $('#morris-line-example').text('No result found!');

                                    $('#loading').hide();
                                    return false;
                                }else{
                                    $('#morris-line-example').text('');
                                }

                                var data_set    = [];
                                $.each(result, function(index, value){

                                    var  data_obj   = new Object;

                                    data_obj['dated']        = value['dated'];
                                    data_obj['clicked']  = value['clicked'];
                                    data_obj['opened']  = value['opened'];

                                    data_set.push(data_obj);

                                });

                                // $("#json_Data").val(data);

                                // console.log($("#json_Data").val());

                                //var $data  = $("#json_Data").val();
                                var $data  = data_set;

                                //var $data = [{"dated":"2017-05-05","clicked":5,"opened":0},{"dated":"2017-05-04","clicked":0,"opened":0},{"dated":"2017-05-03","clicked":2,"opened":6}]
                                Morris.Line({
                                    element: 'morris-line-example',
                                    data: $data,
                                    xkey: 'dated',
                                    ykeys: ['clicked', 'opened'],
                                    labels: ['Clicked', 'Opened'],
                                    fillOpacity:['0.1'],
                                    pointFillColors: ['#ffffff'],
                                    pointStrokeColors: ['#999999'],
                                    behaveLikeLine: true,
                                    gridLineColor: '#eef0f2',
                                    hideHover: 'auto',
                                    lineWidth: '3px',
                                    pointSize: 0,
                                    preUnits: '',
                                    resize: true, //defaulted to true
                                    lineColors: ['#188ae2', '#4bd396']
                                });
                                $('#loading').hide();
                            });
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                });
            }


            // line chart
            !function($) {

                var MorrisCharts = function() {};

                //creates line chart
                MorrisCharts.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
                    Morris.Line({
                        element: element,
                        data: data,
                        xkey: xkey,
                        ykeys: ykeys,
                        labels: labels,
                        fillOpacity: opacity,
                        pointFillColors: Pfillcolor,
                        pointStrokeColors: Pstockcolor,
                        behaveLikeLine: true,
                        gridLineColor: '#eef0f2',
                        hideHover: 'auto',
                        lineWidth: '3px',
                        pointSize: 0,
                        preUnits: '',
                        resize: true, //defaulted to true
                        lineColors: lineColors
                    });
                },

                        MorrisCharts.prototype.init = function() {
                            this.createLineChart('morris-line-example', $data, 'dated', ['clicked', 'opened'], ['Clicked', 'Opened'],['0.1'],['#ffffff'],['#999999'], ['#188ae2', '#4bd396']);

                        },
                        //init
                        $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts

            }(window.jQuery),

                //initializing
                    function($) {
                        $.MorrisCharts.init($data);
                    }(window.jQuery);

        </script>
        <!-- ends graph scripts  -->
    </div>
@endsection