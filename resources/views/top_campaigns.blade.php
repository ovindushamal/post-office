<h4 class="header-title m-t-0"> Latest Campaign(s)</h4>
<div class="all_comp" id="">
    <table class="table table-colored table-inverse table-hover table-striped table-bordered  no-footer">
        <thead>
        <tr>
            <th>Subject</th>
            <th>Opened</th>
            <th>Clicked</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(count($latest_campaigns) >0 ){
        foreach( $latest_campaigns as $value ){ ?>
        <tr>
            <td><?= $value->subject ?></td>
            <td><?= $value->opened ?></td>
            <td><?= $value->clicked ?></td>
        </tr>
        <?php }
        }else{ ?>
        <tr>
            <td colspan="3">No Record found yet.</td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>