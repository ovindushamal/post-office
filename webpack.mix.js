const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
 'assets/css/bootstrap.min.css',
 'assets/css/core.css',
 'assets/css/components.css',
 'assets/css/icons.css',
 'assets/css/pages.css',
 'assets/css/menu.css',
 'assets/css/responsive.css'

], 'assets/css/all.css');

mix.combine([
 'assets/js/jquery.min.js',
 'assets/js/bootstrap.min.js',
 'assets/js/detect.js',
 'assets/js/fastclick.js',
 'assets/js/jquery.blockUI.js',
 'assets/js/waves.js',
 'assets/js/jquery.slimscroll.js',
 'assets/js/jquery.scrollTo.min.js',
 'assets/js/jquery.core.js'

], 'assets/js/all.js');

mix.js('resources/assets/js/app.js', '../js')
    .sass('resources/assets/sass/app.scss', '../css');