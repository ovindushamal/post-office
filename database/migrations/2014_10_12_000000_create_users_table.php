<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email',  128)->unique();
                $table->string('password');
                $table->string('plain_password')->nullable();
                $table->char('type', 1)->default('u');
                $table->string('image',500)->nullable();
                $table->string('email_category',500)->default(0);
                $table->string('email_shared_id',500)->nullable();
                $table->tinyInteger('status')->default(1);
                $table->string('time_zone');
                $table->tinyInteger('sync');
                $table->rememberToken();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
             //add admin data
            $now = DB::raw('NOW()');
            DB::table('users')->insert(
                [
                    'name' => \Session::get('admin_name'),
                    'email' => \Session::get('admin_email'),
                    'password' =>  bcrypt(\Session::get('admin_password')),
                    'plain_password' => \Session::get('admin_password'),
                    'type' => 'adm',
                    'created_at' => $now
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
