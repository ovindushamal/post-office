<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmtpSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('smtp_settings')) {
            Schema::create('smtp_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->length(10)->unsigned();
               $table->string('title', 500)-> varchar(500);
                 $table->string('host', 150)-> varchar(150);
                 $table->string('port', 150)-> varchar(150)->nullable();
                 $table->string('username', 150)-> varchar(150);
                 $table->string('password', 150)-> varchar(150);
                 $table->tinyInteger('ssl')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smtp_settings');
    }
}
