<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAweberSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('aweber_settings')) {
            Schema::create('aweber_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->length(10)->unsigned();
                $table->string('account_title');
                $table->string('aweber_key');
                $table->string('aweber_secret')->nullable();
                $table->string('aweber_token')->nullable();
                $table->string('accessTokenKey')->nullable();
                $table->string('accessTokenSecret')->nullable();
                $table->tinyInteger('status');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aweber_settings');
    }
}
