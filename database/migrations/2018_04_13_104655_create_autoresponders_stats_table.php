<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutorespondersStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('autoresponders_stats')) {
            Schema::create('autoresponders_stats', function (Blueprint $table) {
                $table->increments('id');
                $table->string('broadcast_id');
                $table->integer('user_id')->length(10)->unsigned();
                $table->string('type', 100);
                $table->dateTime('opened_time')->nullable();
                $table->dateTime('clicked_time')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoresponders_stats');
    }
}
