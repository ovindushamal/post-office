<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailchimpSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mailchimp_settings')) {
            Schema::create('mailchimp_settings', function (Blueprint $table) {
                $table->increments('id');
                 $table->integer('user_id')->length(10)->unsigned();
                 $table->string('account_title');
                 $table->string('mailchimp_key');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailchimp_settings');
    }
}

