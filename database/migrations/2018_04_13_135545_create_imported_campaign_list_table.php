<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportedCampaignListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('imported_campaign_list')) {
            Schema::create('imported_campaign_list', function (Blueprint $table) {
                $table->increments('id');
                 $table->integer('user_id')->length(10);
                 $table->string('campaign_id');
                 $table->integer('account_id')->length(10)->unsigned()->nullable();
                 $table->string('api_key', 500);
                 $table->string('type');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imported_campaign_list');
    }
}

