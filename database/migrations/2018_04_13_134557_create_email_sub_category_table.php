<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSubCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_sub_category')) {
            Schema::create('email_sub_category', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('account_id')->nullable()->comment('autoresponder account id');
                $table->string('category');
                $table->string('type');
                $table->integer('user_id')->unsigned();
                $table->tinyInteger('status')->default(0);
                $table->integer('category_id');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_sub_category');
    }
}

