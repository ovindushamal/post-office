<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoresponderScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('autoresponder_schedule')) {
            Schema::create('autoresponder_schedule', function (Blueprint $table) {
                $table->increments('id');
                  $table->string('subject');
                  $table->text('json');
                  $table->integer('email_campaign_title_id')->length(10);
                  $table->integer('subject_id')->length(10)->nullable();
                  $table->string('api_key');
                  $table->integer('email_category_id')->length(10)->unsigned();
                  $table->integer('user_id')->length(10) ;
                  $table->string('campaign_id')->nullable();
                  $table->string('email_brodcast_id');
                  $table->string('type');
                  $table->string('click_link_id');
                  $table->tinyInteger('status');
                  $table->string('date_time');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoresponder_schedule');
    }
}
