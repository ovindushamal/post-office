<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsedAutoResponsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('used_auto_responsers')) {
            Schema::create('used_auto_responsers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->length(10)->unsigned();
                 $table->integer('subject_id')->length(10)->unsigned();
                 $table->string('type');
                 $table->string('title');
                 $table->string('replyto_email');
                 $table->string('fromFieldId');
                 $table->string('campaignId');
                 $table->string('selectedCampaigns');
                 $table->string('newsletterId');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('used_auto_responsers');
    }
}
