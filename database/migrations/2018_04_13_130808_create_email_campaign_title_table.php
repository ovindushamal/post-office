<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailCampaignTitleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_campaign_title')) {
            Schema::create('email_campaign_title', function (Blueprint $table) {
                $table->increments('id');
                  $table->integer('user_id')->length(10)->unsigned();
                  $table->integer('email_category_id')->length(10)->unsigned();
                  $table->string('title', 500);
                  $table->integer('parent_id')->length(10)->nullable();
                  $table->tinyInteger('status')->default(0);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_campaign_title');
    }
}
