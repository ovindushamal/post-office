<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClickLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('click_links')) {
            Schema::create('click_links', function (Blueprint $table) {
              $table->increments('id');
              $table->integer('subject_id')->length(10)->nullable();
              $table->integer('user_id')->length(10);
              $table->string('broadcast_id')->nullable();
              $table->integer('email_category_id')->length(10)->nullable();
              $table->integer('email_campaign_title_id')->length(10)->unsigned()->nullable();
              $table->string('redirect_link')->nullable();
              $table->string('type')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('click_links');
    }
}
