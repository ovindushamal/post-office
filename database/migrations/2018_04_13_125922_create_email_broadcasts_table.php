<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailBroadcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_broadcasts')) {
            Schema::create('email_broadcasts', function (Blueprint $table) {
                $table->increments('id');
                  $table->integer('user_id')->length(10)->unsigned()->nullable();
                  $table->integer('subject_id')->length(10)->unsigned()->nullable();
                  $table->string('message_id')->nullable();
                  $table->string('boradcast_id')->nullable();
                  $table->integer('email_campaign_title_id')->length(10)->unsigned();
                  $table->integer('email_category_id')->length(10)->unsigned();
                  $table->string('type')->nullable();
                  $table->integer('sent')->length(11)->default(0);
                  $table->integer('opened')->length(11)->default(0);
                  $table->integer('clicked')->length(11)->default(0);
                  $table->integer('unsubscribed')->length(11)->default(0);
                  $table->text('json');
                  $table->integer('group_id')->length(10);
                  $table->tinyInteger('status')->default(0)->comment('1 for error');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_broadcasts');
    }
}
