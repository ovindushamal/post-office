<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoresponderContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('autoresponder_contact')) {
            Schema::create('autoresponder_contact', function (Blueprint $table) {
                $table->increments('id');
                  $table->string('user_id')->nullable();
                  $table->string('list_name')->nullable();
                  $table->string('account_id');
                  $table->string('account_title')->nullable();
                  $table->string('type');
                  $table->string('list_id')->nullable();
                  $table->string('contact_id')->nullable();
                  $table->string('name')->nullable();
                  $table->string('email')->nullable();
                  $table->string('phone')->nullable();
                  $table->string('ip_address')->nullable();
                  $table->string('area_code')->nullable();
                  $table->string('country')->nullable();
                  $table->string('region')->nullable();
                  $table->string('longitude')->nullable();
                  $table->string('latitude')->nullable();
                  $table->string('subscribed_at')->nullable();
                  $table->string('status')->nullable();
                  $table->integer('page_no')->length(10)->unsigned();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoresponder_contact');
    }
}
