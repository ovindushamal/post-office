<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_contents')) {
            Schema::create('email_contents', function (Blueprint $table) {
                $table->increments('id');
                 $table->integer('user_id')->length(10)->unsigned();
                 $table->integer('subject_id')->length(10)->unsigned()->default(0);
                 $table->integer('email_category_id')->length(10)->unsigned()->nullable();
                 $table->integer('email_campaign_title_id')->length(10)->unsigned()->nullable();
                 $table->integer('category_id')->length(10)->unsigned()->nullable();
                 $table->text('content');
                 $table->tinyInteger('status')->default(1);
                 $table->tinyInteger('is_private')->unsigned();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_contents');
    }
}
