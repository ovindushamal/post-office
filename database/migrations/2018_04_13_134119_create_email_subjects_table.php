<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_subjects')) {
            Schema::create('email_subjects', function (Blueprint $table) {
                $table->increments('id');
                 $table->integer('user_id')->unsigned();
                 $table->integer('email_category_id')->unsigned()->default(0);
                 $table->integer('sub_category_id')->unsigned()->nullable();
                 $table->string('subject')->default('');
                 $table->integer('use_counter')->unsigned()->default(0);
                 $table->text('details');
                 $table->tinyInteger('status')->unsigned()->default(1);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_subjects');
    }
}

